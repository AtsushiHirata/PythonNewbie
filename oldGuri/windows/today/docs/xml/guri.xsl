<?xml version="1.0" encoding="Shift_JIS"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/TR/WD-xsl">

<xsl:template match="/">
  <html lang="ja">
    <head>
      <title>グリグリ作品</title>
    </head>
    <body>
      <xsl:apply-templates select="グリグリデータ" />
    </body>
  </html>
</xsl:template>

<xsl:template match="グリグリデータ">
  <xsl:apply-templates select="画像ファイル" />
  <xsl:apply-templates select="作成者" />
  <xsl:apply-templates select="保存日時" />
  <xsl:apply-templates select="コメント" />
</xsl:template>

<xsl:template match="画像ファイル">
  <img><xsl:attribute name="src"><xsl:value-of /></xsl:attribute></img>
</xsl:template>

<xsl:template match="作成者">
  <h1>作成者:<xsl:value-of /></h1>
</xsl:template>

<xsl:template match="保存日時">
  <p>保存日時:<xsl:value-of /></p>
</xsl:template>

<xsl:template match="コメント">
  <p>コメント:<xsl:value-of /></p>
</xsl:template>

</xsl:stylesheet>