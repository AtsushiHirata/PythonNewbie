VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Object = "{DB453C48-DD68-4BF0-B92B-05C3FDB4F141}#32.0#0"; "ctlGuriData.ocx"
Object = "{151EC63C-050E-4C62-8225-8448008EBD96}#1.0#0"; "ctlGuriView.ocx"
Begin VB.Form frmPreView 
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows の既定値
   Begin DevGuriViewControlProject.ctlGuriView ctlGuriView 
      Height          =   495
      Left            =   2760
      TabIndex        =   6
      Top             =   1200
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   873
   End
   Begin GuriData_UserControl.GuriData ctlGuriData 
      Height          =   480
      Left            =   2760
      TabIndex        =   5
      Top             =   480
      Width           =   480
      _ExtentX        =   847
      _ExtentY        =   847
   End
   Begin ComctlLib.ProgressBar prgLoad 
      Height          =   255
      Left            =   360
      TabIndex        =   4
      Top             =   2040
      Width           =   1455
      _ExtentX        =   2566
      _ExtentY        =   450
      _Version        =   327682
      Appearance      =   1
   End
   Begin VB.CommandButton cmdApply 
      Caption         =   "Apply"
      Height          =   495
      Left            =   2400
      TabIndex        =   3
      Top             =   2520
      Width           =   1335
   End
   Begin VB.CommandButton cmdCancel 
      Caption         =   "Cancel"
      Height          =   495
      Left            =   0
      TabIndex        =   2
      Top             =   2520
      Width           =   1335
   End
   Begin ComctlLib.ListView lvwPreView 
      Height          =   1215
      Left            =   1080
      TabIndex        =   1
      Top             =   600
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   2143
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      _Version        =   327682
      Icons           =   "imlPreViews"
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.CommandButton cmdRefresh 
      Caption         =   "Refresh"
      Height          =   495
      Left            =   1320
      TabIndex        =   7
      Top             =   2520
      Width           =   1095
   End
   Begin ComctlLib.ImageList imlDummy 
      Left            =   3840
      Top             =   2040
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   100
      ImageHeight     =   100
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin ComctlLib.ImageList imlPreViews 
      Left            =   240
      Top             =   960
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   100
      ImageHeight     =   100
      MaskColor       =   12632256
      _Version        =   327682
   End
   Begin VB.Label lblDataPath 
      Caption         =   "Label1"
      Height          =   255
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4455
   End
End
Attribute VB_Name = "frmPreView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit


'＜シナリオ＞
'・frmPreViewのDataPathプロパティにグリグリのデータが保管されているパスをセットする。
'・frmPreViewはそのパス内のデータファイル名をリストアップする。
'・リストアップされたファイル名をもとに、データを読み込み画像を作成し、イメージリストに登録する。
'・ListViewコントロールにイメージリストの画像を表示する。画像の下にはフルパスではないファイル名を表示。

'================================================
'Private Variables and Objects
'================================================

'プレビューを行うフォルダ
Private m_strDataPath As String

'プレビュー後、ユーザによって選択されたファイルのファイル名
Private m_strSelectedFile As String

'プレビュー後、ユーザによって選択されたファイルを指すリスト中のインデックス番号
Private m_lngSelectedItemIndex As Long

'================================================
'Public Methods
'================================================

'------------------------------------------------
Public Property Get DataPath() As String
'目的：プレビューを行うフォルダ名を返す。
'------------------------------------------------
    DataPath = m_strDataPath
End Property

'------------------------------------------------
Public Property Let DataPath(ByVal NewPath As String)
'目的：プレビューを行うフォルダを指定する。
'　　　フォルダが指定されたら、プレビューを作成する。
'------------------------------------------------
    Dim fh As New CFileHandling     'ファイルハンドリングユーティリティ
    Dim FileList() As String        '指定されたフォルダにあるグリデータファイルのリスト
    
    fh.GetFileList NewPath, FileList, "*.gri"
    
    Debug.Print NewPath
    Debug.Print UBound(FileList)
    
    Me.lblDataPath.Caption = NewPath
   
    
'    ' プログレス バー コントロールを表示します。
    Me.prgLoad.Visible = True

    If UBound(FileList) <> 0 Then
        Dim i As Integer 'counter
        For i = 0 To UBound(FileList)
            LoadData (NewPath & "\" & FileList(i))
            Me.ctlGuriView.CopyToClipboard
            Me.imlPreViews.ListImages.Add , FileList(i), Clipboard.GetData
            
            'リストビューコントロールにセット
            Me.lvwPreView.ListItems.Add i + 1, , FileList(i), FileList(i)
               
            Me.lblDataPath.Caption = " 現在 < " & FileList(i) & " > を読み込み中です。"
    
          ' プログレス バー コントロールを更新します。
          Me.prgLoad.Value = Int((i) / UBound(FileList) * 100)
          Me.Refresh
            
        Next i
    End If

    Me.lblDataPath.Caption = NewPath
    
    ' プログレス バー コントロールを非表示にします。
   Me.prgLoad.Visible = False
    
End Property


'選択したファイルのファイル名を返す。フルパス。
Public Property Get SelectedFile() As String
    SelectedFile = m_strSelectedFile
End Property


Private Sub cmdApply_Click()
    'ここはグリグリにインプリメントの際変更！！
'    frmTestMain.Text1 = m_strSelectedFile
    g_GuriController.SetCommand ("LOAD_BY_THUMNAIL")
    Unload Me
End Sub

Private Sub cmdCancel_Click()
    m_strSelectedFile = ""
    Unload Me
End Sub


Private Sub cmdRefresh_Click()
    Unload Me
    g_GuriController.SetCommand ("VIEW_THUMBNAIL")
End Sub

'Public Property Let SelectedFile(ByVal vNewValue As Variant)
'
'End Property


'================================================
'Private Methods
'================================================

Private Sub Form_Load()
    Me.Caption = VERSION_STRING & " 【サムネイル】"
    'Data Initialize
    m_strDataPath = App.Path
    'イメージリストの初期化
'    Me.lvwPreView.Icons = Me.imlPreViews
    m_strSelectedFile = ""
    
    Me.ctlGuriData.Visible = False
    Me.ctlGuriView.Visible = False
    
    'Size Setting
    Me.Width = 400 * Screen.TwipsPerPixelX
    Me.Height = 250 * Screen.TwipsPerPixelY
    
    Me.ScaleMode = vbPixels
    Me.ScaleHeight = 250
    Me.ScaleWidth = 400
    Me.ScaleLeft = 0
    Me.ScaleTop = 0
    
    Me.lblDataPath.Left = 0
    Me.lblDataPath.Top = 0
    Me.lblDataPath.Height = 20
    Me.lblDataPath.Width = Me.ScaleWidth
        
    'リストビューコントロールの状態設定
    Me.lvwPreView.Left = 0
    Me.lvwPreView.Top = 20
    Me.lvwPreView.Height = 200
    Me.lvwPreView.Width = Me.ScaleWidth
    Me.lvwPreView.View = lvwIcon
    Me.lvwPreView.Arrange = lvwAutoTop

    
    Me.cmdCancel.Left = 0
    Me.cmdCancel.Top = 220
    Me.cmdCancel.Height = 30
    Me.cmdCancel.Width = Int(400 / 3)
    
    Me.cmdRefresh.Left = Me.cmdCancel.Width
    Me.cmdRefresh.Top = 220
    Me.cmdRefresh.Height = 30
    Me.cmdRefresh.Width = Int(400 / 3)
    
    Me.cmdApply.Left = Me.cmdCancel.Width + Me.cmdRefresh.Width
    Me.cmdApply.Top = 220
    Me.cmdApply.Height = Me.cmdCancel.Height
    Me.cmdApply.Width = Int(400 / 3)
    
    Me.prgLoad.max = 100
    Me.prgLoad.Min = 0
    Me.prgLoad.Width = Me.ScaleWidth
    Me.prgLoad.Height = Me.cmdApply.Height
    Me.prgLoad.Left = 0
    Me.prgLoad.Top = Me.cmdCancel.Top
    Me.prgLoad.Visible = False
End Sub


Private Sub LoadData(ByVal filename As String)
    On Error GoTo ERROR_HANDLER
    
    
    
    
    ctlGuriData.LoadData (filename)
    ctlGuriView.ControlWidth = 100 * Screen.TwipsPerPixelX
    ctlGuriView.ControlHeight = 100 * Screen.TwipsPerPixelY
    
    Dim Val() As String
    Dim su As New CStringUtil
    su.StringSprit ctlGuriData.GetMaxSize, ",", Val
    Dim max As Long
    max = Int(MaxVal(Val(0), Val(1), Val(2)))
    'MsgBox (max)
    
    
    ctlGuriView.ControlScaleWidth = max * 2
    ctlGuriView.ControlScaleHeight = max * 2
    ctlGuriView.Left = 0
    ctlGuriView.Top = 0
    ctlGuriView.FigColor = vbBlack
    ctlGuriView.BackColor = vbWhite
    ctlGuriView.Ratio = 1
    ctlGuriView.ViewCoordinates = False
    ctlGuriView.ViewPointerFrom = False
    ctlGuriView.ViewPointerTo = False
    
    Dim i As Long
        ctlGuriView.ClearData
        ctlGuriView.SetMaxSize (ctlGuriData.GetMaxSize)
        For i = 1 To ctlGuriData.GetNumberOfLines
            ctlGuriView.SetLine (ctlGuriData.GetLine(i))
        Next i
        ctlGuriView.SetLatitude 35
        ctlGuriView.SetLongitude -45
        ctlGuriView.Draw
Exit Sub
ERROR_HANDLER:
    MsgBox ("ERROR! データファイル" & filename & "は破損しています。" & Err.Description)
End Sub

Private Function MaxVal(A As Variant, B As Variant, C As Variant) As Variant
    Dim m As Variant
    m = 0
    If A >= m Then m = A
    If B >= m Then m = B
    If C >= m Then m = C
    MaxVal = m
End Function


'フォームのリサイズが起こったら
Private Sub Form_Resize()
    'Size Setting
'    Me.Width = 400 * Screen.TwipsPerPixelX
'    Me.Height = 250 * Screen.TwipsPerPixelY
    
    Me.ScaleMode = vbPixels
    Me.ScaleHeight = 250
    Me.ScaleWidth = 400
    Me.ScaleLeft = 0
    Me.ScaleTop = 0
    
    Me.lblDataPath.Left = 0
    Me.lblDataPath.Top = 0
    Me.lblDataPath.Height = 20
    Me.lblDataPath.Width = Me.ScaleWidth
        
    'リストビューコントロールの状態設定
    Me.lvwPreView.Left = 0
    Me.lvwPreView.Top = 20
    Me.lvwPreView.Height = 200
    Me.lvwPreView.Width = Me.ScaleWidth
    Me.lvwPreView.View = lvwIcon
    Me.lvwPreView.Arrange = lvwAutoTop

    
    Me.cmdCancel.Left = 0
    Me.cmdCancel.Top = 220
    Me.cmdCancel.Height = 30
    Me.cmdCancel.Width = Int(400 / 3)
    
    Me.cmdRefresh.Left = Me.cmdCancel.Width
    Me.cmdRefresh.Top = 220
    Me.cmdRefresh.Height = 30
    Me.cmdRefresh.Width = Int(400 / 3)
    
    Me.cmdApply.Left = Me.cmdCancel.Width + Me.cmdRefresh.Width
    Me.cmdApply.Top = 220
    Me.cmdApply.Height = Me.cmdCancel.Height
    Me.cmdApply.Width = Int(400 / 3)
    
    Me.prgLoad.max = 100
    Me.prgLoad.Min = 0
    Me.prgLoad.Width = Me.ScaleWidth
    Me.prgLoad.Height = Me.cmdApply.Height
    Me.prgLoad.Left = 0
    Me.prgLoad.Top = Me.cmdCancel.Top
End Sub


'
'Private Sub lvwPreView_ItemClick(ByVal Item As MSComctlLib.ListItem)
'    m_strSelectedFile = m_strDataPath & "\" & Item.Text
'    'MsgBox (m_strSelectedFile)
'    Item.Ghosted = True
'    If Not (m_lngSelectedItemIndex = Item.index) And m_lngSelectedItemIndex <> 0 Then
'        Me.lvwPreView.ListItems.Item(m_lngSelectedItemIndex).Ghosted = False
'    End If
'    m_lngSelectedItemIndex = Item.index
'
'End Sub



Private Sub lvwPreView_ItemClick(ByVal Item As ComctlLib.ListItem)
    m_strSelectedFile = Item.Text
    'MsgBox (m_strSelectedFile)
    Item.Ghosted = True
    If Not (m_lngSelectedItemIndex = Item.index) And m_lngSelectedItemIndex <> 0 Then
        Me.lvwPreView.ListItems.Item(m_lngSelectedItemIndex).Ghosted = False
    End If
    m_lngSelectedItemIndex = Item.index
End Sub
