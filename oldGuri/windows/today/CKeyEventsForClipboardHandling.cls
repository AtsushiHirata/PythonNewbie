VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CKeyEventsForClipboardHandling"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

' キーストロークを合成する関数の作成
Private Declare Sub keybd_event Lib "user32.dll" _
   (ByVal bVk As Byte, _
    ByVal bScan As Byte, _
    ByVal dwFlags As Long, _
    ByVal dwExtraInfo As Long)

' キーコード定数
Const VK_LMENU = &HA4
Const VK_SNAPSHOT = &H2C

' キーボードイベントフラグ定数
Const KEYEVENTF_EXTENDEDKEY = &H1
Const KEYEVENTF_KEYUP = &H2


Public Sub CopyEntireFormToClipboard()
    Clipboard.Clear
    'Alt Key Press
    keybd_event VK_LMENU, _
                0, _
                KEYEVENTF_EXTENDEDKEY, _
                0
    'PrintScreen Key Press
    keybd_event VK_SNAPSHOT, _
                0, _
                KEYEVENTF_EXTENDEDKEY, _
                0
    DoEvents
    
    'PrintScreen Key Release
    keybd_event VK_SNAPSHOT, _
                0, _
                KEYEVENTF_EXTENDEDKEY Or _
                KEYEVENTF_KEYUP, _
                0
    'Alt Key Release
    keybd_event VK_LMENU, _
                0, _
                KEYEVENTF_EXTENDEDKEY Or _
                KEYEVENTF_KEYUP, _
                0
End Sub
