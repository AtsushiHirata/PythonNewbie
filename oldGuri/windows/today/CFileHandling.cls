VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFileHandling"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'==========================================================
'Name:        FileHandling ファイル操作に便利なメソッドを集めた
'Author:      平田 敦(a_hirata@siscom.or.jp)
'CreateDate:  2003/02/25
'ChangeDate:
'Revision:


Private Const m_lngMaxSize = 32767     'デフォルトで処理可能な最大ファイル数



'------------------------------------------------
Public Function FileExists(ByVal filename As String) As Boolean
'目的：引数Filenameで指定されたファイルがあるかどうかを返す。あれば真
'引数：Filename As String   ファイル名。フルパス。
'返値：Boolean
'------------------------------------------------
    On Error GoTo ERROR_HANDLER
    Dim fs As Object
    Set fs = CreateObject("Scripting.FileSystemObject")
    If fs.FileExists(filename) Then
        FileExists = True
    Else
        FileExists = False
    End If
Exit Function
ERROR_HANDLER:
    'エラーです。
    MsgBox ("CFileHandling.FileExistsで予期せぬエラーが発生しました。" & vbCrLf & _
            Err.Description)
    '返値は「そのファイルは存在しない」としておく。
    FileExists = False
End Function


'------------------------------------------------
Public Function FolderExists(ByVal FolderName As String) As Boolean
'目的：引数Foldernameで指定されたフォルダがあるかどうかを返す。あれば真
'引数：Foldername As String   フォルダ名。フルパス。
'返値：Boolean
'------------------------------------------------
    On Error GoTo ERROR_HANDLER
    Dim fs As Object
    Set fs = CreateObject("Scripting.FileSystemObject")
    If fs.FolderExists(FolderName) = True Then
        FolderExists = True
    Else
        FolderExists = False
    End If
Exit Function
ERROR_HANDLER:
    'エラーです。
    MsgBox ("CFileHandling.FolderExistsで予期せぬエラーが発生しました。" & vbCrLf & _
            Err.Description)
    '返値は「そのフォルダは存在しない」としておく。
    FolderExists = False
End Function



'------------------------------------------------
Public Function CreateFolder(ByVal FolderName As String) As Boolean
'目的：指定された名前のフォルダを作成する。
'引数：FolderName As String フォルダ名　フルパスで
'戻値：成功したらTrueを返す。
'------------------------------------------------
    On Error GoTo ERROR_HANDLER
    Dim fs As Object
    Set fs = CreateObject("Scripting.FileSystemObject")
    If fs.CreateFolder(FolderName) = True Then
        CreateFolder = True
    Else
        CreateFolder = False
    End If
Exit Function
ERROR_HANDLER:
    'エラーです。
    MsgBox ("CFileHandling.CreateFolderで予期せぬエラーが発生しました。" & vbCrLf & _
            Err.Description)
    '返値は「フォルダ作成は失敗」としておく。
    CreateFolder = False
End Function


'------------------------------------------------
Public Function TrimUpperFoldername(ByVal Longname As String) As String
'目的：フルパスのファイルネームから、ファイル名＋拡張子の部分を返す。
'引数：Longname As String   フルパスファイル名。
'戻値：上位のパスを切り取られたファイル名＋拡張子の部分。文字列。
'------------------------------------------------
    Dim i As Long
    Dim name_length As Long
    Dim path_pos As Long
    Dim tempchar As Variant
    
    name_length = Len(Longname)
    
    For i = 1 To name_length
        tempchar = Mid(Longname, i, 1)
        If tempchar = "\" Then path_pos = i
        'none
    Next i
    
    If path_pos = name_length Then
        '\がないということは、すなわち、ルート。しかし、C:\などとなるから、ありえないが・・・。
        TrimUpperFoldername = Longname
    Else
        TrimUpperFoldername = Right$(Longname, name_length - path_pos)
    End If
End Function

'------------------------------------------------
Public Function TrimExtension(ByVal filename As String) As String
'目的：引数で与えた文字列から、拡張子部分を切り取り、残りを返す。
'引数：Filename　ファイル名。
'戻値：文字列
'------------------------------------------------
    Dim i As Long
    Dim name_length As Long
    Dim dot_pos As Long
    Dim tempchar As Variant
    
    name_length = Len(filename)
    
    For i = name_length To 1 Step -1
        tempchar = Mid(filename, i, 1)
        If tempchar = "." Then
            dot_pos = i
            'MsgBox ("Pos is " & i)
            Exit For
        End If
    Next i
    
    If dot_pos = 1 Then
        '.がないということは、拡張子がないということ。
        TrimExtension = filename
    ElseIf dot_pos > 1 Then
        TrimExtension = Left$(filename, dot_pos - 1)
    Else
        'ファイル名が指定されていない。
        TrimExtension = "ファイル名なし"
    End If
End Function



'------------------------------------------------
Public Sub GetFileList(ByVal Path As String, ByRef List() As String, Optional ByVal Mode As String)
'目的：Pathで指定されたフォルダの、ファイルリストを作成し、List()にセットする。
'引数：Path　一覧を作成するフォルダ名　フルパス。
'　　　List()　戻り値のファイルリストを格納する動的配列
'　　　Mode　フォルダ一覧を取る際のオプション。"*.*"なら、ファイルすべて。""なら、フォルダも含めて。"*.gri"ならグリグリだけ。
'------------------------------------------------
    On Error GoTo ERROR_HANDLER
    Dim fs As Object
    Set fs = CreateObject("Scripting.FileSystemObject")
    If fs.FolderExists(Path) = True Then
        'そのフォルダが存在する。
        'そのフォルダにあるファイル名をリストアップする。
    
        Dim strPATH3 As String          ' 一時利用
        Dim strFILE As String           ' Dir関数の戻り値格納用
        Dim i As Long                   'counter
        
        ReDim Preserve List(m_lngMaxSize)   'とりあえず、大きめにとる
        
        i = 0
        strPATH3 = Path & "\"
        strFILE = Dir(strPATH3 & Mode, vbNormal)
        'Debug.Print strFILE
        Do While strFILE <> ""

                List(i) = strFILE
                i = i + 1
                strFILE = Dir
            'Debug.Print strFILE
        
        Loop
        
        'MsgBox "ファイル数　：　" & i
        
        If i <> 0 Then
            ReDim Preserve List(i - 1)
        Else
            ReDim List(0)
        End If
    Else
        'そのフォルダは存在しない。
        ReDim List(0)
    End If
Exit Sub
ERROR_HANDLER:
    'エラーです。
    MsgBox ("CFileHandling.GetFileListで予期せぬエラーが発生しました。" & vbCrLf & _
            Err.Description)
    ReDim List(0)
End Sub
