VERSION 5.00
Begin VB.Form frmAbout 
   BorderStyle     =   4  '固定ﾂｰﾙ ｳｨﾝﾄﾞｳ
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   5445
   ClientTop       =   4635
   ClientWidth     =   4680
   BeginProperty Font 
      Name            =   "ＭＳ Ｐ明朝"
      Size            =   20.25
      Charset         =   128
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   213
   ScaleMode       =   3  'ﾋﾟｸｾﾙ
   ScaleWidth      =   312
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton bot 
      Caption         =   "OK"
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   9
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1800
      TabIndex        =   0
      Top             =   2520
      Width           =   2535
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   9
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   480
      TabIndex        =   3
      Top             =   1560
      Width           =   3015
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   9
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   480
      TabIndex        =   2
      Top             =   960
      Width           =   3015
   End
   Begin VB.Label Label1 
      Caption         =   "Label1"
      BeginProperty Font 
         Name            =   "ＭＳ Ｐゴシック"
         Size            =   9
         Charset         =   128
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   480
      TabIndex        =   1
      Top             =   360
      Width           =   3015
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Option Explicit
''==========================================================
''ファイル名     frmAbout.frm
''オブジェクト名 frmAbout
''目的          スプラッシュ、バージョン表示などをおこなう
''               フォーム
''作者           平田 敦（vff01137@nifty.ne.jp)
''最終更新日    98/08/13
''==========================================================
''
'

''PRIVATE:****************************************
'
'************************************************
Private Sub bot_Click()
'目的説明:  フォームに配置されているボタンがクリックされたときの処理。
'入力引数:  なし
'戻り値:    なし
'------------------------------------------------

    UnLoad frmAbout 'frmAboutを閉じる
End Sub
'************************************************
'
'
'------------------------------------------------
Public Sub Terminate()
'なぜか、インスタンスを消すのに、普通にunloadコマ
'ンドを外部から呼びたかったが、できなかったので、
'不本意ながら、このメソッドを加えた。
'------------------------------------------------
    UnLoad frmAbout
End Sub
'
'
'
'
'************************************************
Public Sub ShowWithModePicSound(ByVal Mode As String, ByVal Pic As String, ByVal Sound As String)
'目的説明:      modeによって、スプラッシュか、バージョン情報を表示
'
'入力引数:      Mode    "SPLASH"    スプラッシュモード
'                       "VERSION"   バージョン表示モード
'               Pic                 そのとき表示するビットマップファイルへのフルパス
'               Sound               そのとき鳴らすWAVファイルへのフルパス
'戻り値:        なし
'------------------------------------------------
    'vismode = 0 バージョン表示
    'vismode = 1 スプラッシュ

    'バージョン表示
    Dim x As Integer, y As Integer, i As Integer
    
    Dim m_WavePlay As CWavePlay
    Set m_WavePlay = New CWavePlay
    
    Screen.MousePointer = vbHourglass

    Load Me

    'フォームの大きさを設定する
    frmAbout.ScaleMode = 3  'ピクセル
    x = Screen.TwipsPerPixelX
    frmAbout.Width = frmAbout.Width - _
        (frmAbout.ScaleWidth * x) + 320 * x
    y = Screen.TwipsPerPixelY
    frmAbout.Height = frmAbout.Height - _
        (frmAbout.ScaleHeight * y) + 240 * y

    frmAbout.Left = (Screen.Width - frmAbout.Width) / 2
    frmAbout.Top = (Screen.Height - frmAbout.Height) / 2

    set_contsize frmAbout.bot, 280, 30, 20, 200 'OKボタン
    set_contsize frmAbout.Label1(0), 280, 15, 20, 148 '結果表示ラベル
    set_contsize frmAbout.Label1(1), 280, 15, 20, 166
    set_contsize frmAbout.Label1(2), 280, 15, 20, 183

    'フォントの大きさ
    For i = 0 To 2
        frmAbout.Label1(i).FontSize = Int(12 * x / 15)
    Next i
    frmAbout.FontSize = Int(12 * x / 15)
    Screen.MousePointer = vbDefault
    '

    'frmAbout.MMControl1.Visible = False

    '
    Select Case Mode
        Case "VERSION"  'バージョン情報
            frmAbout.Caption = VERSION_STRING
            frmAbout.Picture = LoadPicture(Pic)
            frmAbout.Label1(0).Visible = False
            frmAbout.Label1(1).Visible = False
            frmAbout.Label1(2).Visible = False
            frmAbout.bot.Visible = True
'            wave_play soundSplash, 0
'            m_WavePlay.Play Sound, "SYNC"
            frmAbout.Show
            DoEvents
'            wave_play soundSplash, 1
           m_WavePlay.Play Sound, "SYNC"
 '            m_WavePlay Sound, "SYNC"
            'frmAbout.MMControl1.filename = App.Path & "guri.mid"
            'frmAbout.MMControl1.Command = "Open"
            'frmAbout.MMControl1.Command = "Prev"
            'frmAbout.MMControl1.Command = "Play"

        Case "SPLASH"  'スプラッシュ
            'frmAbout.BorderStyle = vbBSNone
            frmAbout.Caption = VERSION_STRING
            frmAbout.Picture = LoadPicture(Pic)
            frmAbout.Label1(0).Visible = False
            frmAbout.Label1(1).Visible = False
            frmAbout.Label1(2).Visible = False
            frmAbout.bot.Visible = False
            '
            'wave_play soundSplash, 0
            frmAbout.Show
            DoEvents
            'wave_play soundSplash, 1
            m_WavePlay.Play Sound, "SYNC"
            'DoEvents

    End Select
End Sub
'************************************************

'************************************************
Private Sub set_contsize(cont As Control, co_width As Integer, _
                 co_height As Integer, co_left As Integer, co_top As Integer)
'目的説明:  contで受け取ったコントロールのサイズを設定する。
'
'入力引数:
'           cont As Control         コントロールを受け取る
'           co_width As Integer     コントロールの幅
'           co_height As Integer    コントロールの高さ
'           co_left As Integer      コンテナ左上すみから横へ
'           co_top As Integer       コンテナ左上すみから下へ
'
'戻り値:    なし
'------------------------------------------------
'藤田氏、omimi氏の文献を参考  1998/02/05
    cont.Move co_left, co_top, co_width, co_height
End Sub
'************************************************
'


