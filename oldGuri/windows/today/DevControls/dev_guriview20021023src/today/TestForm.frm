VERSION 5.00
Object = "{DB453C48-DD68-4BF0-B92B-05C3FDB4F141}#32.0#0"; "ctlGuriData.ocx"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "*\A..\..\..\..\..\..\..\..\WORKPR~2\VB\Guri\DEV_GU~1.3\today\DEVCON~1\DEV_GU~1\today\DevGuriViewControl.vbp"
Begin VB.Form frmTestForm 
   Caption         =   "Form1"
   ClientHeight    =   4545
   ClientLeft      =   165
   ClientTop       =   735
   ClientWidth     =   5670
   LinkTopic       =   "Form1"
   ScaleHeight     =   4545
   ScaleWidth      =   5670
   StartUpPosition =   3  'Windows の既定値
   Begin VB.CommandButton Command4 
      Caption         =   "CopyToClipboard"
      Height          =   255
      Left            =   3360
      TabIndex        =   10
      Top             =   4200
      Width           =   1695
   End
   Begin DevGuriViewControlProject.ctlGuriView ctlGuriView 
      Height          =   2535
      Left            =   360
      TabIndex        =   9
      Top             =   120
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   4471
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   4800
      Top             =   2880
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command3 
      Caption         =   "線の太さ"
      Height          =   375
      Left            =   2160
      TabIndex        =   8
      Top             =   4080
      Width           =   855
   End
   Begin VB.TextBox txtFigLineSize 
      Height          =   375
      Left            =   240
      TabIndex        =   7
      Text            =   "Text2"
      Top             =   4080
      Width           =   1695
   End
   Begin VB.TextBox Text1 
      Height          =   495
      Left            =   3480
      TabIndex        =   6
      Text            =   "Text1"
      Top             =   3000
      Width           =   975
   End
   Begin VB.Timer tmrTurn 
      Interval        =   1000
      Left            =   4800
      Top             =   3480
   End
   Begin VB.CommandButton Command2 
      Caption         =   "再描画"
      Height          =   375
      Left            =   3360
      TabIndex        =   4
      Top             =   3600
      Width           =   975
   End
   Begin VB.CommandButton Command1 
      Caption         =   "色変更"
      Height          =   375
      Left            =   2160
      TabIndex        =   3
      Top             =   3600
      Width           =   855
   End
   Begin VB.TextBox txtBackColor 
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Text            =   "Text2"
      Top             =   3600
      Width           =   1695
   End
   Begin VB.TextBox txtFigColor 
      Height          =   375
      Left            =   240
      TabIndex        =   1
      Text            =   "Text1"
      Top             =   3000
      Width           =   1695
   End
   Begin GuriData_UserControl.GuriData ctlGuriData 
      Height          =   480
      Left            =   4800
      TabIndex        =   0
      Top             =   240
      Width           =   480
      _ExtentX        =   847
      _ExtentY        =   847
   End
   Begin VB.Label Label1 
      Caption         =   "Timer"
      Height          =   255
      Left            =   2760
      TabIndex        =   5
      Top             =   3120
      Width           =   615
   End
   Begin VB.Menu mnuFile 
      Caption         =   "ファイル"
      Begin VB.Menu mnuFileLoad 
         Caption         =   "ロード"
      End
      Begin VB.Menu mnuFileSave 
         Caption         =   "セーブ"
      End
   End
End
Attribute VB_Name = "frmTestForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const VERSION = "立体グリグリforWindows Ver3.25"
Dim tlat As Long
Dim tlon As Long

Private Sub Command1_Click()
    Me.ctlGuriView.BackColor = CLng(Me.txtBackColor.Text)
    Me.ctlGuriView.FigColor = CLng(Me.txtFigColor.Text)
End Sub

Private Sub Command2_Click()
    ctlGuriView.Draw
End Sub

Private Sub Command3_Click()
    ctlGuriView.FigLineSize = CLng(txtFigLineSize.Text)
End Sub


Private Sub Command4_Click()
    ctlGuriView.CopyToClipboard
End Sub

Private Sub Form_Load()
    frmTestForm.Show
    
    'ctlGuriViewの設定
    ctlGuriView.Left = 10 * Screen.TwipsPerPixelX
    ctlGuriView.Top = 10 * Screen.TwipsPerPixelY
'    ctlGuriView.Width = Me.Width
'    ctlGuriView.Height = Me.Height
    ctlGuriView.ControlWidth = 400 * Screen.TwipsPerPixelX
    ctlGuriView.ControlHeight = 200 * Screen.TwipsPerPixelY
    
    ctlGuriView.ControlScaleWidth = 400
    ctlGuriView.ControlScaleHeight = 200
    
    ctlGuriView.AxisLineSize = 3
    ctlGuriView.SubAxisLineSize = 1
    
    '図形データの読み込み
    ctlGuriData.LoadData (App.Path & "/data/ｷｰﾎﾞｰﾄﾞ.GRI")
    ctlGuriView.SetComment ctlGuriData.GetComment
    ctlGuriView.SetMaxSize ctlGuriData.GetMaxSize
    
    Dim i As Long
    For i = 1 To ctlGuriData.GetNumberOfLines
        ctlGuriView.SetLine ctlGuriData.GetLine(i)
        Debug.Print "Line(", i, "):", ctlGuriData.GetLine(i)
    Next i
    
    Me.txtBackColor.Text = CStr(ctlGuriView.BackColor)
    Me.txtFigColor.Text = CStr(ctlGuriView.FigColor)
    
    Dim result As Variant
    ctlGuriView.PointerFrom = "50,50,50"
    ctlGuriView.PointerTo = "100,100,100"
    ctlGuriView.PointerSize (2)
    ctlGuriView.PointerFromColor = vbRed
    ctlGuriView.PointerToColor = vbBlack
    
    ctlGuriView.ViewTempLine (True)
    ctlGuriView.TempLine = "50,50,50,100,100,100"
    ctlGuriView.viewtempline2 (True)
    ctlGuriView.templine2 = "20,100,150,100,80,20"
    
    ctlGuriView.PointerLineSize = 3
    ctlGuriView.Draw
    
End Sub






Private Sub mnuFileLoad_Click()
On Error GoTo EXITSUB
    CommonDialog1.CancelError = True
    Dim filename As String
    CommonDialog1.InitDir = App.Path
    CommonDialog1.Filter = "ぐりぐりデータ (*.gri)|*.gri"
    CommonDialog1.FilterIndex = 1
    CommonDialog1.ShowOpen
    filename = CommonDialog1.filename
    ctlGuriView.ClearData
    ctlGuriData.LoadData filename
    
    ctlGuriView.SetComment ctlGuriData.GetComment
    ctlGuriView.SetMaxSize ctlGuriData.GetMaxSize
    
    Dim i As Long
    For i = 1 To ctlGuriData.GetNumberOfLines
        ctlGuriView.SetLine ctlGuriData.GetLine(i)
        Debug.Print "Line(", i, "):", ctlGuriData.GetLine(i)
    Next i
EXITSUB:
End Sub






Private Sub mnuFileSave_Click()
On Error GoTo EXITSUB
    CommonDialog1.CancelError = True
    Dim filename As String
    CommonDialog1.InitDir = App.Path
    CommonDialog1.Filter = "ぐりぐりデータ (*.gri)|*.gri"
    CommonDialog1.FilterIndex = 1
    CommonDialog1.ShowSave
    filename = CommonDialog1.filename
    ctlGuriData.SetComment (VERSION & "　" & Date)
    ctlGuriData.SaveData filename
    
EXITSUB:

End Sub

Private Sub Text1_Change()
    If Not IsNumeric(Text1.Text) Then
        'なにもしなーい。
    ElseIf (CLng(Text1.Text) <= 0) Then
        Me.tmrTurn.Enabled = False
    Else
        Me.tmrTurn.Enabled = True
        Me.tmrTurn.Interval = CLng(Text1.Text)
    End If
End Sub

Private Sub tmrTurn_Timer()
    tlat = tlat + 10
    If tlat > 360 Then tlat = 0
    Me.ctlGuriView.SetLongitude tlat
    tlon = tlon + 10
    If tlon > 360 Then tlon = 0
    Me.ctlGuriView.SetLatitude tlon
    Static tempsize As Single
    If tempsize = 0.1 Then tempsize = 5 Else tempsize = 0.1
    Me.ctlGuriView.ratio = tempsize
    Me.ctlGuriView.PointerSize (2)
    ctlGuriView.AxisLineLength = tlat
    ctlGuriView.Draw
End Sub
