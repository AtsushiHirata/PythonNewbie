Attribute VB_Name = "ApiCodeModule"
Option Explicit
'==========================================================
'Name:      ApiCodeModule
'Author:    平田 敦(vff01137@nifty.ne.jp)
'Date:      98/08/14
'Revision:

'====================================================================
'
'           立体グリグリに用いるAPI関数の定義
'
'====================================================================
'
'
'------------------------------
'   絵の重ね合わせに用いる関数
'------------------------------
Declare Function BitBlt Lib "gdi32" _
            (ByVal hDestDC As Long, ByVal x As Long, _
             ByVal y As Long, ByVal nWidth As Long, _
             ByVal nHeight As Long, ByVal hSrcDC As Long, _
             ByVal xSrc As Long, ByVal ySrc As Long, _
             ByVal dwRop As Long) As Long
Public Const SRCCOPY = &HCC0020
Public Const BLACKNESS = &H42
Public Const SRCINVERT = &H660046
Public Const SRCERASE = &H440328
'
Declare Function StretchBlt Lib "gdi32" _
            (ByVal hdc As Long, _
             ByVal x As Long, ByVal y As Long, _
             ByVal nWidth As Long, ByVal nHeight As Long, _
             ByVal hSrcDC As Long, _
             ByVal xSrc As Long, ByVal ySrc As Long, _
             ByVal nSrcWidth As Long, ByVal nSrcHeight As Long, _
             ByVal dwRop As Long) As Long
'


'------------------------------
'         音を鳴らす関数
'------------------------------
Declare Function sndPlaySound Lib "winmm.dll" Alias "sndPlaySoundA" _
    (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Public Const SND_ASYNC = &H1    '非同期再生
Public Const SND_SYNC = &H0     '同期再生
Public Const SND_LOOP = &H8     'ループ再生
Public Const SND_MEMORY = &H4   'メモリ上のサウンドを再生
Public Const SND_NODEFAULT = &H2    '失敗してもシステムサウンドを再生しない。
Public Const SND_NOSTOP = &H10  '再生中なら再生しない。
'
'------------------------------
'      ヘルプの呼び出し
'------------------------------
Declare Function WinHelp Lib "user32" Alias "WinHelpA" _
    (ByVal hwnd As Long, ByVal lphelpfile As String, _
    ByVal wCommand As Long, ByVal dwData As Long) As Long
Public Const HELP_INDEX = &H3   'メニューページの呼び出し
Public Const HELP_CONTEXT = &H1 '特定のページを呼び出す
'
'Public Const cntHelpFile = "./help/guriwin30a.hlp"    'ヘルプファイルの名前
'
'
'
'====================================================================
'
'       APIを使いやすくする関数
'
'====================================================================
'************************************************
Sub wave_play(fnm As String, sw As Integer)
'目的説明:      wavファイルを再生。同期と非同期を選択できる。
'
'入力引数:      fnm As String       '再生するファイル名フルパス
'                sw As Integer       '再生モード。
'戻り値:        なし
'------------------------------------------------
    Dim ret As Integer
    '
    If sw = 0 Then  '非同期再生
        ret = sndPlaySound(App.Path + "/" + fnm, SND_ASYNC Or SND_NODEFAULT)
    Else            '同期再生
        ret = sndPlaySound(App.Path + "/" + fnm, SND_SYNC Or SND_NODEFAULT)
    End If
End Sub
'************************************************

