VERSION 5.00
Begin VB.UserControl ctlGuriView 
   BackColor       =   &H8000000A&
   ClientHeight    =   3075
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   2475
   ScaleHeight     =   3075
   ScaleWidth      =   2475
   Begin VB.PictureBox picView 
      Height          =   1335
      Left            =   720
      ScaleHeight     =   85
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   93
      TabIndex        =   1
      Top             =   1560
      Width           =   1455
   End
   Begin VB.PictureBox picBuffer 
      Height          =   1335
      Left            =   720
      ScaleHeight     =   85
      ScaleMode       =   3  'ﾋﾟｸｾﾙ
      ScaleWidth      =   93
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
   Begin VB.Image Image1 
      Height          =   480
      Left            =   120
      Picture         =   "ctlGuriView.ctx":0000
      Top             =   120
      Width           =   480
   End
End
Attribute VB_Name = "ctlGuriView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit


'================================================
'Name:        ctlGuriView 図形表示を一手に引き受ける
'             あるていどのデータも保持する
'Author:      平田 敦(a_hirata@siscom.or.jp)
'CreateDate:  2003/01/28
'ChangeDate:
'Revision:


'------------------------------------------------
'定数
'------------------------------------------------
Private Const PI = 3.1415
Private Const LightBlue = 16763904         '= RGB(0,204,255)    薄い青 for X axis

'------------------------------------------------
' ユーザ定義型
'------------------------------------------------

'線一本分のデータを保持するための型
Private Type Type_A_Line
    From_x As Single
    From_y As Single
    From_z As Single
    To_x As Single
    To_y As Single
    To_z As Single
End Type

'ひとつの座標を保持するための型
Private Type Type_A_Pos
    x As Single
    y As Single
    Z As Single
End Type


'------------------------------------------------
' Data Fields
'------------------------------------------------

'ピクチャボックスの寸法を保持（単位　ピクセル）
Private m_ControlScaleWidth As Long      'ピクチャボックスの中の寸法「横幅：ピクセル」
Private m_ControlScaleHeight As Long     'ピクチャボックスの中の寸法「たて：ピクセル」


'Private m_booEnablePointer As Boolean   '作図ポインタを表示するかどうか。
'                                        'デフォルトは表示する。
'Private m_booEnableAxisLine As Boolean  '座標軸を表示するかどうか
'                                        'デフォルトは表示する。

Dim m_Lines() As Type_A_Line
'この配列については１からカウントする。
'０番については放置。
'そのほうが線の本数が１５本なら１から１５までループでわかりやすいかな。

Dim m_TLines() As Type_A_Line
'座標変換された後の座標値

Dim m_longitude As Long     '経度
Dim m_latitude As Long      '緯度

Dim m_xmax As Single        '最大寸法　最大座標ではない。
Dim m_ymax As Single
Dim m_zmax As Single

Dim m_Txmax As Single       '変換後の最大寸法
Dim m_Tymax As Single
Dim m_Tzmax As Single



Dim m_Ratio As Single       '図形の拡大表示率


Dim m_FigColor As Long      '図形の線の色
Dim m_BackColor As Long     '背景の色
Dim m_AxisColor(3) As Long    '軸の色

Dim m_figaxislinesize As Long   '座標軸の太さ

Dim m_FigLineSize As Long   '図形の線の太さ
Dim m_AxisLineSize As Long  '座標軸の線の太さ
Dim m_SubAxisLineSize As Long '補助座標軸の線の太さ

Dim m_AxisLength As Long    '座標軸の長さ
Dim m_SubAxisLength As Long   '補助座標軸の長さ

Dim m_ViewAxis As Boolean   '座標軸を表示するかどうか
Dim m_ViewSubAxis As Boolean '補助座標軸を表示するかどうか

Dim m_AxisLine(3) As Type_A_Line  '座標軸座標　もとの座標値
Dim m_TAxisLine(3) As Type_A_Line '座標軸座標　変換後の座標

Dim m_SubAxisLine(3) As Type_A_Line '補助座標軸座標　もとの座標値
Dim m_TSubAxisLine(3) As Type_A_Line '補助座標軸座標　変換後の座標値


Dim m_PointerFrom As Type_A_Pos     '描画ポインタ、始点の座標
Dim m_TPointerFrom As Type_A_Pos    '描画ポインタ、変換後の始点の座標
Dim m_PointerTo As Type_A_Pos       '描画ポインタ、終点の座標
Dim m_TPointerTo As Type_A_Pos      '描画ポインタ、変換後の終点の座標
Dim m_ViewPointerFrom As Boolean    '描画ポインタ、始点を表示するか
Dim m_ViewPointerTo As Boolean      '描画ポインタ、終点を表示するかどうか
Dim m_PointerSize                   '描画ポインタの大きさ
Dim m_PointerFromColor As Long      '描画ポインタ、始点の色
Dim m_PointerToColor As Long        '描画ポインタ、終点の色

Dim m_PointerLineSize As Integer    '描画ポインタの線の太さ

Dim m_TempLine As Type_A_Line       '未確定の一時的な線分をあらわす
Dim m_TTempLine As Type_A_Line      '未確定の一時的な線分、変換後の座標
Dim m_TempLineSize As Long          '未確定の一時的な線分の太さ
Dim m_ViewTempLine As Boolean       '未確定の一時的な線分を表示するかどうか
Dim m_TempLineColor As Long          '未確定の一時的な線分の色

'やがて円弧の描画が必要になったときのために
Dim m_TempLine2 As Type_A_Line       '２つめの未確定の一時的な線分をあらわす
Dim m_TTempLine2 As Type_A_Line      '２つめの未確定の一時的な線分、変換後の座標
Dim m_TempLineSize2 As Long          '２つめの未確定の一時的な線分の太さ
Dim m_ViewTempLine2 As Boolean       '２つめの未確定の一時的な線分を表示するかどうか
Dim m_TempLineColor2 As Long          '２つめの未確定の一時的な線分の色


Dim m_Comment As String     'コメント


Dim m_booViewCoordinates As Boolean '座標数値を表示するか否か　デフォルトTrue

'================================================
Public Function SetLine(a_line As String) As Boolean
'線分データを受け取り、内部の配列データとして取り込む。
'成功したらTrue。失敗したらFalse。
'------------------------------------------------
    Dim pos() As String
    
    StrSplit a_line, ",", pos
    ReDim Preserve m_Lines(UBound(m_Lines) + 1)
    With m_Lines(UBound(m_Lines))
        .From_x = CSng(pos(0))
        .From_y = CSng(pos(1))
        .From_z = CSng(pos(2))
        .To_x = CSng(pos(3))
        .To_y = CSng(pos(4))
        .To_z = CSng(pos(5))
    End With
    

    
    'MsgBox "現在の配列m_Linesのサイズは" & UBound(m_Lines) & "です", vbOKOnly
    SetLine = True
    
End Function
'================================================


'================================================
Public Function Draw() As Boolean
'保持している図形データを内在コントロールのピクチャ
'ボックスに図形として描画する。
'先ず、picBufferに。それをpicViewにBitBlit。
'成功したらTrue。とはいえ、先ず失敗などないだろうけど。
'================================================
    
    '--------------------------------------------
    'テストルーチン
'    Dim i As Long
'    Dim temp_str As String
'    If UBound(m_Lines) = 0 Then
'        temp_str = "データはありません"
'    Else
'        temp_str = "Max(" & m_xmax & "," & m_ymax & "," & _
'                    m_zmax & ")" & vbCrLf & _
'                    m_Comment & vbCrLf
'        For i = 1 To UBound(m_Lines)
'            'temp_str = temp_str + Str(i) + " : " + _
'                                Str(m_Lines(i).From_x) + "," + _
'                                Str(m_Lines(i).From_y) + "," + _
'                                Str(m_Lines(i).From_z) + "," + _
'                                Str(m_Lines(i).To_x) + "," + _
'                                Str(m_Lines(i).To_y) + "," + _
'                                Str(m_Lines(i).To_z) + "," + _
'                                vbCrLf
'        Next i
'    End If
    '.Text = temp_str
    'テストルーチン
    '--------------------------------------------
    Call Rotation
    Call DrawToPicBuffer
    Call CopyBufferToView
    
    Draw = True
    
    'ここでウインドウズに処理を返しておく
    '速度低下につながるが、まあ、仕方ない
    'DoEvents
    
End Function
'================================================



'================================================
Public Function ClearData() As Boolean
'保持していたデータを放棄する
'================================================
    ReDim m_Lines(0)
    m_xmax = 0
    m_ymax = 0
    m_zmax = 0
    
End Function
'================================================



Private Sub UserControl_Initialize()
    
    '変数初期化
    ReDim m_Lines(0)
    m_longitude = -45
    m_latitude = 30
    
    m_Ratio = 1
    
    m_FigColor = vbBlack
    m_BackColor = vbWhite
    
    
    picBuffer.ScaleMode = vbPixels
    
    picView.Width = UserControl.Width
    picView.Height = UserControl.Height
    picBuffer.Width = picView.Width
    picBuffer.Height = picView.Height
    
    picView.Left = 0
    picView.Top = 0
    
    picBuffer.Left = picView.Width
    picBuffer.Top = picView.Height
    
    picBuffer.AutoRedraw = True
    picView.AutoRedraw = True
    
    m_FigLineSize = 1
    m_PointerLineSize = 2
    m_AxisLineSize = 1
    m_SubAxisLineSize = 1
    
    m_AxisLength = 200
    m_SubAxisLength = 32767 'Integerの最大値
    
    'm_ViewAxis = False
    m_ViewAxis = True
    m_ViewSubAxis = True
    
    m_AxisLine(1).From_x = m_AxisLength
    m_AxisLine(2).From_y = m_AxisLength
    m_AxisLine(3).From_z = m_AxisLength
    m_AxisColor(1) = LightBlue
    m_AxisColor(2) = vbYellow
    m_AxisColor(3) = vbGreen
    m_SubAxisLine(1).From_x = m_SubAxisLength
    m_SubAxisLine(2).From_y = m_SubAxisLength
    m_SubAxisLine(3).From_z = m_SubAxisLength
    
    m_booViewCoordinates = True
    
    
    'm_PointerFrom     '描画ポインタ、始点の座標
    'm_PointerTo       '描画ポインタ、終点の座標
    m_ViewPointerFrom = True   '描画ポインタ、始点を表示するか
    m_ViewPointerTo = True       '描画ポインタ、終点を表示するかどうか
    m_PointerSize = 10          '描画ポインタの大きさ
    m_PointerFromColor = vbRed      '描画ポインタ、始点の色
    m_PointerToColor = vbMagenta        '描画ポインタ、終点の色
    'm_TempLine As Type_A_Line       '未確定の一時的な線分をあらわす
    m_ViewTempLine = True        '未確定の一時的な線分を表示するかどうか
    m_TempLineColor = vbRed          '未確定の一時的な線分の色
    m_TempLineSize = 1
    m_ViewTempLine2 = True        '未確定の一時的な線分を表示するかどうか
    m_TempLineColor2 = vbBlue          '未確定の一時的な線分の色
    m_TempLineSize2 = 1


    m_ControlScaleWidth = 200: picBuffer.ScaleWidth = 200
    m_ControlScaleHeight = 200: picBuffer.ScaleHeight = 200
    
    'm_booEnablePointer = True   '作図ポインタ表示
    'm_booEnableAxisLine = True  '座標軸の表示
    
End Sub














'------------------------------------------------------
'===== StrSplit Module =====
'(C)1999-2001 けるべ
'MAIL : kelbe@geocities.co.jp
'HOME : http://www.geocities.co.jp/SilkRoad/4511/

'引数 lngDefaultMaxArray を省略した場合に使用される値です。
'詳しくは下の StrSplit 関数仕様をご覧下さい.

'----- StrSplit 関数 Ver 1.05 -----
'
'文字列を特定の区切り文字で分割し、その結果を引数 strArray() で
'指定された文字列変数配列に格納します。詳しくはサンプルフォーム参照。
'
'引数 strExpression
'   必ず指定します。文字列と区切り文字を含んだ文字列式を指定します。
'   引数 strExpression が長さ 0 の文字列 ("") である場合、
'   StrSplit 関数は 0 を返します。
'
'引数 strDelimiter
'   文字列の区切りを識別する文字を指定します。引数 strDelimiter が
'   長さ 0 の文字列 ("") である場合は、引数 strExpression 全体の
'   文字列を含む単一の要素の配列を返します。指定された区切り文字が
'   存在しない場合も同様です。
'
'引数 strArray()
'   分割結果が格納される文字列型変数配列を指定します。StrSplit 関数を
'   呼び出すプロシージャ内で動的配列として宣言して渡して下さい。
'   配列サイズは関数内で初期化されるため、あらかじめ確保しておく
'   必要はありません。
'
'引数 lngCompare
'   省略可能です。文字列式を評価するときに使用する文字列比較の
'   モードを表す数値を指定します。省略した場合、バイナリ モードで
'   比較が行われます。
'
'   vbBinaryCompare : バイナリ モードで比較を行います。(0)
'   vbTextCompare   : テキスト モードで比較を行います。(1)
'
'引数 lngDefaultMaxArray
'   省略可能です。配列要素最大数の初期値を設定します。
'   もし配列サイズがここで設定した値では足りない場合、ここで設定した
'   個数ずつ配列サイズを増加させてゆきます。そのため分割項目数が
'   多い場合、この値を大きくすれば処理速度は向上しますが、
'   その分メモリを多く食います。予想される分割項目数に合わせて
'   調節して下さい。省略した場合、STRSPLIT_DAFAULT_MAXARRAY 定数の
'   値が使用されます。
'
'戻り値
'   成功した場合、配列 strArray() に格納した配列要素の
'   個数(要素最大値 + 1)を返します。
'   引数 strExpression に空の文字列を渡した場合、0 を返します。
'
'☆ StrSplit 関数と、VB6 の Split 関数との相違点
'
'   1.戻り値は配列の個数である。
'   2.デリミタ(区切り文字)は省略できない。
'   3.第 3 引数に宣言済み文字列変数配列を渡す。
'     (ここに分割結果が格納される)
'   4.count 引数はない。
'   5.配列個数初期値が設定できる。意味ねー(笑)
'
'戻り値が配列の個数となるため、Split 関数のように配列個数を
'UBound 関数で調べる必要もないため使いやすいと思います。
'処理速度はどっちが速いのかわかりません(^^;
'
'☆仕様変更
'
'Ver 1.03 より、返値を「配列の個数」に変更しました。
'Ver 1.02 以前のバージョンから使用されている方は注意して下さい。
'
'Ver 1.05 より、引数 strDelimiter で指定された区切り文字が
'存在しない場合は、引数 strExpression 全体の文字列を含む単一の要素の
'配列を返すように変更しました。(strArray(0) = strExpression)
'
'Ver 1.05 より、配列要素最大値の初期値を設定できるようにしました。
'末尾のオプション引数なので、省略しても何ら問題ありません。
'
Private Function StrSplit _
    (ByRef strExpression As String, _
     ByRef strDelimiter As String, _
     ByRef strArray() As String, _
     Optional ByVal lngCompare As Long = vbBinaryCompare, _
     Optional ByVal lngDefaultMaxArray As Long = 255) As Long

 Dim lngPos1 As Long     'InStr関数用検索開始位置
 Dim lngPos2 As Long     'InStr関数用文字検出位置
 Dim lngStrLen As Long   '検索される文字列のサイズ
 Dim lngDivLen As Long   '区切り文字のサイズ
 Dim lngCnt As Long      '項目数(=配列要素数)をあらわすカウンタ
 Dim lngMaxArray As Long '配列要素の最大数

    lngStrLen = Len(strExpression)  '元の文字列 strExpression の文字数を取得
    lngDivLen = Len(strDelimiter)   '区切り文字 strDelimiter の文字数を取得
    
    If lngStrLen = 0 Then           '引数 strExpression に空の文字列を渡した場合
        StrSplit = 0                '0 を返す
        Exit Function
    ElseIf lngDivLen = 0 Then       '引数 strDelimiter に空の文字列を渡した場合
        ReDim strArray(0)
        strArray(0) = strExpression '引数 strExpression を単一要素の配列として返す
        StrSplit = 1
        Exit Function
    End If
    
    lngMaxArray = lngDefaultMaxArray
    ReDim strArray(lngMaxArray)     '配列最大要素数を初期値にセット
    lngPos1 = 1                     '初期検索開始点を設定

    Do                                                                             '区切り文字が検出されなくなるまでループ
        If lngCnt > lngMaxArray Then                                               '項目数が配列要素最大数を超えてしまった場合
            lngMaxArray = lngMaxArray + lngDefaultMaxArray                         '配列要素最大数を lngDefaultMaxArray 増やす
            ReDim Preserve strArray(lngMaxArray)
        End If
        lngPos2 = InStr(lngPos1, strExpression, strDelimiter, lngCompare)          '区切り文字を検索しその位置を返す
        If lngPos2 Then                                                            '区切り文字が存在した場合
            strArray(lngCnt) = Mid$(strExpression, lngPos1, lngPos2 - lngPos1)     '検索開始点から(区切り文字位置 - 1)までの文字列をを配列に代入
            lngPos1 = lngPos2 + lngDivLen                                          '次回の検索開始点を設定
            lngCnt = lngCnt + 1                                                    '次回のため項目数を一つ増やす
        Else                                                                       '区切り文字が存在しない場合
            If lngCnt Then                                                         '最後の要素である場合
                strArray(lngCnt) = Right$(strExpression, lngStrLen - lngPos1 + 1)  '文字列の最後から検索開始点までの文字列を配列に代入
            Else                                                                   '区切り文字が全く存在しない場合
                strArray(lngCnt) = strExpression                                   '引数 strExpression を単一要素の配列として返す
                Exit Do
            End If
        End If
    Loop Until lngPos2 = 0

    ReDim Preserve strArray(lngCnt) '配列の余分な要素を削る
    StrSplit = lngCnt + 1           '配列の個数を返す

End Function












'************************************************
Private Sub Rotation()
'------------------------------------------------
    Dim i As Long                               'カウンタ
    Dim trash As Variant                        'ごみいれ
    Dim result As String: Dim pos() As String   '変換用一時変数
    
'    If m_booEnableAxisLine Then
        '表示される座標軸線の座標変換
        For i = 1 To 3
            '座標軸
            With m_AxisLine(i)
                result = RotateALine(CStr(.From_x) & "," & CStr(.From_y) & "," & CStr(.From_z) & "," & _
                            CStr(.To_x) & "," & CStr(.To_y) & "," & CStr(.To_z), _
                            m_longitude, m_latitude)
            End With
            StrSplit result, ",", pos
            With m_TAxisLine(i)
                .From_x = CSng(pos(0)): .From_y = CSng(pos(1)): .From_z = CSng(pos(2))
                .To_x = CSng(pos(3)): .To_y = CSng(pos(4)): .To_z = CSng(pos(5))
            End With
            '補助座標軸
            With m_SubAxisLine(i)
                result = RotateALine(CStr(.From_x) & "," & CStr(.From_y) & "," & CStr(.From_z) & "," & _
                            CStr(.To_x) & "," & CStr(.To_y) & "," & CStr(.To_z), _
                            m_longitude, m_latitude)
            End With
            StrSplit result, ",", pos
            With m_TSubAxisLine(i)
                .From_x = CSng(pos(0)): .From_y = CSng(pos(1)): .From_z = CSng(pos(2))
                .To_x = CSng(pos(3)): .To_y = CSng(pos(4)): .To_z = CSng(pos(5))
            End With
        Next i
'    End If
    
    '線分の座標変換
    ReDim m_TLines(UBound(m_Lines))
    For i = 1 To UBound(m_Lines)
        With m_Lines(i)
            result = RotateALine( _
                CStr(.From_x) & "," & CStr(.From_y) & "," & CStr(.From_z) & "," & _
                CStr(.To_x) & "," & CStr(.To_y) & "," & CStr(.To_z), _
                m_longitude, m_latitude)
        End With
        StrSplit result, ",", pos
        With m_TLines(i)
            .From_x = CSng(pos(0)): .From_y = CSng(pos(1)): .From_z = CSng(pos(2))
            .To_x = CSng(pos(3)): .To_y = CSng(pos(4)): .To_z = CSng(pos(5))
        End With
    Next i
    '最大寸法値の座標変換
    result = RotateALine(CStr(m_xmax) & "," & CStr(m_ymax) & "," & CStr(m_zmax) & "," & _
                        CStr(0) & "," & CStr(0) & "," & CStr(0), _
                        m_longitude, m_latitude)
    StrSplit result, ",", pos
    m_Txmax = CSng(pos(0)): m_Tymax = CSng(pos(1)): m_Tzmax = CSng(pos(2))
    trash = CSng(pos(3)): trash = CSng(pos(4)): trash = CSng(pos(5))
    
'    If m_booEnablePointer Then
        '作図ポインタの座標変換
        'from,to いっぺんに
        result = RotateALine(CStr(m_PointerFrom.x) & "," & CStr(m_PointerFrom.y) & "," & CStr(m_PointerFrom.Z) & "," & _
                    CStr(m_PointerTo.x) & "," & CStr(m_PointerTo.y) & "," & CStr(m_PointerTo.Z), _
                    m_longitude, m_latitude)
        StrSplit result, ",", pos
        m_TPointerFrom.x = CSng(pos(0)): m_TPointerFrom.y = CSng(pos(1)): m_TPointerFrom.Z = CSng(pos(2))
        m_TPointerTo.x = CSng(pos(3)): m_TPointerTo.y = CSng(pos(4)): m_TPointerTo.Z = CSng(pos(5))
        '一時的な未決定の線分の書き込み
        If m_ViewTempLine Then
        With m_TempLine
        result = RotateALine(CStr(.From_x) & "," & CStr(.From_y) & "," & CStr(.From_z) & "," & _
                            CStr(.To_x) & "," & CStr(.To_y) & "," & CStr(.To_z), _
                            m_longitude, m_latitude)
        End With
        StrSplit result, ",", pos
        With m_TTempLine
            
            .From_x = CSng(pos(0)): .From_y = CSng(pos(1)): .From_z = CSng(pos(2))
            .To_x = CSng(pos(3)): .To_y = CSng(pos(4)): .To_z = CSng(pos(5))
        End With
        End If
        '２つめの一時的な未決定の線分の書き込み
        If m_ViewTempLine2 Then
        With m_TempLine2
        result = RotateALine(CStr(.From_x) & "," & CStr(.From_y) & "," & CStr(.From_z) & "," & _
                            CStr(.To_x) & "," & CStr(.To_y) & "," & CStr(.To_z), _
                            m_longitude, m_latitude)
        End With
        StrSplit result, ",", pos
        With m_TTempLine2
            
            .From_x = CSng(pos(0)): .From_y = CSng(pos(1)): .From_z = CSng(pos(2))
            .To_x = CSng(pos(3)): .To_y = CSng(pos(4)): .To_z = CSng(pos(5))
        End With
        End If
'    End If
End Sub
'************************************************


'************************************************
Private Function RotateALine(input_line As String, longitude As Long, latitude As Long) As String
'線分一本を座標変換して返す
'------------------------------------------------
    Dim pos() As String
    Dim orgLine As Type_A_Line
    Dim transLine As Type_A_Line
    Dim xx As Single, xxx As Single    '座標計算値の一時保管
    Dim yy As Single, yyy As Single
    Dim zz As Single, zzz As Single
    Dim lon As Single, lat As Single    '経度、緯度
    
    lon = longitude / 180 * PI: lat = -latitude / 180 * PI
    
    StrSplit input_line, ",", pos
    With orgLine
        .From_x = CSng(pos(0)): .From_y = CSng(pos(1)): .From_z = CSng(pos(2))
        .To_x = CSng(pos(3)): .To_y = CSng(pos(4)): .To_z = CSng(pos(5))
    End With
    With orgLine
         xx = .From_x * Cos(lon) + .From_z * Sin(lon)
         yy = .From_y
         zz = -.From_x * Sin(lon) + .From_z * Cos(lon)
         xxx = .To_x * Cos(lon) + .To_z * Sin(lon)
         yyy = .To_y
         zzz = -.To_x * Sin(lon) + .To_z * Cos(lon)
     End With
     With transLine
         .From_x = xx
         .From_y = yy * Cos(lat) + zz * Sin(lat)
         .From_z = -yy * Sin(lat) + zz * Cos(lat)
         .To_x = xxx
         .To_y = yyy * Cos(lat) + zzz * Sin(lat)
         .To_z = -yyy * Sin(lat) + zzz * Cos(lat)
     'ここが返り血
         RotateALine = CStr(.From_x) & "," & CStr(.From_y) & "," & CStr(.From_z) & "," & _
                     CStr(.To_x) & "," & CStr(.To_y) & "," & CStr(.To_z)
     End With
End Function
'************************************************







'************************************************
Private Sub DrawToPicBuffer()
'保持しているデータをバックバッファに描き込む
'------------------------------------------------
    Dim i As Long
    picBuffer.Cls
    '座標系の調整
    With picBuffer
        .ScaleWidth = m_ControlScaleWidth / m_Ratio
        .ScaleHeight = -m_ControlScaleHeight / m_Ratio
        .ScaleLeft = -m_ControlScaleWidth / (2 * m_Ratio) + m_Txmax / 2
        .ScaleTop = m_ControlScaleHeight / (2 * m_Ratio) + m_Tymax / 2
    End With
    
    '座標軸の表示
'    If m_booEnableAxisLine Then
        If m_ViewAxis Then
            For i = 1 To 3
                With m_TAxisLine(i)
                    picBuffer.DrawWidth = m_AxisLineSize
                    picBuffer.Line (.From_x, .From_y)- _
                                    (.To_x, .To_y), m_AxisColor(i)
                End With
            Next i
        End If
        '補助座標軸の表示
        If m_ViewSubAxis Then
            For i = 1 To 3
                With m_TSubAxisLine(i)
                    picBuffer.DrawWidth = m_SubAxisLineSize
                    picBuffer.Line (.From_x, .From_y)- _
                                    (.To_x, .To_y), m_AxisColor(i)
                    '色は座標軸と同じ
                End With
            Next i
        End If
'    End If
    '図形の描画
    For i = 1 To UBound(m_Lines)
        With m_TLines(i)
            picBuffer.DrawWidth = m_FigLineSize
            picBuffer.Line (.From_x, .From_y)- _
                            (.To_x, .To_y), m_FigColor
        End With
    Next i

'    If m_booEnablePointer Then
        '線分の太さのバックアップを取ってから変更する
        Dim WidthBackup As Integer
        WidthBackup = DrawWidth
        picBuffer.DrawWidth = m_PointerLineSize
        '作図ポインタの表示
        If m_ViewPointerFrom Then
            With m_TPointerFrom
                picBuffer.Circle (.x, .y), m_PointerSize / m_Ratio, m_PointerFromColor
                picBuffer.PSet (.x, .y), m_PointerFromColor
                If m_booViewCoordinates Then
                    picBuffer.Print "　x= " + Format(m_PointerFrom.x, "###0") + _
                                      ", y= " + Format(m_PointerFrom.y, "###0") + _
                                      ", z= " + Format(m_PointerFrom.Z, "###0")
                End If

            End With
        End If
        If m_ViewPointerTo Then
            With m_TPointerTo
                picBuffer.Circle (.x, .y), m_PointerSize / m_Ratio, m_PointerToColor
                picBuffer.PSet (.x, .y), m_PointerToColor
                If m_booViewCoordinates Then
                    picBuffer.Print "　x= " + Format(m_PointerTo.x, "###0") + _
                                      ", y= " + Format(m_PointerTo.y, "###0") + _
                                      ", z= " + Format(m_PointerTo.Z, "###0")
                End If
            
            End With
        End If
        picBuffer.DrawWidth = WidthBackup '線分の太さの復旧
        '未確定の線分
        If m_ViewTempLine Then
                With m_TTempLine
                    picBuffer.DrawWidth = m_TempLineSize
                    picBuffer.Line (.From_x, .From_y)- _
                                    (.To_x, .To_y), m_TempLineColor
                End With
        End If
        '二本目の未確定の線分
        If m_ViewTempLine Then
            With m_TTempLine2
                picBuffer.DrawWidth = m_TempLineSize2
                picBuffer.Line (.From_x, .From_y)- _
                                (.To_x, .To_y), m_TempLineColor2
            End With
        End If
'    End If
End Sub
'************************************************







'************************************************
Public Function SetMaxSize(size As String) As Boolean
'------------------------------------------------
    Dim pos() As String
    StrSplit size, ",", pos
    m_xmax = pos(0)
    m_ymax = pos(1)
    m_zmax = pos(2)
End Function
'************************************************



'************************************************
Public Function SetComment(Comment As String) As Boolean
    m_Comment = Comment
    SetComment = True
End Function
'************************************************











'************************************************
Private Sub CopyBufferToView()
'------------------------------------------------
    Dim ret As Long
    '作業用ピクチャボックスから表ピクチャボックスへ転送！やっとできた・・・
    'ビットブリットはScalewidth、Scaleheightに関係なく、左上すみから
    '何ピクセル送るかを指定しなければならないことがわかった。
    ret = BitBlt(picView.hdc, 0, 0, _
                                picView.Width, picView.Height, _
                                picBuffer.hdc, _
                                0, 0, _
                                SRCCOPY)
    picView.Refresh
End Sub
'************************************************





'背景の色
Public Property Get BackColor() As Long
    BackColor = m_BackColor
End Property

Public Property Let BackColor(ByVal NewValue As Long)
    m_BackColor = NewValue
    UserControl.picBuffer.BackColor = m_BackColor
End Property



'図形の線の色
Public Property Get FigColor() As Long
    FigColor = m_FigColor
End Property

Public Property Let FigColor(ByVal NewValue As Long)
    m_FigColor = NewValue
End Property

Public Function SetLongitude(val As Long)
    If val > 360 Then val = val Mod 360
    m_longitude = val
    'Call Draw
End Function

Public Function SetLatitude(val As Long)
    If val > 360 Then val = val Mod 360
    m_latitude = val
    'Call Draw

End Function

Public Property Get Ratio() As Single
    Ratio = m_Ratio
End Property

Public Property Let Ratio(ByVal NewValue As Single)
    m_Ratio = NewValue
End Property



'************************************************
'コントロールの幅と高さの設定
Public Property Get ControlWidth() As Long
    ControlWidth = UserControl.Width
    
End Property

Public Property Let ControlWidth(ByVal NewValue As Long)
    UserControl.Width = NewValue
    picView.Width = UserControl.Width
    picBuffer.Width = UserControl.Width
    
End Property

Public Property Get ControlHeight() As Long
    ControlHeight = UserControl.Height
End Property

Public Property Let ControlHeight(ByVal NewValue As Long)
    UserControl.Height = NewValue
    picView.Height = UserControl.Height
    picBuffer.Height = UserControl.Height
End Property
'************************************************








'************************************************
Public Property Get FigLineSize() As Long
    FigLineSize = m_FigLineSize
End Property

Public Property Let FigLineSize(ByVal NewValue As Long)
    m_FigLineSize = NewValue
End Property
'************************************************





'************************************************
Public Property Get AxisLineSize() As Long
    AxisLineSize = m_AxisLineSize
End Property

Public Property Let AxisLineSize(ByVal NewValue As Long)
    m_AxisLineSize = NewValue
End Property
'************************************************

'************************************************
Public Property Get SubAxisLineSize() As Long
    SubAxisLineSize = m_SubAxisLineSize
End Property

Public Property Let SubAxisLineSize(ByVal NewValue As Long)
    m_SubAxisLineSize = NewValue
End Property
'************************************************





Public Property Get PointerFrom() As String
    With m_PointerFrom
        PointerFrom = CStr(.x) & "," & CStr(.y) & "," & CStr(.Z)
    End With
End Property

Public Property Let PointerFrom(ByVal NewValue As String)
    Dim pos() As String
    
    StrSplit NewValue, ",", pos
    With m_PointerFrom
        .x = CSng(pos(0))
        .y = CSng(pos(1))
        .Z = CSng(pos(2))
    End With
        
End Property

Public Property Get PointerTo() As String
    With m_PointerTo
        PointerTo = CStr(.x) & "," & CStr(.y) & "," & CStr(.Z)
    End With
End Property

Public Property Let PointerTo(ByVal NewValue As String)
    Dim pos() As String
    
    StrSplit NewValue, ",", pos
    With m_PointerTo
        .x = CSng(pos(0))
        .y = CSng(pos(1))
        .Z = CSng(pos(2))
    End With
End Property



'Public Function ViewPointerFrom(mode As Boolean) As Boolean
'    m_ViewPointerFrom = mode
'End Function
'
'Public Function ViewPointerTo(mode As Boolean) As Boolean
'    m_ViewPointerTo = mode
'
'End Function

Public Property Get TempLine() As String
    
End Property

Public Property Let TempLine(ByVal NewValue As String)
    Dim pos() As String
    
    StrSplit NewValue, ",", pos
    With m_TempLine
        .From_x = CSng(pos(0))
        .From_y = CSng(pos(1))
        .From_z = CSng(pos(2))
        .To_x = CSng(pos(3))
        .To_y = CSng(pos(4))
        .To_z = CSng(pos(5))
    End With
        
End Property

Public Function ViewTempLine(mode As Boolean) As Boolean
    m_ViewTempLine = mode
End Function

Public Sub TempLineSize(size As Long)
    m_TempLineSize = size
    

End Sub

Public Function PointerSize(size As Long) As Boolean
    m_PointerSize = size
    PointerSize = True
End Function




Public Property Get TempLine2() As String

End Property

Public Property Let TempLine2(ByVal NewValue As String)
    Dim pos() As String
    
    StrSplit NewValue, ",", pos
    With m_TempLine2
        .From_x = CSng(pos(0))
        .From_y = CSng(pos(1))
        .From_z = CSng(pos(2))
        .To_x = CSng(pos(3))
        .To_y = CSng(pos(4))
        .To_z = CSng(pos(5))
    End With
        

End Property

Public Function ViewTempLine2(mode As Boolean)
    m_ViewTempLine2 = mode
End Function

Public Property Get ControlScaleWidth() As Long


End Property

Public Property Let ControlScaleWidth(ByVal NewValue As Long)
    m_ControlScaleWidth = NewValue
    With picBuffer
        .ScaleWidth = m_ControlScaleWidth / m_Ratio
        '.ScaleHeight = -m_ControlScaleHeight / m_ratio
        .ScaleLeft = -.ScaleWidth / (2 * m_Ratio) + m_Txmax / 2
        '.ScaleTop = .ScaleHeight / (2 * m_ratio) + m_Tymax / 2
    End With
End Property

Public Property Get ControlScaleHeight() As Long

End Property

Public Property Let ControlScaleHeight(ByVal NewValue As Long)
    m_ControlScaleHeight = NewValue
    With picBuffer
        '.ScaleWidth = m_ControlScaleWidth / m_ratio
        .ScaleHeight = -m_ControlScaleHeight / m_Ratio
        '.ScaleLeft = -.ScaleWidth / (2 * m_ratio) + m_Txmax / 2
        .ScaleTop = .ScaleHeight / (2 * m_Ratio) + m_Tymax / 2
    End With
End Property

Private Sub UserControl_Paint()
    picBuffer.Left = picView.Width
    picBuffer.Top = picView.Height
End Sub

'Public Property Get EnablePointer() As Boolean
'    EnablePointer = m_booEnablePointer
'End Property
'
'Public Property Let EnablePointer(ByVal NewValue As Boolean)
'    m_booEnablePointer = NewValue
'End Property
'
'
'
'Public Property Get EnableAxisLine() As Boolean
'    EnableAxisLine = m_booEnableAxisLine
'End Property
'
'Public Property Let EnableAxisLine(ByVal NewValue As Boolean)
'    m_booEnableAxisLine = NewValue
'End Property

Public Property Get ViewAxis() As Boolean
    ViewAxis = m_ViewAxis
End Property

Public Property Let ViewAxis(ByVal NewValue As Boolean)
    m_ViewAxis = NewValue
End Property

Public Property Get ViewsubAxis() As Boolean
    ViewsubAxis = m_ViewSubAxis
End Property

Public Property Let ViewsubAxis(ByVal NewValue As Boolean)
    m_ViewSubAxis = NewValue
End Property

Public Property Get ViewPointerFrom() As Boolean
    ViewPointerFrom = m_ViewPointerFrom
End Property

Public Property Let ViewPointerFrom(ByVal NewValue As Boolean)
    m_ViewPointerFrom = NewValue
End Property

Public Property Get ViewPointerTo() As Boolean
    ViewPointerTo = m_ViewPointerTo
End Property

Public Property Let ViewPointerTo(ByVal NewValue As Boolean)
    m_ViewPointerTo = NewValue
End Property

Public Property Get PointerFromColor() As Long
    PointerFromColor = m_PointerFromColor
End Property

Public Property Let PointerFromColor(ByVal NewValue As Long)
    m_PointerFromColor = NewValue
End Property

Public Property Get PointerToColor() As Long
    PointerToColor = m_PointerToColor
End Property

Public Property Let PointerToColor(ByVal NewValue As Long)
    m_PointerToColor = NewValue
End Property

Public Property Get PointerLineSize() As Integer
    PointerLineSize = m_PointerLineSize
End Property

Public Property Let PointerLineSize(ByVal NewValue As Integer)
    m_PointerLineSize = NewValue
End Property

Public Property Get AxisLineLength() As Integer
    AxisLineLength = m_AxisLength
End Property

Public Property Let AxisLineLength(ByVal NewValue As Integer)
    m_AxisLength = NewValue
    m_AxisLine(1).From_x = m_AxisLength
    m_AxisLine(2).From_y = m_AxisLength
    m_AxisLine(3).From_z = m_AxisLength
End Property

Public Property Get SubAxisLineLength() As Integer
    SubAxisLineLength = m_SubAxisLength
End Property

Public Property Let SubAxisLineLength(ByVal NewValue As Integer)
    m_SubAxisLength = NewValue
    m_SubAxisLine(1).From_x = m_SubAxisLength
    m_SubAxisLine(2).From_y = m_SubAxisLength
    m_SubAxisLine(3).From_z = m_SubAxisLength
End Property
'------------------------------------------------
Public Sub CopyToClipboard()
'目的説明：現在の画面をクリップボードへコピー
    'ピクチャボックスをクリア
    picView.Picture = LoadPicture()
    '再描画
    Call Draw
    'クリップボードをクリア
    Clipboard.Clear
    'クリップボードへコピー
    Clipboard.SetData picView.Image, vbCFBitmap
End Sub

'------------------------------------------------
Public Property Get ViewCoordinates() As Boolean
'目的：座標数値を表示するかどうか
'------------------------------------------------
    ViewCoordinates = m_booViewCoordinates
End Property

Public Property Let ViewCoordinates(ByVal NewValue As Boolean)
    m_booViewCoordinates = NewValue
End Property
