VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGuriView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Class Name:   CGuriView
' Author:       平田 敦, G-Soft
' Date:         2002/01/30
' Description:  立体ぐりぐりのデータを
'　　　　　　　　"表示する"ことを担当する。
' Edit History:
'
Option Explicit
'
'
' PUBLIC: ******************************************
' Public data members

' PRIVATE: *****************************************
' Private data members

'読み込むデータのファイル名がフルパスで格納される。
Private m_strPathAndFileName As String
'図が表示されるピクチャボックス
Private m_ctlDestPicBox As PictureBox
'作業領域となるピクチャボックス｡
Private m_ctlDestPicBack As PictureBox

'図形の座標値やコメントなどが格納されるオブジェクト
Private m_objGuriData As CGuriData
'ファイル処理に使用するオブジェクト
'Private m_objFile As CFile


' PUBLIC: *****************************************
' Public property procedures
'
'Public Property Let PathAndFileName(ByVal Name As String)
'    m_strPathAndFileName = Name
'    'そのファイル名のデータを読み込む。
'    '読み込みには、CFileクラスを使用する。格納先はCGuriDataクラス
'    Set m_objGuriData = New CGuriData
'    Set m_objFile = New CFile
'    m_objFile.Load m_strPathAndFileName, m_objGuriData
'    '読み込んだデータをピクチャボックスに表示する。
'    Call Draw
'    Debug.Print "CGuriView.PathAndFileName Set And Drawn"
'End Property


Public Property Let DestPicBox(ByRef PicBox As Control)
    Set m_ctlDestPicBox = PicBox
    m_ctlDestPicBox.AutoRedraw = True
    m_ctlDestPicBox.ScaleMode = vbPixels
End Property

Public Property Let DestPicBack(ByRef picBack As Control)
    Set m_ctlDestPicBack = picBack
    picBack.Visible = False
    picBack.AutoRedraw = True
End Property

' PUBLIC: ***************************************
' Public procedure

' PRIVATE: **************************************
' Private procedure


'************************************************
'目的：     Rotation　図形の座標値を視点の位置から見えるように変換
'引数：     なし
'戻り値：   なし
'************************************************
Public Sub Rotation()

    Debug.Print "CGuriView.Rotation"
    Dim lon As Single   '経度　radian
    Dim lat As Single   '緯度　radian
    Dim i As Long       'general counter
    Dim xx, yy, zz As Single '座標計算値の一時保存場所
    '

    '角度を度からラジアンに変換する｡
    lon = m_lngLongitude / 180 * PI
    lat = -m_lngLatitude / 180 * PI
    '
    
    ReDim Preserve m_sngTransX(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngTransY(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngTransZ(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngX(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngY(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngZ(m_lngNumberOfLines * 2)
    
    xx = m_sngXMax * Cos(lon) + m_sngZMax * Sin(lon)
    yy = m_sngYMax
    zz = -m_sngXMax * Sin(lon) + m_sngZMax * Cos(lon)
    m_sngTransXMax = xx
    m_sngTransYMax = yy * Cos(lat) + zz * Sin(lat)
    m_sngTransZMax = -yy * Sin(lat) + zz * Cos(lat)
    
    For i = 1 To m_lngNumberOfLines * 2
        xx = m_sngX(i) * Cos(lon) + m_sngZ(i) * Sin(lon)
        yy = m_sngY(i)
        zz = -m_sngX(i) * Sin(lon) + m_sngZ(i) * Cos(lon)
    
        m_sngTransX(i) = xx
        m_sngTransY(i) = yy * Cos(lat) + zz * Sin(lat)
        m_sngTransZ(i) = -yy * Sin(lat) + zz * Cos(lat)
    Next i
End Sub
'************************************************




Public Property Get CurrentGuriData() As Variant
    CurrentGuriData = m_objGuriData
End Property

Public Property Let CurrentGuriData(ByVal vNewValue As Variant)
    m_objGuriData = vNewValue
End Property

