VERSION 5.00
Begin VB.UserControl GuriView 
   ClientHeight    =   3600
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4800
   ScaleHeight     =   3600
   ScaleWidth      =   4800
   Begin VB.PictureBox picBack 
      Height          =   1455
      Left            =   2160
      ScaleHeight     =   1395
      ScaleWidth      =   1755
      TabIndex        =   1
      Top             =   360
      Width           =   1815
   End
   Begin VB.PictureBox picFront 
      Height          =   1455
      Left            =   240
      ScaleHeight     =   1395
      ScaleWidth      =   1755
      TabIndex        =   0
      Top             =   360
      Width           =   1815
   End
End
Attribute VB_Name = "GuriView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'GuriViewのインターフェイス
Implements IGuriView

Private m_objGuriData As CGuriData
Private m_objGuriView As CGuriView
Private m_objViewData As CViewData
Private m_objLogging As CLogging




Private Sub IGuriView_LoadData(ByVal FileName As String)
    m_objGuriData.Load FileName
    'テスト用ステートメント
    m_objLogging.Log m_objGuriData.GetLine(2)
    'ここまで
    
End Sub

Private Sub IGuriView_SaveData(ByVal FileName As String)
    m_objGuriData.Save FileName
End Sub



Private Sub UserControl_Initialize()
    Set m_objLogging = New CLogging
    m_objLogging.Log "●GuriView UserControl_Initialize() called"
    Set m_objGuriData = New CGuriData
    Set m_objGuriView = New CGuriView
    Set m_objViewData = New CViewData
End Sub

Private Sub UserControl_Paint()
    
End Sub

Private Sub UserControl_Terminate()
    m_objLogging.Log "○GuriView UserControl_Terminate() called"
End Sub

'------------------------------------------------
Private Sub Draw()
'目的説明：     読み込まれたデータをピクチャボックスに表示する。
'引数：         なし
'返り値：       なし
'作成者：       平田 敦、G-Soft
'作成年月日：   98/09/22
'変更履歴：
'------------------------------------------------
    Debug.Print "CGuriView.Draw Start"
'
    Dim i As Integer            'general counter
    Dim xx As String, yy As String, zz As String
    Dim ret As Integer
    Dim PenColor As Long
    
    PenColor = vbBlack
    '
    '視点位置によるデータの変換
    m_objGuriData.Rotation
    '表示されるピクチャボックスの各プロパティを調整
    With m_ctlDestPicBack
        '裏画面の消去
        .Cls
        '座標系の調整
        m_objGuriData.Ratio = .Width / 2 _
            / (2 * (m_objGuriData.XMax + _
                    m_objGuriData.YMax + _
                    m_objGuriData.ZMax) / 3)
        Debug.Print "m_ctlDestPicBack.ScaleWidth" & .ScaleWidth
        Debug.Print "Screen.TwipsPerPixelX" & Screen.TwipsPerPixelX
        Debug.Print "XMax " & m_objGuriData.XMax
        Debug.Print "CGuriData.Ratio = " & m_objGuriData.Ratio
        .ScaleWidth = .Width / m_objGuriData.Ratio
            Debug.Print ".ScaleWidth" & .ScaleWidth
        .ScaleHeight = -.Height / m_objGuriData.Ratio
            Debug.Print ".ScaleHeight" & .ScaleHeight
        .ScaleLeft = -.Width / (2 * m_objGuriData.Ratio) + _
                m_objGuriData.TransXMax
        .ScaleTop = .Height / (2 * m_objGuriData.Ratio) + _
                    m_objGuriData.TransYMax
    End With
    With m_ctlDestPicBox
        .ScaleWidth = m_ctlDestPicBack.ScaleWidth
        .ScaleHeight = m_ctlDestPicBack.ScaleHeight
        .ScaleLeft = m_ctlDestPicBack.ScaleLeft
        .ScaleTop = m_ctlDestPicBack.ScaleTop
    End With
    '
    '図形の描画
    'If FigFlags.DrawingMode = True Then PenColor = vbBlack Else PenColor = vbWhite
    With m_objGuriData
        For i = 1 To .NumberOfLines * 2 Step 2
            m_ctlDestPicBack.Line (.TransX(i), .TransY(i)) _
                            -(.TransX(i + 1), .TransY(i + 1)), _
                            PenColor
        Next i
    End With
    '
    '作業用ピクチャボックスから表ピクチャボックスへ転送！
    ret = BitBlt(m_ctlDestPicBox.hDC, 0, 0, _
                        m_ctlDestPicBox.Width, _
                        m_ctlDestPicBox.Height, _
                        m_ctlDestPicBack.hDC, _
                        0, 0, _
                        SRCCOPY)
    m_ctlDestPicBox.Refresh
    m_ctlDestPicBack.Refresh
End Sub




Public Sub LoadData(ByVal FileName As String)
    m_objLogging.Log "ctlGuriView.LoadData(" + FileName + ")"
    Call IGuriView_LoadData(FileName)
End Sub

Public Sub SaveData(ByVal FileName As String)
    m_objLogging.Log "ctlGuriView.SaveData(" + FileName + ")"
    Call IGuriView_SaveData(FileName)
End Sub
