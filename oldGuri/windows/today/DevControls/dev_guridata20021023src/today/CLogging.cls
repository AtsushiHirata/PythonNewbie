VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLogging"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Dim m_strFileName As String
Dim Fhandle As Integer

Private Sub Class_Initialize()
    If LOGGING_ACTIVE Then
        m_strFileName = Format(Date, "yyyy_mm_dd") + ".log"
        m_strFileName = App.Path + "\ctlGuriDataOCX_logfile_" + m_strFileName
        Fhandle = FreeFile
        Open m_strFileName For Append As #Fhandle
'            Print #Fhandle, "-------------------------"
'            Print #Fhandle, "Logging class initialized"
'            Print #Fhandle, App.EXEName
'            Print #Fhandle, App.CompanyName
'            Print #Fhandle, App.Comments
'            Print #Fhandle, App.Path
'            Print #Fhandle, "-------------------------"
        Close #Fhandle
    End If
End Sub

Public Sub Log(ByVal comment As String)
    If LOGGING_ACTIVE Then
        Fhandle = FreeFile
        Open m_strFileName For Append As #Fhandle
        Print #Fhandle, comment
        Close #Fhandle
    End If
End Sub

