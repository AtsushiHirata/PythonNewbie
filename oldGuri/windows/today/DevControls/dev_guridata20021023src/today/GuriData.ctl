VERSION 5.00
Begin VB.UserControl GuriData 
   BackColor       =   &H0000FF00&
   ClientHeight    =   1515
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   1635
   Picture         =   "GuriData.ctx":0000
   ScaleHeight     =   87.351
   ScaleMode       =   0  'ﾕｰｻﾞｰ
   ScaleWidth      =   1744
End
Attribute VB_Name = "GuriData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'------------------------------------------------
' ファイル名:   ctlGuriData.ocx
' 作成者　:     平田 敦, G-Soft
' 作成開始:     2002/01/23
' 機能要約:     立体ぐりぐりのデータを格納する。
'               ファイルへの入出力を司る。
'               それらの機能を全てCGuriDataに委譲する。
' 修正履歴:
'------------------------------------------------
Option Explicit


Private m_objGuriData As CGuriData
Private m_objLogging As CLogging


'--------------------------------------------------
'コンストラクタ・デストラクタ

Private Sub UserControl_Initialize()
    Set m_objLogging = New CLogging
    m_objLogging.Log "●GuriData UserControl_Initialize() called"
    
    Set m_objGuriData = New CGuriData
End Sub

Private Sub UserControl_Terminate()
    m_objLogging.Log "○GuriData UserControl_Terminate() called"
End Sub







'常にアイコンのサイズで表示する。（表示するならね）
Private Sub UserControl_Paint()
    UserControl.Width = 32 * Screen.TwipsPerPixelX
    UserControl.Height = 32 * Screen.TwipsPerPixelY
End Sub








'--------------------------------------------------
'CGuriDataへデレゲートした公開操作

Public Function GetLine(ByVal i As Long) As String
    GetLine = m_objGuriData.GetLine(i)
End Function

Public Function SetLine(ByVal line As String) As Boolean
    SetLine = m_objGuriData.SetLine(line)
End Function

Public Function DelLine(ByVal i As Long) As Boolean
    DelLine = m_objGuriData.DelLine(i)
End Function

Public Function LoadData(ByVal filename As String) As Boolean
    LoadData = m_objGuriData.LoadData(filename)
End Function

Public Function SaveData(ByVal filename As String) As Boolean
    SaveData = m_objGuriData.SaveData(filename)
End Function

Public Function GetNumberOfLines() As Long
    GetNumberOfLines = m_objGuriData.GetNumberOfLines
End Function

Public Function GetComment() As String
    GetComment = m_objGuriData.GetComment
End Function

Public Function SetComment(ByVal comment As String) As Boolean
    SetComment = m_objGuriData.SetComment(comment)
End Function

Public Function GetMaxSize() As String
    GetMaxSize = m_objGuriData.GetMaxSize
End Function

Public Function SetLineTo(ByVal line_number As Long, _
                          ByVal a_line As String) _
                                         As Boolean
    SetLineTo = m_objGuriData.SetLineTo(line_number, _
                                            a_line)
End Function

Public Function ClearData() As Boolean
     ClearData = m_objGuriData.ClearData
End Function

Public Function IsDirty() As Boolean
    IsDirty = m_objGuriData.IsDirty
End Function

