VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGuriData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'------------------------------------------------
' クラス名:     CGuriData
' 作成者　:     平田 敦, G-Soft
' 作成開始:     2002/01/23
' 機能要約:     立体ぐりぐりのデータを格納する。
'               ファイルへの入出力を司る。
' 修正履歴:
'   2002/10/22  最大値、最小値、最大寸法のバグを修正。
'               不要な変数を削除。
'------------------------------------------------
Option Explicit










'------------------------------------------------
'このクラス内だけで有効な定数








'------------------------------------------------
'　属性
'
Private m_sngXMax As Single
Private m_sngYMax As Single
Private m_sngZMax As Single
Private m_lngNumberOfLines As Long
Private m_objLine() As CLine
Private m_strComment As String
Private m_booIsDirty As Boolean
Private m_strFileName As String 'ファイル名（フルパスで格納）










'------------------------------------------------
'内部処理用変数

Private m_objLogging As CLogging    'ロギング用オブジェクト










'================================================
'　操作(Public)
'

'************************************************
Public Function GetLine(ByVal LineNumber As Long) As String
'LineNumberで指定された番号の線の座標を返す。
'線は1番から始まる。
'------------------------------------------------
    If m_lngNumberOfLines = 0 Then
        GetLine = "0,0,0,0,0,0"
    Else
        GetLine = CStr(m_objLine(LineNumber).FromX) + "," + _
                  CStr(m_objLine(LineNumber).FromY) + "," + _
                  CStr(m_objLine(LineNumber).FromZ) + "," + _
                  CStr(m_objLine(LineNumber).ToX) + "," + _
                  CStr(m_objLine(LineNumber).ToY) + "," + _
                  CStr(m_objLine(LineNumber).ToZ)
    End If
End Function
'************************************************










'************************************************
Public Function SetLine(ByVal a_line As String) As Boolean
'線の本数を一つ増やす。配列の末尾に追加。
'------------------------------------------------
On Error GoTo ErrorHandler
    '取得した線分データを数値に変換する
    Dim strPos() As String, sngPos(6) As Single
    Dim i As Long, j As Long
    i = StrSplit(a_line, ",", strPos)
    For j = 0 To i - 1
        sngPos(j) = CSng(strPos(j))
    Next j
    '線分を加えるかどうかのチェック
    If CheckLine(sngPos(0), sngPos(1), sngPos(2), _
                 sngPos(3), sngPos(4), sngPos(5)) Then
        '正しいデータなら以下の処理を行う
        m_lngNumberOfLines = m_lngNumberOfLines + 1
        'Lineオブジェクトを一つ増やす。
        ReDim Preserve m_objLine(UBound(m_objLine()) + 1)
        Set m_objLine(m_lngNumberOfLines) = New CLine
        'データを正式に保管
        With m_objLine(m_lngNumberOfLines)
            .FromX = sngPos(0): .FromY = sngPos(1): .FromZ = sngPos(2)
            .ToX = sngPos(3): .ToY = sngPos(4): .ToZ = sngPos(5)
        End With
        SetLine = True
        'ダーティフラグをあげる
        m_booIsDirty = True
        Call SearchMaxSize
    Else
        '正しいデータでなかったら以下の処理を行う
        SetLine = False
        'ダーティフラグは保留
    End If
Exit Function
ErrorHandler:
    'エラーによって線を追加できなかった
    SetLine = False
    'ダーティフラグは保留
End Function
'************************************************









'***********************************************
Public Function DelLine(ByVal line_number As Long) As Boolean
'line_numberで指定した番号の線分を削除する。
'線番は１から始まる。
'-----------------------------------------------
    Dim i As Long, result As Boolean
  
    If (line_number <> 0) And _
      (line_number <= m_lngNumberOfLines) Then
      For i = line_number To m_lngNumberOfLines - 1
        m_objLine(i).FromX = m_objLine(i + 1).FromX
        m_objLine(i).FromY = m_objLine(i + 1).FromY
        m_objLine(i).FromZ = m_objLine(i + 1).FromZ
        m_objLine(i).ToX = m_objLine(i + 1).ToX
        m_objLine(i).ToY = m_objLine(i + 1).ToY
        m_objLine(i).ToZ = m_objLine(i + 1).ToZ
      Next i
      ReDim Preserve m_objLine(m_lngNumberOfLines - 1)
      m_lngNumberOfLines = m_lngNumberOfLines - 1
      DelLine = True
      'ダーティフラグをあげる
      m_booIsDirty = True
      Call SearchMaxSize
    Else
        'ダーティフラグは保留
        'm_booIsDirty = False
    End If
End Function
'************************************************













'************************************************
Public Function LoadData(ByVal filename As String) As Boolean
'機能説明：
'          FileNameで与えられたデータを取得する
'引数：
'          FileName   読み込むファイルの名前。
'                     フルパスで。
'返り値：  なし
'------------------------------------------------
On Error GoTo ErrorHandler
    Dim i As Integer    'general counter
    Dim X1, Y1, Z1, X2, Y2, Z2  'あえてバリアント型
    Dim Fhandle As Integer
    Dim result As Boolean
labelResume:
    Fhandle = FreeFile
    Open filename For Input As #Fhandle
    Input #Fhandle, m_sngXMax, m_sngYMax, m_sngZMax, _
                    m_lngNumberOfLines
    ReDim m_objLine(m_lngNumberOfLines) As CLine
    For i = 1 To m_lngNumberOfLines
        Input #Fhandle, X1, Y1, Z1, X2, Y2, Z2
        Set m_objLine(i) = New CLine
        With m_objLine(i)
            .FromX = X1: .FromY = Y1: .FromZ = Z1
            .ToX = X2: .ToY = Y2: .ToZ = Z2
        End With
    Next i
    Input #Fhandle, m_strComment
    Close #Fhandle
    LoadData = True
    Call SearchMaxSize
    'ダーティフラグをおろす
    m_booIsDirty = False
Exit Function
ErrorHandler:
    MsgBox Err.Description, , "データ読み込みエラー"
    LoadData = False
    'ダーティフラグをおろす
    m_booIsDirty = False
End Function
'************************************************









'************************************************
Public Function SaveData(ByVal filename As String) As Boolean
'目的説明:      保持しているデータを保存する。
'
'入力引数:      フルパスのファイルネーム
'戻り値:        実行結果をブーリアンで。
'------------------------------------------------
' 1998/01/16エラー処理のルーチンを付加
On Error GoTo ErrorHandler
    Dim i As Integer    'general counter
    Dim Fhandle As Integer
    
    If Not filename = "" Then
        Fhandle = FreeFile  '使用可能なファイルハンドルを取得する
        Open filename For Output As #Fhandle
        Print #Fhandle, m_sngXMax, m_sngYMax, m_sngZMax, _
                        m_lngNumberOfLines
        For i = 1 To m_lngNumberOfLines
            With m_objLine(i)
                Print #Fhandle, .FromX, .FromY, .FromZ, _
                                .ToX, .ToY, .ToZ
            End With
        Next i
        Print #Fhandle, m_strComment
        Close #Fhandle
    End If
    SaveData = True
    'ダーティフラグをおろす
    m_booIsDirty = False
Exit Function
ErrorHandler:
    MsgBox Err.Description, , "データ書き込みエラー"
    SaveData = False
    'ダーティフラグの変更は保留
End Function
'************************************************












'--------------------------------------------
'現在の線の本数を返す
Public Function GetNumberOfLines() As Long
    GetNumberOfLines = UBound(m_objLine)
End Function






'--------------------------------------------
'データに付与されたコメントを返す
Public Function GetComment() As String
    GetComment = m_strComment
End Function







'---------------------------------------------
'データにコメントを付与する
Public Function SetComment(ByVal comment As String) As Boolean
    m_strComment = comment
    SetComment = True
    'ダーティフラグをあげる
    m_booIsDirty = True
End Function







'--------------------------------------------
'図形の最大サイズを返す。
'最大サイズとは、図形の各軸方向についての最大
'寸法のことで、最大座標値ではない。
Public Function GetMaxSize() As String
    GetMaxSize = CStr(m_sngXMax) + "," _
               + CStr(m_sngYMax) + "," _
               + CStr(m_sngZMax)
End Function








'---------------------------------------------
'line_numberで指定した順番に
'a_lineで指定した線分データを格納する
'line_numberがm_lngNumberOfLinesより大きい場合には
'末尾に追加する
Public Function SetLineTo(ByVal line_number As Long, _
                          ByVal a_line As String) _
                                         As Boolean
On Error GoTo ErrorHandler
    Dim strPos() As String, sngPos(6) As Single
    Dim i As Long, j As Long
    i = StrSplit(a_line, ",", strPos)
    For j = 0 To i - 1
        sngPos(j) = CSng(strPos(j))
    Next j
    '全ての処理に先立って、その線が有効かどうかチェック
    If CheckLine(sngPos(0), sngPos(1), sngPos(2), _
                 sngPos(3), sngPos(4), sngPos(5)) Then
        '有効ならば、線分の数を増やし、格納する。
        
        '現在の本数より大きな数が指定されたら、
        '末尾＋１の位置とする。
        If line_number > m_lngNumberOfLines Then
            line_number = m_lngNumberOfLines + 1
        End If
        ReDim Preserve m_objLine(m_lngNumberOfLines + 1)
        m_lngNumberOfLines = m_lngNumberOfLines + 1
            Set m_objLine(m_lngNumberOfLines) = New CLine
        '指定された位置から後ろのデータを、
        'ひとつずつ後ろへずらす
        If line_number < m_lngNumberOfLines Then
            For i = m_lngNumberOfLines To line_number + 1 Step -1
                m_objLine(i).FromX = m_objLine(i - 1).FromX
                m_objLine(i).FromY = m_objLine(i - 1).FromY
                m_objLine(i).FromZ = m_objLine(i - 1).FromZ
                m_objLine(i).ToX = m_objLine(i - 1).ToX
                m_objLine(i).ToY = m_objLine(i - 1).ToY
                m_objLine(i).ToZ = m_objLine(i - 1).ToZ
            Next i
        End If
        '指定の場所へデータを格納
        With m_objLine(line_number)
            .FromX = sngPos(0)
            .FromY = sngPos(1)
            .FromZ = sngPos(2)
            .ToX = sngPos(3):
            .ToY = sngPos(4):
            .ToZ = sngPos(5)
        End With
        SetLineTo = True
        'ダーティフラグをあげる
        m_booIsDirty = True
        Call SearchMaxSize
    Else
        'ダーティフラグは保留
        SetLineTo = False
    End If
Exit Function
ErrorHandler:
    'ダーティフラグは保留
    SetLineTo = False
End Function






'---------------------------------------------
'データを全て消去する。
Public Function ClearData() As Boolean
    ReDim m_objLine(0)
    m_lngNumberOfLines = 0
    m_sngXMax = 0
    m_sngYMax = 0
    m_sngZMax = 0
    m_strComment = ""
    m_booIsDirty = False
    m_strFileName = ""
    ClearData = True
    Call SearchMaxSize
End Function






'---------------------------------------------
'変更が加えられているかどうか。
'変更があればＴｒｕｅ
Public Function IsDirty() As Boolean
    IsDirty = m_booIsDirty
End Function







'------------------------------------------------
'　操作(Private)
'
Private Sub Class_Initialize()
    Set m_objLogging = New CLogging
    '内部変数の初期化
    m_strComment = ""
    m_sngXMax = 0: m_sngYMax = 0: m_sngZMax = 0
    m_lngNumberOfLines = 0
    m_strFileName = ""
    m_booIsDirty = False
    ReDim m_objLine(0) As CLine
End Sub


Private Sub Class_Terminate()
    '特になし。うーん。おべんり。
End Sub





'------------------------------------------------------
'===== StrSplit Module =====
'(C)1999-2001 けるべ
'MAIL : kelbe@geocities.co.jp
'HOME : http://www.geocities.co.jp/SilkRoad/4511/

'引数 lngDefaultMaxArray を省略した場合に使用される値です。
'詳しくは下の StrSplit 関数仕様をご覧下さい.

'----- StrSplit 関数 Ver 1.05 -----
'
'文字列を特定の区切り文字で分割し、その結果を引数 strArray() で
'指定された文字列変数配列に格納します。詳しくはサンプルフォーム参照。
'
'引数 strExpression
'   必ず指定します。文字列と区切り文字を含んだ文字列式を指定します。
'   引数 strExpression が長さ 0 の文字列 ("") である場合、
'   StrSplit 関数は 0 を返します。
'
'引数 strDelimiter
'   文字列の区切りを識別する文字を指定します。引数 strDelimiter が
'   長さ 0 の文字列 ("") である場合は、引数 strExpression 全体の
'   文字列を含む単一の要素の配列を返します。指定された区切り文字が
'   存在しない場合も同様です。
'
'引数 strArray()
'   分割結果が格納される文字列型変数配列を指定します。StrSplit 関数を
'   呼び出すプロシージャ内で動的配列として宣言して渡して下さい。
'   配列サイズは関数内で初期化されるため、あらかじめ確保しておく
'   必要はありません。
'
'引数 lngCompare
'   省略可能です。文字列式を評価するときに使用する文字列比較の
'   モードを表す数値を指定します。省略した場合、バイナリ モードで
'   比較が行われます。
'
'   vbBinaryCompare : バイナリ モードで比較を行います。(0)
'   vbTextCompare   : テキスト モードで比較を行います。(1)
'
'引数 lngDefaultMaxArray
'   省略可能です。配列要素最大数の初期値を設定します。
'   もし配列サイズがここで設定した値では足りない場合、ここで設定した
'   個数ずつ配列サイズを増加させてゆきます。そのため分割項目数が
'   多い場合、この値を大きくすれば処理速度は向上しますが、
'   その分メモリを多く食います。予想される分割項目数に合わせて
'   調節して下さい。省略した場合、STRSPLIT_DAFAULT_MAXARRAY 定数の
'   値が使用されます。
'
'戻り値
'   成功した場合、配列 strArray() に格納した配列要素の
'   個数(要素最大値 + 1)を返します。
'   引数 strExpression に空の文字列を渡した場合、0 を返します。
'
'☆ StrSplit 関数と、VB6 の Split 関数との相違点
'
'   1.戻り値は配列の個数である。
'   2.デリミタ(区切り文字)は省略できない。
'   3.第 3 引数に宣言済み文字列変数配列を渡す。
'     (ここに分割結果が格納される)
'   4.count 引数はない。
'   5.配列個数初期値が設定できる。意味ねー(笑)
'
'戻り値が配列の個数となるため、Split 関数のように配列個数を
'UBound 関数で調べる必要もないため使いやすいと思います。
'処理速度はどっちが速いのかわかりません(^^;
'
'☆仕様変更
'
'Ver 1.03 より、返値を「配列の個数」に変更しました。
'Ver 1.02 以前のバージョンから使用されている方は注意して下さい。
'
'Ver 1.05 より、引数 strDelimiter で指定された区切り文字が
'存在しない場合は、引数 strExpression 全体の文字列を含む単一の要素の
'配列を返すように変更しました。(strArray(0) = strExpression)
'
'Ver 1.05 より、配列要素最大値の初期値を設定できるようにしました。
'末尾のオプション引数なので、省略しても何ら問題ありません。
'
Private Function StrSplit _
    (ByRef strExpression As String, _
     ByRef strDelimiter As String, _
     ByRef strArray() As String, _
     Optional ByVal lngCompare As Long = vbBinaryCompare, _
     Optional ByVal lngDefaultMaxArray As Long = 255) As Long

 Dim lngPos1 As Long     'InStr関数用検索開始位置
 Dim lngPos2 As Long     'InStr関数用文字検出位置
 Dim lngStrLen As Long   '検索される文字列のサイズ
 Dim lngDivLen As Long   '区切り文字のサイズ
 Dim lngCnt As Long      '項目数(=配列要素数)をあらわすカウンタ
 Dim lngMaxArray As Long '配列要素の最大数

    lngStrLen = Len(strExpression)  '元の文字列 strExpression の文字数を取得
    lngDivLen = Len(strDelimiter)   '区切り文字 strDelimiter の文字数を取得
    
    If lngStrLen = 0 Then           '引数 strExpression に空の文字列を渡した場合
        StrSplit = 0                '0 を返す
        Exit Function
    ElseIf lngDivLen = 0 Then       '引数 strDelimiter に空の文字列を渡した場合
        ReDim strArray(0)
        strArray(0) = strExpression '引数 strExpression を単一要素の配列として返す
        StrSplit = 1
        Exit Function
    End If
    
    lngMaxArray = lngDefaultMaxArray
    ReDim strArray(lngMaxArray)     '配列最大要素数を初期値にセット
    lngPos1 = 1                     '初期検索開始点を設定

    Do                                                                             '区切り文字が検出されなくなるまでループ
        If lngCnt > lngMaxArray Then                                               '項目数が配列要素最大数を超えてしまった場合
            lngMaxArray = lngMaxArray + lngDefaultMaxArray                         '配列要素最大数を lngDefaultMaxArray 増やす
            ReDim Preserve strArray(lngMaxArray)
        End If
        lngPos2 = InStr(lngPos1, strExpression, strDelimiter, lngCompare)          '区切り文字を検索しその位置を返す
        If lngPos2 Then                                                            '区切り文字が存在した場合
            strArray(lngCnt) = Mid$(strExpression, lngPos1, lngPos2 - lngPos1)     '検索開始点から(区切り文字位置 - 1)までの文字列をを配列に代入
            lngPos1 = lngPos2 + lngDivLen                                          '次回の検索開始点を設定
            lngCnt = lngCnt + 1                                                    '次回のため項目数を一つ増やす
        Else                                                                       '区切り文字が存在しない場合
            If lngCnt Then                                                         '最後の要素である場合
                strArray(lngCnt) = Right$(strExpression, lngStrLen - lngPos1 + 1)  '文字列の最後から検索開始点までの文字列を配列に代入
            Else                                                                   '区切り文字が全く存在しない場合
                strArray(lngCnt) = strExpression                                   '引数 strExpression を単一要素の配列として返す
                Exit Do
            End If
        End If
    Loop Until lngPos2 = 0

    ReDim Preserve strArray(lngCnt) '配列の余分な要素を削る
    StrSplit = lngCnt + 1           '配列の個数を返す

End Function











'************************************************
Private Function CheckLine(ByVal FromX As Single, _
                           ByVal FromY As Single, _
                           ByVal FromZ As Single, _
                           ByVal ToX As Single, _
                           ByVal ToY As Single, _
                           ByVal ToZ As Single) As Boolean
'引数に与えられた線の座標が適当なものかチェックする。
'   ・既に存在する線ならFalse。
'   ・点ならFalse
'   ・適当な線ならTrue
'   　適当な線であった場合には、最大、最小値の
'   　チェックをし、m_sngMaxX,Y,Zを更新する。
'************************************************
    
    '点ならだめ
    If IsPoint(FromX, FromY, FromZ, ToX, ToY, ToZ) Then
        CheckLine = False   '線分ではない
        Exit Function       'ゆえに関数終了
    End If
    
    '既に存在する線ならだめ
    If IsLineExists(FromX, FromY, FromZ, _
                    ToX, ToY, ToZ) Then
        CheckLine = False   'すでに存在する線である
        Exit Function       'ゆえに終了
    End If
    
    '最大、最小値のチェック
    Call SearchMaxSize
    CheckLine = True

End Function

'************************************************
'CheckLine用のサブルーチン
'************************************************


'************************************************
Private Function IsPoint( _
                fx As Single, fy As Single, fz As Single, _
                tx As Single, ty As Single, tz As Single _
                        ) As Boolean
'与えられた座標値が点をあらわしているか否かを判別
'   (fx,fy,fz)-(tx,ty,tz)
'------------------------------------------------
    If (fx = tx) And (fy = ty) And (fz = tz) Then
        'これは線ではなく点である。
        IsPoint = True
    Else
        IsPoint = False
    End If

End Function
'************************************************
                        
                        
                        
'************************************************
Private Function IsLineExists( _
                fx As Single, fy As Single, fz As Single, _
                tx As Single, ty As Single, tz As Single _
                            ) As Boolean
'与えられた座標値の線分が既に存在するかどうか
'
'とりあえず、現在のところは、始点、終点が
'完全に一致する場合のみ検出する。
'------------------------------------------------
    Dim i As Long
    For i = 1 To UBound(m_objLine)
        With m_objLine(i)
            If (fx = .FromX) And (fy = .FromY) And (fz = .FromZ) And _
                (tx = .ToX) And (ty = .ToY) And (tz = .ToZ) _
            Then
                '完全に一致
                IsLineExists = True
                Exit Function
            End If
            '始点、終点逆の場合
            If (tx = .FromX) And (ty = .FromY) And (tz = .FromZ) And _
                (fx = .ToX) And (fy = .ToY) And (fz = .ToZ) _
            Then
                '完全に一致
                IsLineExists = True
                Exit Function
            End If
        End With
    Next i
    'ここまで到達したということは一致する線分はなかった
    IsLineExists = False
End Function
'************************************************





'************************************************
Private Sub SearchMaxSize()
'現在の保持データ内の最大寸法を取得
'最も小さい寸法値、最も大きい寸法値を得る
    Dim i As Long   'General Counter
    Dim tXmax As Single, tXmin As Single
    Dim tYmax As Single, tYmin As Single
    Dim tZmax As Single, tZmin As Single

    If UBound(m_objLine) > 0 Then
        tXmin = m_objLine(1).FromX
        tYmin = m_objLine(1).FromY
        tZmin = m_objLine(1).FromZ
        For i = 1 To UBound(m_objLine)
            With m_objLine(i)
                GetMax tXmax, .FromX
                GetMax tXmax, .ToX
                GetMin tXmin, .FromX
                GetMin tXmin, .ToX
                GetMax tYmax, .FromY
                GetMax tYmax, .ToY
                GetMin tYmin, .FromY
                GetMin tYmin, .ToY
                GetMax tZmax, .FromZ
                GetMax tZmax, .ToZ
                GetMin tZmin, .FromZ
                GetMin tZmin, .ToZ
            End With
        Next i
        m_sngXMax = Abs(tXmax - tXmin)
        m_sngYMax = Abs(tYmax - tYmin)
        m_sngZMax = Abs(tZmax - tZmin)
    End If
End Sub
'************************************************

Private Function GetMax(ByRef maxval As Single, ByVal val As Single) As Single
    If maxval <= val Then
        maxval = val
    End If
    GetMax = maxval
End Function

Private Function GetMin(ByRef minval As Single, ByVal val As Single) As Single
    If minval >= val Then
        minval = val
    End If
    GetMin = minval
End Function
