VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGuriData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Class Name:   CGuriData
' Author:       平田 敦, G-Soft
' Date:         98/09/20
' Description:  立体ぐりぐりのデータを格納する
' Edit History:
'
Option Explicit
Option Base 1       '立体のデータを格納する配列が、
                    '1から始まったほうが都合が良い。
'
' PUBLIC: ******************************************
' Public data members

' PRIVATE: *****************************************
' Private data members

Private m_sngXMax As Single
Private m_sngYMax As Single
Private m_sngZMax As Single
Private m_lngNumberOfLines As Long
Private m_sngX() As Single
Private m_sngY() As Single
Private m_sngZ() As Single
Private m_strComment As String
'
Private m_lngLongitude As Long      '経度
Private m_lngLatitude As Long       '緯度
Private m_sngRatio As Single        '図形表示の拡大率
'
Private m_strFileName As String     'ファイル名はフルパスで

'視点の角度から計算した座標値
Private m_sngTransXMax As Single
Private m_sngTransYMax As Single
Private m_sngTransZMax As Single
Private m_sngTransX() As Single
Private m_sngTransY() As Single
Private m_sngTransZ() As Single


' PUBLIC: *****************************************
' Public property procedures


Public Property Get XMax() As Single
    XMax = m_sngXMax
End Property
Public Property Let XMax(ByVal XVal As Single)
    m_sngXMax = XVal
End Property

Public Property Get YMax() As Single
    YMax = m_sngYMax
End Property
Public Property Let YMax(ByVal YVal As Single)
    m_sngYMax = YVal
End Property

Public Property Get ZMax() As Single
    ZMax = m_sngZMax
End Property
Public Property Let ZMax(ByVal ZVal As Single)
    m_sngZMax = ZVal
End Property

Public Property Get NumberOfLines() As Long
    NumberOfLines = m_lngNumberOfLines
End Property
Public Property Let NumberOfLines(ByVal NVal As Long)
    m_lngNumberOfLines = NVal
End Property

Public Property Get x(ByVal i As Long) As Single
    x = m_sngX(i)
End Property
Public Property Let x(ByVal i As Long, ByVal XVal As Single)
    ReDim Preserve m_sngX(i)
    m_sngX(i) = XVal
End Property

Public Property Get y(ByVal i As Long) As Single
    y = m_sngY(i)
End Property
Public Property Let y(ByVal i As Long, ByVal YVal As Single)
    ReDim Preserve m_sngY(i)
    m_sngY(i) = YVal
End Property

Public Property Get Z(ByVal i As Long) As Single
    Z = m_sngZ(i)
End Property
Public Property Let Z(ByVal i As Long, ByVal ZVal As Single)
    ReDim Preserve m_sngZ(i)
    m_sngZ(i) = ZVal
End Property

Public Property Get Comment() As String
    Comment = m_strComment
End Property

Public Property Let Comment(ByVal NewValue As String)
    m_strComment = NewValue
End Property

Public Property Get Longitude() As Long
    Longitude = m_lngLongitude
End Property

Public Property Let Longitude(ByVal NewValue As Long)
    m_lngLongitude = NewValue
End Property

Public Property Get Latitude() As Long
    Latitude = m_lngLatitude
End Property

Public Property Let Latitude(ByVal NewValue As Long)
    m_lngLatitude = NewValue
End Property
Public Property Get Ratio() As Single
    Ratio = m_sngRatio
End Property

Public Property Let Ratio(ByVal NewValue As Single)
    m_sngRatio = NewValue
End Property

Public Property Get filename() As String

End Property

Public Property Let filename(ByVal Name As String)
    m_strFileName = Name
End Property



Public Property Get TransXMax() As Single
    TransXMax = m_sngTransXMax
End Property
Public Property Get TransYMax() As Single
    TransYMax = m_sngTransYMax
End Property
Public Property Get TransZMax() As Single
    TransZMax = m_sngTransZMax
End Property

Public Property Get TransX(ByVal i As Long) As Single
    TransX = m_sngTransX(i)
End Property
'Public Property Let TransX(ByVal i As Long, ByVal XVal As Single)
'
'End Property


Public Property Get TransY(ByVal i As Long) As Single
    TransY = m_sngTransY(i)
End Property
'Public Property Let TransY(ByVal i As Long, ByVal YVal As Single)
'
'End Property

Public Property Get TransZ(ByVal i As Long) As Single
    TransZ = m_sngTransZ(i)
End Property
'Public Property Let TransZ(ByVal i As Long, ByVal ZVal As Single)
'
'End Property






' PUBLIC: ******************************************
' Public procedures

' PRIVATE: *****************************************
' Private procedures

'************************************************
'目的：     Rotation　図形の座標値を視点の位置から見えるように変換
'引数：     なし
'戻り値：   なし
'************************************************
Public Sub Rotation()

    Debug.Print "CGuriData.Rotation Start"
    Dim lon As Single   '経度　radian
    Dim lat As Single   '緯度　radian
    Dim i As Long       'general counter
    Dim xx, yy, zz As Single '座標計算値の一時保存場所
    '

    '角度を度からラジアンに変換する｡
    lon = m_lngLongitude / 180 * PI
    lat = -m_lngLatitude / 180 * PI
    '
    
    ReDim Preserve m_sngTransX(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngTransY(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngTransZ(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngX(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngY(m_lngNumberOfLines * 2)
    ReDim Preserve m_sngZ(m_lngNumberOfLines * 2)
    
    xx = m_sngXMax * Cos(lon) + m_sngZMax * Sin(lon)
    yy = m_sngYMax
    zz = -m_sngXMax * Sin(lon) + m_sngZMax * Cos(lon)
    m_sngTransXMax = xx
    m_sngTransYMax = yy * Cos(lat) + zz * Sin(lat)
    m_sngTransZMax = -yy * Sin(lat) + zz * Cos(lat)
    
    For i = 1 To m_lngNumberOfLines * 2
        xx = m_sngX(i) * Cos(lon) + m_sngZ(i) * Sin(lon)
        yy = m_sngY(i)
        zz = -m_sngX(i) * Sin(lon) + m_sngZ(i) * Cos(lon)
    
        m_sngTransX(i) = xx
        m_sngTransY(i) = yy * Cos(lat) + zz * Sin(lat)
        m_sngTransZ(i) = -yy * Sin(lat) + zz * Cos(lat)
    Next i
End Sub
'************************************************



Private Sub Class_Initialize()
    Debug.Print "CGuriData.Initialize called"
End Sub

Private Sub Class_Terminate()
    Debug.Print "CGuriData.Terminate"
End Sub
