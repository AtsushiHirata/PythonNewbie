VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGuriView"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
' Class Name:   CGuriView
' Author:       平田 敦, G-Soft
' Date:         98/09/20
' Description:  立体ぐりぐりのデータを表示する
' Edit History:
'
Option Explicit
'
'
' PUBLIC: ******************************************
' Public data members

' PRIVATE: *****************************************
' Private data members

'読み込むデータのファイル名がフルパスで格納される。
Private m_strPathAndFileName As String
'図が表示されるピクチャボックス
Private m_ctlDestPicBox As PictureBox
'作業領域となるピクチャボックス｡
Private m_ctlDestPicBack As PictureBox

'図形の座標値やコメントなどが格納されるオブジェクト
Private m_objGuriData As CGuriData
'ファイル処理に使用するオブジェクト
Private m_objFile As CFile


' PUBLIC: *****************************************
' Public property procedures

Public Property Let PathAndFileName(ByVal Name As String)
    m_strPathAndFileName = Name
    'そのファイル名のデータを読み込む。
    '読み込みには、CFileクラスを使用する。格納先はCGuriDataクラス
    Set m_objGuriData = New CGuriData
    Set m_objFile = New CFile
    m_objFile.Load m_strPathAndFileName, m_objGuriData
    '読み込んだデータをピクチャボックスに表示する。
    Call Draw
    Debug.Print "CGuriView.PathAndFileName Set And Drawn"
End Property


Public Property Let DestPicBox(ByRef PicBox As Control)
    Set m_ctlDestPicBox = PicBox
    m_ctlDestPicBox.AutoRedraw = True
    m_ctlDestPicBox.ScaleMode = vbPixels
End Property

Public Property Let DestPicBack(ByRef picBack As Control)
    Set m_ctlDestPicBack = picBack
    picBack.Visible = False
    picBack.AutoRedraw = True
End Property

' PUBLIC: ***************************************
' Public procedure

' PRIVATE: **************************************
' Private procedure

'------------------------------------------------
Private Sub Draw()
'目的説明：     読み込まれたデータをピクチャボックスに表示する。
'引数：         なし
'返り値：       なし
'作成者：       平田 敦、G-Soft
'作成年月日：   98/09/22
'変更履歴：
'------------------------------------------------
    Debug.Print "CGuriView.Draw Start"
'
    Dim i As Integer            'general counter
    Dim xx As String, yy As String, zz As String
    Dim ret As Integer
    Dim PenColor As Long
    
    PenColor = vbBlack
    '
    '視点位置によるデータの変換
    m_objGuriData.Rotation
    '表示されるピクチャボックスの各プロパティを調整
    With m_ctlDestPicBack
        '裏画面の消去
        .Cls
        '座標系の調整
        m_objGuriData.Ratio = .Width / 2 _
            / (2 * (m_objGuriData.XMax + _
                    m_objGuriData.YMax + _
                    m_objGuriData.ZMax) / 3)
        Debug.Print "m_ctlDestPicBack.ScaleWidth" & .ScaleWidth
        Debug.Print "Screen.TwipsPerPixelX" & Screen.TwipsPerPixelX
        Debug.Print "XMax " & m_objGuriData.XMax
        Debug.Print "CGuriData.Ratio = " & m_objGuriData.Ratio
        .ScaleWidth = .Width / m_objGuriData.Ratio
            Debug.Print ".ScaleWidth" & .ScaleWidth
        .ScaleHeight = -.Height / m_objGuriData.Ratio
            Debug.Print ".ScaleHeight" & .ScaleHeight
        .ScaleLeft = -.Width / (2 * m_objGuriData.Ratio) + _
                m_objGuriData.TransXMax
        .ScaleTop = .Height / (2 * m_objGuriData.Ratio) + _
                    m_objGuriData.TransYMax
    End With
    With m_ctlDestPicBox
        .ScaleWidth = m_ctlDestPicBack.ScaleWidth
        .ScaleHeight = m_ctlDestPicBack.ScaleHeight
        .ScaleLeft = m_ctlDestPicBack.ScaleLeft
        .ScaleTop = m_ctlDestPicBack.ScaleTop
    End With
    '
    '図形の描画
    'If FigFlags.DrawingMode = True Then PenColor = vbBlack Else PenColor = vbWhite
    With m_objGuriData
        For i = 1 To .NumberOfLines * 2 Step 2
            m_ctlDestPicBack.Line (.TransX(i), .TransY(i)) _
                            -(.TransX(i + 1), .TransY(i + 1)), _
                            PenColor
        Next i
    End With
    '
    '作業用ピクチャボックスから表ピクチャボックスへ転送！
    ret = BitBlt(m_ctlDestPicBox.hdc, 0, 0, _
                        m_ctlDestPicBox.Width, _
                        m_ctlDestPicBox.Height, _
                        m_ctlDestPicBack.hdc, _
                        0, 0, _
                        SRCCOPY)
    m_ctlDestPicBox.Refresh
    m_ctlDestPicBack.Refresh
End Sub



