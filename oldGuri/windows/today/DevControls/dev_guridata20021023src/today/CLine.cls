VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CLine"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private m_sngFromX As Single
Private m_sngFromY As Single
Private m_sngFromZ As Single
Private m_sngToX As Single
Private m_sngToY As Single
Private m_sngToZ As Single


Private m_objLogging As CLogging





Public Property Get FromX() As Single
    FromX = m_sngFromX
End Property

Public Property Let FromX(ByVal NewValue As Single)
    m_sngFromX = NewValue
End Property

Public Property Get FromY() As Single
    FromY = m_sngFromY
End Property

Public Property Let FromY(ByVal NewValue As Single)
    m_sngFromY = NewValue
End Property

Public Property Get FromZ() As Single
    FromZ = m_sngFromZ
End Property

Public Property Let FromZ(ByVal NewValue As Single)
    m_sngFromZ = NewValue
End Property

Public Property Get ToX() As Single
    ToX = m_sngToX
End Property

Public Property Let ToX(ByVal NewValue As Single)
    m_sngToX = NewValue
End Property

Public Property Get ToY() As Single
    ToY = m_sngToY
End Property

Public Property Let ToY(ByVal NewValue As Single)
    m_sngToY = NewValue
End Property

Public Property Get ToZ() As Single
    ToZ = m_sngToZ
End Property

Public Property Let ToZ(ByVal NewValue As Single)
    m_sngToZ = NewValue
End Property

Private Sub Class_Initialize()
    Set m_objLogging = New CLogging
'    m_objLogging.Log "●CLineのインスタンスが生成されました。"
    Debug.Print "CLine.Initialize"
End Sub

Private Sub Class_Terminate()
'    m_objLogging.Log "○CLineのインスタンスが破棄されました。"
End Sub
