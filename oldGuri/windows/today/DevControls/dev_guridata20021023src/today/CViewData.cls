VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CViewData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private m_objLogging As CLogging

Private Sub Class_Initialize()
    Set m_objLogging = New CLogging
    m_objLogging.Log "●CViewDataが生成されました"
End Sub

Private Sub Class_Terminate()
    m_objLogging.Log "○CViewDataが破棄されました"
End Sub
