VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CWavePlay"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'==========================================================
'Name:      CWavePlay
'Author:    平田 敦(a_hirata@siscom.or.jp)
'Date:      2003/02/12
'Revision:


'------------------------------
'         音を鳴らす関数
'------------------------------
Private Declare Function sndPlaySound Lib "winmm.dll" Alias "sndPlaySoundA" _
    (ByVal lpszSoundName As String, ByVal uFlags As Long) As Long
Const SND_ASYNC = &H1    '非同期再生
Const SND_SYNC = &H0     '同期再生
Const SND_LOOP = &H8     'ループ再生
Const SND_MEMORY = &H4   'メモリ上のサウンドを再生
Const SND_NODEFAULT = &H2    '失敗してもシステムサウンドを再生しない。
Const SND_NOSTOP = &H10  '再生中なら再生しない。

'************************************************
Public Sub Play(Filename As String, Mode As String)
'目的説明:      wavファイルを再生。同期と非同期を選択できる。
'
'入力引数:      Filename As String  '再生するファイル名フルパス
'               Mode As Integer     '再生モード。
'戻り値:        なし
'------------------------------------------------
    Dim ret As Integer
    '
    Select Case Mode
        Case "ASYNC"   '非同期再生
            ret = sndPlaySound(Filename, SND_ASYNC Or SND_NODEFAULT)
        Case "SYNC"    '同期再生
            ret = sndPlaySound(Filename, SND_SYNC Or SND_NODEFAULT)
        Case Else
            'do nothing
    End Select
End Sub
'************************************************


