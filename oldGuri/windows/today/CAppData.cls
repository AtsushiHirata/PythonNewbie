VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAppData"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'==========================================================
'Name:        AppData   グリグリのアプリケーションデータを管理
'                       グリグリのデータファイル以外の部分。
'Author:      平田 敦(a_hirata@siscom.or.jp)
'CreateDate:  2003/02/26
'ChangeDate:
'Revision:


'------------------------------------------------
' Data Fields
'------------------------------------------------

Private m_strDataPath As String     'ファイルロード、セーブにあたって開くべきフォルダのパス
Private m_strFileName As String     'データファイルのファイル名（フルパス）

Private m_intLongitude As Integer   '視点の経度
Private m_intLatitude As Integer    '視点の緯度
Private m_sngRatio As Single        '表示倍率
Private m_sngRatioMax As Single     '表示倍率最大値
Private m_sngRatioMin As Single     '表示倍率最小値
Private m_strMode As String         '表示モードか、"VIEW_MODE"
                                    '作図モードか。"DRAW_MODE"
                                    '将来モードが増えたときのために、
                                    'ストリングでモード判別。
Private m_intUnitLength As Integer  '描画ポインタの単位移動距離
Private m_intUnitAngle As Integer   '図形の単位回転角
Private m_intMaxSize As Integer     'ユーザ図形描画の許可最大サイズ

Private m_strFormSize As String     'メインフォームの大きさ
Private m_intPointerSize As Integer '描画ポインタの大きさ

'--------------------------------------------閲覧モード
Private m_lngVIEW_MODE_FigColor As Long     'ワイヤーフレームの色
Private m_lngVIEW_MODE_BackColor As Long    '背景の色
'--------------------------------------------作図モード
Private m_lngDRAW_MODE_FigColor As Long     'ワイヤーフレームの色
Private m_lngDRAW_MODE_BackColor As Long    '背景の色
Private m_lngTempLineColor As Long          '未決定の線の色。線引き中の線の色。
Private m_lngPointerColor As Long           'ポインターの色
Private m_lngSecondaryPointerColor As Long  'ポインターの色その２。線分の始点が決定したときのポインタの色

Private m_strContinuousTurningDirection As String '連続回転の方向　デフォルトは左から
                                                  'そのために、左の前の下をセット


Private m_intTickTime As Integer    '図形回転中の静止時間　単位ｍ秒（ミリ秒）


Private m_intPointerFromX As Integer    '作図ポインタ　 始点の位置
Private m_intPointerFromY As Integer
Private m_intPointerFromZ As Integer
Private m_lngPointerFromColor As Long   '　　　　　　　 始点の色

Private m_intPointerToX As Integer      '作図ポインタ　 終点の位置
Private m_intPointerToY As Integer
Private m_intPointerToZ As Integer
Private m_lngPointerToColor As Long     '               終点の色

Private m_strDrawingCondition As String     '作図状態を保持
    '�@始点未決定　＆　終点未決定   "FROM"
    '�A始点決定　　＆　終点未決定   "TO"
    '終点が決定したら�@にもどす
    
Private m_strDifficulty As String       '作図の難易度
    '"DIFFICULTY_BEGINNER"
    '"DIFFICULTY_MIDIUM"
    '"DIFFICULTY_SENIOR"
    
Private m_booThirdAngleProjectionMethodMode As Boolean  '現在三面図表示中かどうか

Private m_strFileNameHistory() As String    'データファイルの履歴
Private m_intFileNameHistoryNum As Integer  'データファイルの履歴数

Private m_intAxisLineLength As Integer      '座標軸の長さ
Private m_intAxisLineSize As Integer        '座標軸の太さ
Private m_intSubAxisLineLength As Integer   '補助座標軸の長さ
Private m_intSubAxisLineSize As Integer     '補助座標軸の太さ

Private m_booProInterface As Boolean        'プロフェッショナルインターフェイスが有効か
                                            '初期値は無効
Private m_intLineNumber As Long             'プロインターフェイスに表示中の線分の番号





'================================================
' コンストラクタ
'================================================

Private Sub Class_Initialize()
    '- - - - - - - - - - - - - - - - - - - - - -
    'インストールデフォルト
    ' 今後、開発が進んだ段階で、レジストリなり、
    ' オプションファイルから別の設定を読み込むこ
    ' とを見込んでいます。
    '- - - - - - - - - - - - - - - - - - - - - -
    m_strDataPath = App.Path + "\data"                     'ファイルロード、セーブにあたって開くべきフォルダのパス
    m_strFileName = App.Path + "\data\sample\guri01.gri"   'データファイルのファイル名（フルパス）
    m_intLongitude = -45                '視点の経度
    m_intLatitude = 35                  '視点の緯度
    m_sngRatio = 1                      '表示倍率
    m_sngRatioMax = 3
    m_sngRatioMin = 0.1
    m_strMode = "VIEW_MODE"             '表示モードか、"VIEW_MODE"
                                        '作図モードか。"DRAW_MODE"
                                        '将来モードが増えたときのために、
                                        'ストリングでモード判別。
    m_intUnitLength = 20                '描画ポインタの単位移動距離
    m_intUnitAngle = 5                  '図形の単位回転角
    m_intMaxSize = 200                  'ユーザ図形描画の許可最大サイズ
    m_strFormSize = "800x400"           '勝手ながら、平田の開発環境を標準と・・・
    m_intPointerSize = 5                '描画ポインタのサイズ。作図モードでは５
    m_intAxisLineLength = 200           '座標軸の長さ
    m_intSubAxisLineLength = 32767      '補助座標軸の長さ
    m_intAxisLineSize = 2               '座標軸の太さ
    m_intSubAxisLineSize = 1            '補助座標軸の太さ
    
                                        'VIEW_MODE
    m_lngVIEW_MODE_FigColor = vbWhite   'ワイヤーフレームの色
    m_lngVIEW_MODE_BackColor = 13056    '= RGB(0, 51, 0)   深い緑 '背景の色
                                        'DRAW_MODE
    m_lngDRAW_MODE_FigColor = vbBlack   'ワイヤーフレームの色
    m_lngDRAW_MODE_BackColor = vbWhite  '背景の色
    m_lngTempLineColor = vbRed          '未決定の線の色。線引き中の線の色。
    m_lngPointerColor = vbRed           'ポインターの色
    m_lngSecondaryPointerColor = vbBlack 'ポインターの色その２。線分の始点が決定したときのポインタの色

    m_strContinuousTurningDirection = "DOWN"    '連続回転の方向　デフォルトは左殻回し始めるために、右にセットしておく
    
    m_intTickTime = 100                  '100ミリ秒ごとに図形回転
    
                                        '作図ポインタの位置
    m_intPointerFromX = 0
    m_intPointerFromY = 0
    m_intPointerFromZ = 0
    m_intPointerToX = 0
    m_intPointerToY = 0
    m_intPointerToZ = 0
        
    m_lngPointerFromColor = vbRed
    m_lngPointerToColor = vbBlack
    
    m_strDrawingCondition = "FROM"
    
    m_strDifficulty = "DIFFICULTY_MIDIUM"
    
    m_booThirdAngleProjectionMethodMode = False
    
    m_intFileNameHistoryNum = 5     '履歴は５個保存する
    '履歴配列の初期化
    ReDim m_strFileNameHistory(m_intFileNameHistoryNum)
    'プロフェッショナルインターフェイス
    m_booProInterface = False   '初期値は無効
    m_intLineNumber = 1
End Sub




'------------------------------------------------
' Public Methods
'------------------------------------------------



'------------------------------------------------
Public Sub ClearData()
'目的：図形に関するデータのクリヤ
'------------------------------------------------
    With Me
        .filename = ""
        '.DataPath パスは保管
        .DrawingCondition = "FROM"
        .PointerFromX = 0
        .PointerFromY = 0
        .PointerFromZ = 0
        .PointerToX = 0
        .PointerToY = 0
        .PointerToZ = 0
        .ProIF_LineNumber = 0
        .Ratio = 1
    End With
End Sub


'------------------------------------------------
'目的：データファイルのファイルネーム（フルパス）へのアクセッサ
Public Property Get filename() As String
    filename = m_strFileName
End Property

Public Property Let filename(ByVal NewValue As String)
    m_strFileName = NewValue
    m_strDataPath = SetDataPathFromFullPathFileName(NewValue)
    Call SetFileNameToHistory(NewValue)
End Property


'------------------------------------------------
'目的：データフォルダのパス取得、設定
Public Property Get DataPath() As String
    DataPath = m_strDataPath
End Property

Public Property Let DataPath(ByVal NewValue As String)
    m_strDataPath = NewValue
End Property



'------------------------------------------------
'目的：視点の経度へのアクセッサ
'      −１８０から１８０度の間で正規化
Public Property Get Longitude() As Integer
    Longitude = m_intLongitude
End Property

Public Property Let Longitude(ByVal NewValue As Integer)
    m_intLongitude = NewValue Mod 360
    If m_intLongitude > 180 Then
        m_intLongitude = m_intLongitude - 360
    End If
    If m_intLongitude < -180 Then
        m_intLongitude = m_intLongitude + 360
    End If
End Property


'------------------------------------------------
'目的：視点の緯度へのアクセッサ
'      −１８０から１８０度の間で正規化
Public Property Get Latitude() As Integer
    Latitude = m_intLatitude
End Property

Public Property Let Latitude(ByVal NewValue As Integer)
    m_intLatitude = NewValue Mod 360
    If m_intLatitude > 180 Then
        m_intLatitude = m_intLatitude - 360
    End If
    If m_intLatitude < -180 Then
        m_intLatitude = m_intLatitude + 360
    End If
End Property


'------------------------------------------------
'目的：図形の拡大表示率へのアクセッサ
Public Property Get Ratio() As Single
    Ratio = m_sngRatio
End Property

Public Property Let Ratio(ByVal NewValue As Single)
    m_sngRatio = NewValue
    If m_sngRatio < m_sngRatioMin Then m_sngRatio = m_sngRatioMin
    If m_sngRatio > m_sngRatioMax Then m_sngRatio = m_sngRatioMax
End Property


'------------------------------------------------
'目的：「現在のモードが閲覧・作図モードどちらか」へのアクセッサ
'       "VIEW_MODE"　閲覧モード
'       "DRAW_MODE"　作図モード
Public Property Get Mode() As String
    Mode = m_strMode
End Property

Public Property Let Mode(ByVal NewValue As String)
    m_strMode = NewValue
    Select Case m_strMode
        Case "VIEW_MODE"
            'm_intPointerSize = 0
        Case "DRAW_MODE"
            'm_intPointerSize = 5
        Case Else
            'do nothing
    End Select
End Property



'------------------------------------------------
'目的：作図ポインタの単位移動距離へのアクセッサ
Public Property Get UnitLength() As Integer
    UnitLength = m_intUnitLength
End Property

Public Property Let UnitLength(ByVal NewValue As Integer)
    m_intUnitLength = NewValue
End Property



'------------------------------------------------
'目的：図形の単位回転角へのアクセッサ
Public Property Get UnitAngle() As Integer
    UnitAngle = m_intUnitAngle
End Property

Public Property Let UnitAngle(ByVal NewValue As Integer)
    m_intUnitAngle = NewValue
End Property



'------------------------------------------------
'目的：ユーザ図形描画の許可最大サイズへのアクセッサ
Public Property Get MaxSize() As Integer
    MaxSize = m_intMaxSize
End Property

Public Property Let MaxSize(ByVal NewValue As Integer)
    m_intMaxSize = NewValue
End Property


'メインフォームの大きさ
Public Property Get FormSize() As String
    FormSize = m_strFormSize
End Property

Public Property Let FormSize(ByVal NewValue As String)
    m_strFormSize = NewValue
End Property


'作図ポインタの大きさ（始点、終点共用）
Public Property Get PointerSize() As Integer
    PointerSize = m_intPointerSize
End Property

Public Property Let PointerSize(ByVal NewValue As Integer)
    m_intPointerSize = NewValue
End Property


'閲覧モード時の図形の（線分の）色
Public Property Get VIEW_MODE_FigColor() As Long
    VIEW_MODE_FigColor = m_lngVIEW_MODE_FigColor
End Property

Public Property Let VIEW_MODE_FigColor(ByVal NewValue As Long)
    m_lngVIEW_MODE_FigColor = NewValue
End Property
'閲覧モード時の背景色
Public Property Get VIEW_MODE_BackColor() As Long
    VIEW_MODE_BackColor = m_lngVIEW_MODE_BackColor
End Property

Public Property Let VIEW_MODE_BackColor(ByVal NewValue As Long)
    m_lngVIEW_MODE_BackColor = NewValue
End Property

'作図モード時の図形の（線分の）色
Public Property Get DRAW_MODE_FigColor() As Long
    DRAW_MODE_FigColor = m_lngDRAW_MODE_FigColor
End Property

Public Property Let DRAW_MODE_FigColor(ByVal NewValue As Long)
    m_lngDRAW_MODE_FigColor = NewValue
End Property

'作図表示時の背景の色
Public Property Get DRAW_MODE_BackColor() As Long
    DRAW_MODE_BackColor = m_lngDRAW_MODE_BackColor
End Property

Public Property Let DRAW_MODE_BackColor(ByVal NewValue As Long)
    m_lngDRAW_MODE_BackColor = NewValue
End Property

'未決定線分の色
Public Property Get TempLineColor() As Long
    TempLineColor = m_lngTempLineColor
End Property

Public Property Let TempLineColor(ByVal NewValue As Long)
    m_lngTempLineColor = NewValue
End Property

'始点指示用ポインタの色
Public Property Get PointerFromColor() As Long
    PointerFromColor = m_lngPointerFromColor
End Property

Public Property Let PointerFromColor(ByVal NewValue As Long)
    m_lngPointerFromColor = NewValue
End Property

'終点指示用ポインタの色
Public Property Get PointerToColor() As Long
    PointerToColor = m_lngPointerToColor
End Property

Public Property Let PointerToColor(ByVal NewValue As Long)
    m_lngPointerToColor = NewValue
End Property

'連続回転表示の回転方向へのアクセッサ
Public Property Get ContinuousTurningDirection() As String
    ContinuousTurningDirection = m_strContinuousTurningDirection
End Property

Public Property Let ContinuousTurningDirection(ByVal NewValue As String)
    If NewValue = "LEFT" Or NewValue = "RIGHT" Or NewValue = "UP" Or NewValue = "DOWN" Then
        m_strContinuousTurningDirection = NewValue
    Else
        m_strContinuousTurningDirection = "LEFT"
    End If
End Property

'図形回転時のディレイタイム
Public Property Get TickTime() As Integer
    TickTime = m_intTickTime
End Property

Public Property Let TickTime(ByVal NewValue As Integer)
    m_intTickTime = NewValue
    If m_intTickTime < 0 Then m_intTickTime = -m_intTickTime
End Property



'------------------------------------------------
'作図ポインタの座標値のアクセッサ
Public Property Get PointerFromX() As Integer
    PointerFromX = m_intPointerFromX
End Property

Public Property Let PointerFromX(ByVal NewValue As Integer)
    m_intPointerFromX = NewValue
    If m_intPointerFromX > m_intMaxSize Then m_intPointerFromX = m_intMaxSize
    If m_intPointerFromX < 0 Then m_intPointerFromX = 0
End Property

Public Property Get PointerFromY() As Integer
    PointerFromY = m_intPointerFromY
End Property

Public Property Let PointerFromY(ByVal NewValue As Integer)
    m_intPointerFromY = NewValue
    If m_intPointerFromY > m_intMaxSize Then m_intPointerFromY = m_intMaxSize
    If m_intPointerFromY < 0 Then m_intPointerFromY = 0
End Property

Public Property Get PointerFromZ() As Integer
    PointerFromZ = m_intPointerFromZ
End Property

Public Property Let PointerFromZ(ByVal NewValue As Integer)
    m_intPointerFromZ = NewValue
    If m_intPointerFromZ > m_intMaxSize Then m_intPointerFromZ = m_intMaxSize
    If m_intPointerFromZ < 0 Then m_intPointerFromZ = 0
End Property

Public Property Get PointerToX() As Integer
    PointerToX = m_intPointerToX
End Property

Public Property Let PointerToX(ByVal NewValue As Integer)
    m_intPointerToX = NewValue
    If m_intPointerToX > m_intMaxSize Then m_intPointerToX = m_intMaxSize
    If m_intPointerToX < 0 Then m_intPointerToX = 0
End Property

Public Property Get PointerToY() As Integer
    PointerToY = m_intPointerToY
End Property

Public Property Let PointerToY(ByVal NewValue As Integer)
    m_intPointerToY = NewValue
    If m_intPointerToY > m_intMaxSize Then m_intPointerToY = m_intMaxSize
    If m_intPointerToY < 0 Then m_intPointerToY = 0
End Property

Public Property Get PointerToZ() As Integer
    PointerToZ = m_intPointerToZ
End Property

Public Property Let PointerToZ(ByVal NewValue As Integer)
    m_intPointerToZ = NewValue
    If m_intPointerToZ > m_intMaxSize Then m_intPointerToZ = m_intMaxSize
    If m_intPointerToZ < 0 Then m_intPointerToZ = 0
End Property
'------------------------------------------------

'作図状態へのアクセッサ
Public Property Get DrawingCondition() As String
    DrawingCondition = m_strDrawingCondition
End Property

Public Property Let DrawingCondition(ByVal NewValue As String)
    If (NewValue = "FROM" Or NewValue = "TO") Then
        m_strDrawingCondition = NewValue
    Else
        m_strDrawingCondition = "FROM"
    End If
End Property


Public Property Get Difficulty() As String
    Difficulty = m_strDifficulty
End Property

Public Property Let Difficulty(ByVal NewValue As String)
    If (NewValue = "DIFFICULTY_BEGINNER" Or _
       NewValue = "DIFFICULTY_MIDIUM" Or _
       NewValue = "DIFFICULTY_SENIOR") Then
        m_strDifficulty = NewValue
    Else
        m_strDifficulty = "DIFFICULTY_MIDIUM"
    End If
End Property


Public Property Get ThirdAngleProjectionMethodMode() As Boolean
    ThirdAngleProjectionMethodMode = m_booThirdAngleProjectionMethodMode
End Property

Public Property Let ThirdAngleProjectionMethodMode(ByVal NewValue As Boolean)
    m_booThirdAngleProjectionMethodMode = NewValue
End Property







'------------------------------------------------
Private Function SetDataPathFromFullPathFileName(ByVal FullPath As String) As String
'データファイルのフルパスファイルネームから、データパスを切り出す
    Dim i As Integer    'Counter
    For i = Len(FullPath) To 1 Step -1
        If Mid(FullPath, i, 1) = "\" Or Mid(FullPath, i, 1) = "/" Then
            'ファイルネームを除いたパスの部分の位置
            '\を除いて返す
            'MsgBox (Left(FullPath, i - 1))
            SetDataPathFromFullPathFileName = Left(FullPath, i - 1)
            Exit Function
        End If
    Next i
    '見つからなければ、アプリケーションのデータパスを返す。
    SetDataPathFromFullPathFileName = App.Path + "\data"
End Function


'------------------------------------------------
Public Function GetHistory(ByVal index As Integer) As String
'目的：データファイルの履歴を返す
'引数：index As Integer 履歴配列の引数　０から始まる。
'                       ０が最新
'返値：データファイル名をフルパスで
'------------------------------------------------
'履歴の保存はFilenameプロパティの変更があった時点で行う。
    If index <= m_intFileNameHistoryNum - 1 And index >= 0 Then
        GetHistory = m_strFileNameHistory(index)
    Else
        GetHistory = ""
    End If
End Function

'------------------------------------------------
Private Sub SetFileNameToHistory(ByVal NewFileName As String)
'目的：ファイル名を履歴に加える。重複のある場合の処理など。
'引数：NewFileName As String 新しくセットされるファイル名
'------------------------------------------------
'＜処理の手順＞
'・重複するものが無ければ、先頭（添え字０）にセット。
'　過去のものをひとつずつずらす。
'・重複するものがあれば、重複するものを削除し、間を詰める。
'　その後に先頭にセット。
'・あふれたデータは末尾（添え字m_intFileNameHistoryNum）から消去
'------------------------------------------------
    Dim i As Integer          'カウンタ
    Dim j As Integer          'カウンタ
    For i = 0 To m_intFileNameHistoryNum - 1
        If m_strFileNameHistory(i) = NewFileName Then
            '重複あり。
            For j = i To m_intFileNameHistoryNum - 1 - 1
                '削除して間をつめる
                m_strFileNameHistory(j) = m_strFileNameHistory(j + 1)
            Next j
        Else
            '重複なし。なにもしない。
        End If
    Next i
    '履歴をひとつずつずらす
    For i = m_intFileNameHistoryNum - 1 To 0 + 1 Step -1
        m_strFileNameHistory(i) = m_strFileNameHistory(i - 1)
    Next i
    '履歴先頭に新しいデータをセットする
    m_strFileNameHistory(0) = NewFileName
End Sub

'------------------------------------------------
'データファイルの履歴をいくつ残すかを設定。取得。
'設定は不可としておく。
Public Property Get FileNameHistoryNum() As Integer
    FileNameHistoryNum = m_intFileNameHistoryNum
End Property

'Public Property Let FileNameHistoryNum(ByVal NewValue As Integer)
'    If NewValue >= 0 Then
'        m_intFileNameHistoryNum = NewValue
'    Else
'        m_intFileNameHistoryNum = 0
'    End If
'End Property

'------------------------------------------------
Public Sub SetHistory(ByVal index As Integer, ByVal filename As String)
'指定された添え字の位置に履歴をセット。
'できれば使いたくないメソッドだが、起動時に設定情報から読み込むために必要になったため。
'------------------------------------------------
    If index >= 0 And index < m_intFileNameHistoryNum - 1 And filename <> "" Then
        m_strFileNameHistory(index) = filename
    End If
End Sub


Public Property Get AxisLineLength() As Integer
    AxisLineLength = m_intAxisLineLength
End Property

Public Property Let AxisLineLength(ByVal NewValue As Integer)
    m_intAxisLineLength = NewValue
End Property

Public Property Get AxisLineSize() As Integer
    AxisLineSize = m_intAxisLineSize
End Property

Public Property Let AxisLineSize(ByVal NewValue As Integer)
    m_intAxisLineSize = NewValue
End Property


Public Property Get SubAxisLineLength() As Integer
    SubAxisLineLength = m_intSubAxisLineLength
End Property

Public Property Let SubAxisLineLength(ByVal NewValue As Integer)
    m_intSubAxisLineLength = NewValue
End Property

Public Property Get SubAxisLineSize() As Integer
    SubAxisLineSize = m_intSubAxisLineSize
End Property

Public Property Let SubAxisLineSize(ByVal NewValue As Integer)
    m_intSubAxisLineSize = NewValue
End Property


Public Property Get ProInterface() As Boolean
    ProInterface = m_booProInterface
End Property

Public Property Let ProInterface(ByVal NewValue As Boolean)
    m_booProInterface = NewValue
End Property

Public Property Get ProIF_LineNumber() As Integer
    ProIF_LineNumber = m_intLineNumber
End Property

Public Property Let ProIF_LineNumber(ByVal NewValue As Integer)
    m_intLineNumber = NewValue
End Property
