VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGuriIniFileHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Sub ReadSetting()
    Dim IniFile As String
    IniFile = App.Path + "\GuriWin33.ini"
    If FileExists(IniFile) = False Then   'iniファイルが無ければ
        Exit Sub
    Else 'iniファイルがあれば
        Dim FileNo As Integer
        Dim TempLine As String
        Dim val As String
        FileNo = FreeFile
        Open IniFile For Input As #FileNo
        'iniファイル読み出し
        Do Until EOF(1)
            Line Input #FileNo, TempLine
            If Left(TempLine, 1) = "[" Then
                '行頭が"["だったらなにもしない
            ElseIf Left(TempLine, 15) = "DataFolderPath=" Then
                val = Mid(TempLine, 16)
                g_AppData.DataPath = val
            ElseIf Left(TempLine, 9) = "DataFile=" Then
                val = Mid(TempLine, 10)
                g_AppData.filename = val
            ElseIf Left(TempLine, 11) = "Difficulty=" Then
                val = Mid(TempLine, 12)
                g_AppData.Difficulty = val
            ElseIf Left(TempLine, 9) = "FormSize=" Then
                val = Mid(TempLine, 10)
                g_AppData.FormSize = val
            ElseIf Left(TempLine, 7) = "History" Then
                Dim index As Integer
                index = Mid(TempLine, 8, 1)
                val = Mid(TempLine, 10)
                g_AppData.SetHistory index, val
            End If
        Loop
        Close #FileNo
    End If
End Sub


Public Sub WriteSetting()
    Dim IniFile As String
    IniFile = App.Path + "\GuriWin33.ini"
        Dim FileNo As Integer
        Dim TempLine As String
        Dim val As String
        FileNo = FreeFile
        Open IniFile For Output As #FileNo
        
        Print #FileNo, "[Version]"
        Print #FileNo, "Version="; VERSION_STRING
        Print #FileNo, "[Last]"
        Print #FileNo, "Last="; Date & " " & Time
        
        Print #FileNo, ""
        
        Print #FileNo, "[DataFilePath]"
        Print #FileNo, "DataFilePath="; g_AppData.DataPath
        Print #FileNo, "[DataFile]"
        Print #FileNo, "DataFile="; g_AppData.filename
        
        Print #FileNo, ""
        
        Print #FileNo, "[Difficulty]"
        Print #FileNo, "Difficulty="; g_AppData.Difficulty
        
        Print #FileNo, ""
        
        Print #FileNo, "[FromSize]"
        Print #FileNo, "FormSize="; g_AppData.FormSize
        
        Print #FileNo, ""
        
        Print #FileNo, "[History]"
        Dim i As Integer 'counter
        For i = 0 To g_AppData.FileNameHistoryNum
            If g_AppData.GetHistory(i) <> "" Then
                Print #FileNo, "History" & i & "="; g_AppData.GetHistory(i)
            End If
        Next i
        
        Close #FileNo
        

End Sub


Private Function FileExists(ByVal filename As String) As Boolean
    FileExists = (Dir(filename) <> "")
End Function

