VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CKeyHandler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'==========================================================
'Name:        KeyHandler    キーボード入力をハンドルする
'Author:      平田 敦(a_hirata@siscom.or.jp)
'CreateDate:  2003/02/22
'ChangeDate:
'Revision:
'------------------------------------------------
'ハンドルするキー入力
'  作図作業時の
'　（Ｘ、Ｙ、Ｚ、Ｏ、シフト、ＢＳ、ＥＳＣ、ＥＮＴＥＲ）
'  立体操作時の
'　（←、→、↓、↑、スペース、ＥＳＣ）


Public Sub KeyDown(KeyCode As Integer, Shift As Integer)
    Select Case KeyCode
        '----------------------------------------
        'ファンクションキーは廃止
        '----------------------------------------
'        Case &H70       'f･1キーが押された
'            'Call Magnify
'        Case &H71       'f･2キーが押された
'            If Shift = 0 Then
'                Call Magnify    'シフトキーが押されてなかったら拡大表示
'            ElseIf Shift = 1 Then
'                Call Reduce     'シフトキーが押されていたら縮小表示
'            End If
'        Case &H72       'f･3キーが押された
'            If Shift = 0 Then
'                Call FrontView
'            ElseIf Shift = 1 Then
'                Call FrontViewSC
'            End If
'        Case &H73       'f･4キーが押された
'            If Shift = 0 Then
'                Call TopView
'            ElseIf Shift = 1 Then
'                Call TopViewSC
'            End If
'        Case &H74       'f･5キーが押された
'            If Shift = 0 Then
'                Call SideView
'            ElseIf Shift = 1 Then
'                Call SideViewSC
'            End If
'        Case &H75       'f･6キーが押された
'            If Shift = 0 Then
'                Call EqualAngleView
'            ElseIf Shift = 1 Then
'                Call EqualAngleViewSC
'            End If
'        Case &H76       'f･7キーが押された
'            Call ThirdAngleProjectionMethod
'        Case &H77       'f･8キーが押された
'            Call DrawFig
        '----------------------------------------
        Case &H1B       'ESCキーが押された
            'Call PushESC(Shift)
            '連続回転中止
            g_GuriController.SetCommand ("CONTINUOUS_TURN_STOP")
        Case &H20       'SPCキーが押された
            'Call ContinuousTurn
            g_GuriController.SetCommand ("CONTINUOUS_TURN")
        Case &H26       '↑キーが押された
            'Call TurnUp
            'MsgBox ("確かに↑だす。")
            g_GuriController.SetCommand ("TURN_UP")
        Case &H28       '↓キーが押された
            'Call TurnDown
            g_GuriController.SetCommand ("TURN_DOWN")
        Case &H25       '←キーが押された
            'Call TurnLeft
            g_GuriController.SetCommand ("TURN_LEFT")
        Case &H27       '→キーが押された
            'Call TurnRight
            g_GuriController.SetCommand ("TURN_RIGHT")
    End Select
    'If FigFlags.DrawingMode = True Then
    If g_AppData.Mode = "DRAW_MODE" And _
        g_AppData.ProInterface = False Then
        '作図モード、かつ、詳細入力モードでないとき、のみ有効なキー入力
        Dim i As Integer 'counter
        Select Case KeyCode
            Case &H58   'Xキーが押された
                If Shift = 0 Then
                    'シフトキーが押されてなかったら普通に動かす
                    g_GuriController.SetCommand ("POINTER_MOVE_X")
                ElseIf Shift = 1 Then
                    g_GuriController.SetCommand ("POINTER_MOVE_MX")
                ElseIf Shift = 2 Then
                    'コントロールキーが押されていたら、大きく動かす
                    For i = 1 To 10
                        g_GuriController.SetCommand ("POINTER_MOVE_X")
                    Next i
                ElseIf Shift = 3 Then
                    'Shift+Ctrlだったら、大きくマイナス方向へ動かす
                    For i = 1 To 10
                        g_GuriController.SetCommand ("POINTER_MOVE_MX")
                    Next i
                End If
            Case &H59   'Yキーが押された
                If Shift = 0 Then
                    'シフトキーが押されてなかったら普通に動かす
                    g_GuriController.SetCommand ("POINTER_MOVE_Y")
                ElseIf Shift = 1 Then
                    g_GuriController.SetCommand ("POINTER_MOVE_MY")
                ElseIf Shift = 2 Then
                    'コントロールキーが押されていたら、大きく動かす
                    For i = 1 To 10
                        g_GuriController.SetCommand ("POINTER_MOVE_Y")
                    Next i
                ElseIf Shift = 3 Then
                    'Shift+Ctrlだったら、大きくマイナス方向へ動かす
                    For i = 1 To 10
                        g_GuriController.SetCommand ("POINTER_MOVE_MY")
                    Next i
                End If
            Case &H5A   'Zキーが押された
                If Shift = 0 Then
                    'シフトキーが押されてなかったら普通に動かす
                    g_GuriController.SetCommand ("POINTER_MOVE_Z")
                ElseIf Shift = 1 Then
                    g_GuriController.SetCommand ("POINTER_MOVE_MZ")
                ElseIf Shift = 2 Then
                    'コントロールキーが押されていたら、大きく動かす
                    For i = 1 To 10
                        g_GuriController.SetCommand ("POINTER_MOVE_Z")
                    Next i
                ElseIf Shift = 3 Then
                    'Shift+Ctrlだったら、大きくマイナス方向へ動かす
                    For i = 1 To 10
                        g_GuriController.SetCommand ("POINTER_MOVE_MZ")
                    Next i
                End If
            Case &H4F   'Oキーが押された
                'Call ReturnOrigin
                g_GuriController.SetCommand ("RETURN_POINTER_TO_ORIGIN")
            Case &HD    'リターンキーが押された
                'Call PushRet(Shift)
                g_GuriController.SetCommand ("APPLY")
            Case &H8    'BSキーが押された
                'Call DelATail
                g_GuriController.SetCommand ("DEL_TAIL")
        End Select
    End If
    DoEvents
End Sub
'************************************************

