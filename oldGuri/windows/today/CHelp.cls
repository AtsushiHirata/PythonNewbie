VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CHelp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'==========================================================
'Name:      CHelp
'Author:    平田 敦(vff01137@nifty.ne.jp)
'Date:      2003/02/12
'Revision:


'------------------------------
'      ヘルプの呼び出し
'------------------------------
Private Declare Function WinHelp Lib "user32" Alias "WinHelpA" _
    (ByVal hWnd As Long, ByVal lphelpfile As String, _
    ByVal wCommand As Long, ByVal dwData As Long) As Long
Const HELP_INDEX = &H3   'メニューページの呼び出し
Const HELP_CONTEXT = &H1 '特定のページを呼び出す


'------------------------------------------------
Public Sub Show(ByVal hWnd As Long, ByVal filename As String)
'目的：ヘルプファイルを表示する
'引数：hWnd　ヘルプを表示するアプリケーションのウインドウハンドル
'　　　filename ヘルプファイルのファイルネーム。フルパスで。
'------------------------------------------------
    Dim ret As Integer
    App.HelpFile = filename 'この行がないと、目次が表示されない！
    ret = WinHelp(hWnd, filename, HELP_INDEX, 0)
End Sub



