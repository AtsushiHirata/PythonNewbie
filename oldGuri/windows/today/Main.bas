Attribute VB_Name = "Main_TheApp"
Option Explicit
'================================================
'Name:        Main_TheApp アプリケーションの
'                         エントリポイントを所有
'Author:      平田 敦(a_hirata@siscom.or.jp)
'CreateDate:  2003/01/25
'ChangeDate:
'Revision:
'================================================
'
'             立体グリグリforWindows
'
'              G-SOFT 技術教育ソフト
'        http://www.gijyutsu.com/g-soft/
'
'       (C)Jun Kawamata & Atsushi Hirata
'
Public Const VERSION_STRING = "G-Soft 立体グリグリ Ver.3.3α"
Public Const COPYRIGHT = "(C)Copyright 2003 by J.Kawamata A.Hirata"

'
'
'====================================================================
'================================================
' 変数、オブジェクトの宣言
'================================================

'------------------------------------------------
' Grobal Objects and Variables
'------------------------------------------------
Public g_GuriController As CGuriController
Public g_GuriProController As CGuriProController

Public g_frmGuriWin     As frmGuriWin

Public g_ctlGuriData    As GuriData
Public g_ctlGuriView    As ctlGuriView

Public g_AppData As CAppData

'------------------------------------------------
' Local Objects and Variables
'------------------------------------------------

'================================================
' Main Routine アプリケーションのエントリポイント
'================================================



Sub Main()
    Call InitInstances
    
    g_GuriController.SetCommand ("BOOT_GURI")
    
End Sub


'================================================
' Private Methods
'================================================

'------------------------------------------------
Private Sub InitInstances()
'目的：　グリグリで使うオブジェクトのインスタンスを生成し、
'        初期化までを行う。
'引数：　なし
'返値：　なし
'------------------------------------------------
    Set g_GuriController = New CGuriController
    Set g_GuriProController = New CGuriProController
    Set g_AppData = New CAppData
    Set g_frmGuriWin = New frmGuriWin
    Set g_ctlGuriData = g_frmGuriWin.ctlGuriData
    Set g_ctlGuriView = g_frmGuriWin.ctlGuriView
End Sub

