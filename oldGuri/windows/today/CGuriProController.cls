VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGuriProController"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'==========================================================
'Name:        GuriProController frmGuriWinのProインターフェ
'             イスからイベントを委譲される
'Author:      平田 敦(a_hirata@siscom.or.jp)
'CreateDate:  2003/02/20
'ChangeDate:
'Revision:


'------------------------------------------------
Public Function SetCommand(ByVal NewCommand As String) As Boolean
'目的：ProInterfaceにかかわる操作を中継する。Facade、Mediator的なメソッド。
'------------------------------------------------
    Select Case NewCommand
        Case "TURN_UP"
            g_GuriController.SetCommand (NewCommand)
            SetCommand = True
        Case "TURN_DOWN"
            g_GuriController.SetCommand (NewCommand)
            SetCommand = True
        Case "TURN_LEFT"
            g_GuriController.SetCommand (NewCommand)
            SetCommand = True
        Case "TURN_RIGHT"
            g_GuriController.SetCommand (NewCommand)
            SetCommand = True
        Case "APPLY_LINE"
            Call ApplyLine
            SetCommand = True
        Case "DELETE_LINE"
            Call DeleteLine
            SetCommand = True
        Case "NEW_LINE"
            Call NewLine
            SetCommand = True
        
        'これはプロインターフェイスに入った初回のみ
        Case "REFLESH_PRO_INTERFACE"
            Call SetCurrentLineToProInterface
            SetCommand = True
            
        'プロインターフェイスに変化があったときに呼ぶ
        Case "PRO_INTERFACE_CHANGE"
            Call ProInterfaceChange
            SetCommand = True
            
            
        Case "SETUP_VSCROLL_BAR"
            Call SetUpVscrollBar
            SetCommand = True
        Case "SETUP_LINE_NUMBER_LABEL"
            Call SetUpLineNumberLabel
            SetCommand = True
        Case "VSCR_LINE_NUMBER_CHANGE"
            Call VscrLineNumberChange
            SetCommand = True
            
            
        Case "SET_TEMP_LINE"
            Call SetTempLine
            SetCommand = True
            
            
        Case ""
            '不正なコマンド
            SetCommand = False
        Case Else
            '不正なコマンド
            SetCommand = False
    End Select

End Function

'------------------------------------------------
Private Sub ApplyLine()
'目的：プロインターフェイスに設定された線分をデータに加える
'------------------------------------------------
    Dim result As Boolean
    With g_frmGuriWin
        If g_AppData.ProIF_LineNumber = 0 Then g_AppData.ProIF_LineNumber = 1
        result = g_ctlGuriData.SetLineTo(g_AppData.ProIF_LineNumber, _
                .txtPos(0) + "," + .txtPos(2) + "," + .txtPos(4) + "," + _
                .txtPos(1) + "," + .txtPos(3) + "," + .txtPos(5))
        If result Then
            MsgBox "Apply Succeeded"
            'フォームの各要素に増減を反映
            Call SetUpVscrollBar
        Else
            MsgBox "Apply Failed"
        End If
            
    End With
    Call SetCurrentLineToProInterface
End Sub

'------------------------------------------------
Private Sub DeleteLine()
'目的：プロインターフェイスに表示されている線分をデータから取り除く
'------------------------------------------------
    Dim result As Long
    result = MsgBox(g_AppData.ProIF_LineNumber & " 番目の線を削除しようとしています。" & vbCrLf & _
                    "本当によろしいですか？", vbYesNo)
    If result = vbYes Then
        '削除します。
            If g_ctlGuriData.DelLine(g_AppData.ProIF_LineNumber) = True Then
                '正常に削除されました。
                MsgBox ("削除完了")
                If g_ctlGuriData.GetNumberOfLines >= g_AppData.ProIF_LineNumber Then
                    '問題なし
                Else
                    g_AppData.ProIF_LineNumber = g_ctlGuriData.GetNumberOfLines
        
                End If
                Call SetUpVscrollBar
                Call SetCurrentLineToProInterface
            Else
                '削除されませんでした。
                MsgBox ("エラー、または削除されませんでした。")
            End If
    Else
        'なにもしません。
    End If
End Sub

'------------------------------------------------
Private Sub NewLine()
'目的：プロインターフェイスに、新しい線分のための表示を行う。
'　　　表示中の線分が、末尾であれば、末尾のToの座標を新しい線分のFrom、Toとする。
'　　　表示中の線分が、ｉ番目であれば、ｉ番目の線分のToを新しい線分のFrom、Toとする。
'------------------------------------------------
    'フォームの各要素に増減を反映
    Call SetUpVscrollBar
    Call SetCurrentLineToProInterface
End Sub


'------------------------------------------------
Private Sub SetlblLineNumber()
'目的：線番号表示ラベルの更新
'------------------------------------------------
    g_frmGuriWin.lblLineNumber = "LineNo. " & g_AppData.ProIF_LineNumber & " / " & g_ctlGuriData.GetNumberOfLines
End Sub


'------------------------------------------------
Private Sub SetUpVscrollBar()
'目的：スクロールバーの設定。できれば、フォーム内におきたかったが、ほかのオブジェクトへの参照を持たせたくなかった。
'------------------------------------------------
    With g_frmGuriWin.vscrLineNumber
        .Max = g_ctlGuriData.GetNumberOfLines
        If g_ctlGuriData.GetNumberOfLines <> 0 Then
            .Min = 1
        Else
            .Min = 0
        End If
    End With
End Sub

'------------------------------------------------
Private Sub SetUpLineNumberLabel()
'目的：プロインターフェイス初期化時のLineNumberラベル初期化
'------------------------------------------------
    Call SetlblLineNumber
End Sub

'------------------------------------------------
Private Sub VscrLineNumberChange()
    g_AppData.ProIF_LineNumber = g_frmGuriWin.vscrLineNumber.Value
    Call SetCurrentLineToProInterface
End Sub


'------------------------------------------------
Private Sub SetCurrentLineToProInterface()
'目的：g_appdata.proif_linenumberで指している線分の座標を各テキストボックスに反映
'------------------------------------------------
    If g_ctlGuriData.GetNumberOfLines = 0 Then
        With g_frmGuriWin
            .txtPos(0) = 0
            .txtPos(2) = 0
            .txtPos(4) = 0
            .txtPos(1) = 0
            .txtPos(3) = 0
            .txtPos(5) = 0
        End With
        g_ctlGuriView.PointerFrom = "0,0,0"
        g_ctlGuriView.PointerTo = "0,0,0"
    Else
        Dim cstring As CStringUtil
        Set cstring = New CStringUtil
        Dim strPos() As String
        Dim lngResult As Long
        lngResult = cstring.StringSprit(g_ctlGuriData.GetLine(g_AppData.ProIF_LineNumber), ",", strPos)
        With g_frmGuriWin
            .txtPos(0) = strPos(0): g_AppData.PointerFromX = strPos(0)
            .txtPos(2) = strPos(1): g_AppData.PointerFromY = strPos(1)
            .txtPos(4) = strPos(2): g_AppData.PointerFromZ = strPos(2)
            .txtPos(1) = strPos(3): g_AppData.PointerToX = strPos(3)
            .txtPos(3) = strPos(4): g_AppData.PointerToY = strPos(4)
            .txtPos(5) = strPos(5): g_AppData.PointerToZ = strPos(5)
        End With
        g_ctlGuriView.PointerFrom = strPos(0) & "," & strPos(1) & "," & strPos(2)
        g_ctlGuriView.PointerTo = strPos(3) & "," & strPos(4) & "," & strPos(5)
    End If

    g_ctlGuriView.TempLine = g_ctlGuriView.PointerFrom & "," & _
                             g_ctlGuriView.PointerTo
    If g_AppData.Mode = "DRAW_MODE" Then
        g_ctlGuriView.ViewPointerFrom = True
        g_ctlGuriView.ViewPointerTo = True
        g_ctlGuriView.ViewTempLine True
    Else
        g_ctlGuriView.ViewPointerFrom = False
        g_ctlGuriView.ViewPointerTo = False
        g_ctlGuriView.ViewTempLine False
    End If
    g_GuriController.SetCommand ("REDRAW")
    Call SetlblLineNumber
End Sub



'------------------------------------------------
Private Sub SetTempLine()
'目的：詳細入力モードの数値をAppdataオブジェクトに通知する
'------------------------------------------------
    With g_frmGuriWin
        Call MoveWithCheck(g_AppData.PointerFromX, .txtPos(0))
        Call MoveWithCheck(g_AppData.PointerFromY, .txtPos(2))
        Call MoveWithCheck(g_AppData.PointerFromZ, .txtPos(4))
        Call MoveWithCheck(g_AppData.PointerToX, .txtPos(1))
        Call MoveWithCheck(g_AppData.PointerToY, .txtPos(3))
        Call MoveWithCheck(g_AppData.PointerToZ, .txtPos(5))
    End With
End Sub
'------------------------------------------------
Private Sub MoveWithCheck(ByVal Dest As Integer, ByVal Src As Variant)
'目的：SrcからDestへデータを移動する際、Srcが数値か、適正な整数かをチェックする。
'------------------------------------------------
    If IsNumeric(Src) Then
        'うん、確かに数値。
        If Src < 0 Then
            Dest = 0
        ElseIf Src > g_AppData.MaxSize Then
            Dest = g_AppData.MaxSize
        Else
            Dest = Src
        End If
    Else
        '数値でなければ、ゼロをセットしよう。
        Dest = 0
    End If
End Sub


Private Sub SetTempLineToProInterface()
    With g_frmGuriWin
        .txtPos(0) = g_AppData.PointerFromX
        .txtPos(2) = g_AppData.PointerFromY
        .txtPos(4) = g_AppData.PointerFromZ
        .txtPos(1) = g_AppData.PointerToX
        .txtPos(3) = g_AppData.PointerToY
        .txtPos(5) = g_AppData.PointerToZ
        g_ctlGuriView.PointerFrom = .txtPos(0) & "," & .txtPos(2) & "," & .txtPos(4)
        g_ctlGuriView.PointerTo = .txtPos(1) & "," & .txtPos(3) & "," & .txtPos(5)
        g_ctlGuriView.TempLine = g_ctlGuriView.PointerFrom + "," + _
                                 g_ctlGuriView.PointerTo
    End With

    g_ctlGuriView.ViewPointerFrom = True
    g_ctlGuriView.ViewPointerTo = True
    g_ctlGuriView.ViewTempLine True
    g_GuriController.SetCommand ("REDRAW")
End Sub

Private Sub ProInterfaceChange()
    Call SetTempLineToProInterface
End Sub



