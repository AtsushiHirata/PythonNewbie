# GuriPy (旧名Python Newbie)

Pythonに立体グリグリを移植するプロジェクト。(2017/09/20〜)

## DOS版立体グリグリキーボード操作一覧

![DOS版立体グリグリキーボード操作一覧](./figs/DosGuriKeyControls.jpg "DOS版立体グリグリ操作一覧")

## 開発の際の手順

* Python 3.6.2
* 開発環境 macOS high sierra
* pip list の結果は以下の通り

```
altgraph (0.14)
macholib (1.8)
modulegraph (0.15)
pep8 (1.7.1)
pip (9.0.1)
py2app (0.14)
pycodestyle (2.3.1)
setuptools (36.5.0)
wheel (0.30.0)
```

* virtualenvからvenvに変更(2018/07/18)。

* ユニットテスト。テストファーストで開発。追加したいクラスとそのメソッドのテストコードを記述。テストを実行して当然エラーになることを確認。そのエラーを解消するようなコードを追加していく。冗長な警告を表示させたい場合はunittests.sh、シンプルにテストしたい場合はut.shを実行する。


## 参考

[サンプルコード集](http://www.souzousha.iinaa.net/SourcePY.html)

[tkinterを使ったGUIプログラミングのまとめ(入門)記事](https://qiita.com/narupo/items/04e02093f08142fa8f1a)
