from tkinter import *
import time
import math
def array(N1,N2):
    return [[0 for i in range(N2)] for j in range(N1)]
Num=20     # 節点の数-1 
Myu=0.002  # 粘性抵抗
M=2        # 質量
Kx=100     # X方向硬さ(ばね定数)
Ky=0.005   # Y方向硬さ
DT=0.01    # 時間方向刻み幅
DX=1       # X方向刻み幅
X  = array(2, Num+1); Y  = array(2, Num+1) #　座標配列
VX = array(2, Num+1); VY = array(2, Num+1) #　速度配列
for i in range(Num+1):          # X座標値初期設定
    X[0][i]=i*DX
X[1][0]=0; X[1][Num]=DX*Num     # 先頭と最後は変更されない
InitialType = 2                 # Y座標値設定方法(この値を変えてみよう)
if InitialType==1:         
    DP = math.pi/Num            # sin波
    TH = 0
    for i in range(Num+1):
        Y[0][i] = 8 * math.sin(TH)
        TH += DP
elif InitialType==2:
    for i in range(Num+1):      # 三角(弦を引っ張って離す/弾く)
        if i<=int(Num/2): Y[0][i]=16*i/Num
        else: Y[0][i]=16-16*i/Num
else: VY[0][int(Num/2)]=200     # 中央の初速度(弦を叩く)
#■計算
def nextTime(i1):
    i2=1-i1
    T=0
    for i in range(1,Num):
        # X方向
        D = X[i1][i-1] - 2 * X[i1][i] + X[i1][i + 1]
        A = D * M/Kx - VX[i1][i]*Myu
        VX[i2][i] = VX[i1][i] + A * DT
        X[i2][i]  = X[i1][i] +  VX[i2][i] * DT
        # X方向
        D = Y[i1][i-1] - 2 * Y[i1][i] + Y[i1][i + 1]
        A = D * M/Ky-VY[i1][i]*Myu/DT
        VY[i2][i] = VY[i1][i] + A * DT
        Y[i2][i]  = Y[i1][i] + VY[i2][i] *DT
        T+= abs(VY[i2][i])+abs(Y[i2][i])
    if T<1: return 3 # 弦の停止を判定
    return i2
#■弦の描画
def drawLine(canvas,i1):
    canvas.delete("DT")
    DP = math.pi/Num            # sin波近似曲線
    TH = 0
    Y0=Y[i1][int(Num/2)]
    for i in range(Num):
        YY1 = 100-10*Y0 * math.sin(TH); YY2 = 100-10 * Y0 * math.sin(TH+DP)
        XX1 = 10 + i * DX * 10; XX2=XX1+DX*10
        canvas.create_line(XX1, YY1, XX2, YY2, fill="black", tag="DT", width=1)
        TH += DP
    for i in range(Num):        #弦の描画
        canvas.create_line(10+10 * X[i1][i]  , 100-10*Y[i1][i]  ,
                           10+10 * X[i1][i+1], 100-10*Y[i1][i+1], fill="blue", tag="DT", width=2) 
    for i in range(Num+1):
        XX = 10 + 10 * X[i1][i]
        YY =100 - 10 * Y[i1][i]
        canvas.create_oval(XX-2, YY-2, XX+2, YY+2, fill="red", tag="DT") 
#■メイン処理    
root=Tk()
root.title("弦の振動")
canvas=Canvas(root,width=220, height=240)
canvas.pack()
i1=0
while True:
    drawLine(canvas,i1)
    root.update_idletasks()
    root.update()
    i1=nextTime(i1)
    if i1==3: break
    time.sleep(0.001)

              
