import turtle
#■立ち枯れ木立をTurtleグラフィックスで描く
def blightTree(t,L,angTB,forTB):
    if L<2.0:
        t.forward(L)
    else:
        for j in range(0,4):
            t.left(angTB[j])
            blightTree(t,L*forTB[j],angTB,forTB)

#■メイン処理
t=turtle.Pen()
t.screen.title("Blighted Tree used Turtle graphics")
#タートル移動を速くする
t.speed(0)
#描く順序を見たいとき以下の第1引数を小さくする
t.screen.tracer(100000,0)
#タートルを見えなくする
t.hideturtle()
#描画開始位置に移動
t.up()
t.goto(-300,100)
t.down()
#線幅とカラーを設定
t.width(1)
t.color('#883300')
#立ち枯れ木立描画開始
angTB=[0.0, 88.0, -176.0, 88.0]
forTB=[0.28, 0.28, 0.28, 0.7]
blightTree(t,420,angTB,forTB)
t.screen.tracer(1,0)
t.screen.mainloop()

            
        
