#!/usr/bin/python
# -*- coding: utf-8 -*-

from tkinter import *

import TestApp

def keyF1(event):
    print("メインアプリ：F1 key 押されたよ")
    ta.keyF1Pressed()

root = Tk()
canvas=Canvas(root, width=100,height=100,bg="#CCCCCC")
canvas.pack()
canvas.focus_set()
canvas.bind("<F1>",keyF1)
ta = TestApp.TestApp(root)
root.mainloop()
