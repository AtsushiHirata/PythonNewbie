#!/usr/bin/python
# -*- coding: utf-8 -*-

from tkinter import *

class TestApp:

    def __init__(self,ma):
        self.mainapp = ma

    def keyF1Pressed(self):
        print("TestApp class:F1 key pressed")
        self.mainapp.destroy()
        sys.exit()
