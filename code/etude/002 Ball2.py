from tkinter import *
import random
import time

#■ボールのクラス定義
class Ball:
    def __init__(self,canvas, color):
        self.canvas=canvas
        self.width=15
        self.hight=15
        self.id=canvas.create_oval(10,10,25,25,fill=color)
        self.start(canvas)

    def start(self,canvas):
        self.canvas.coords(self.id,250,60,265,75)
        starts=[-3,-2,-1,1,2,3]
        random.shuffle(starts)
        self.a=0.1
        self.v=3
        self.x=starts[0]
        self.y=-5
        self.h=self.canvas.winfo_height()
        self.w=self.canvas.winfo_width()
       
    #■ボールを動かす　　　
    def move(self):
        self.canvas.move(self.id,self.x,self.y)
        p=self.canvas.coords(self.id)
        self.x*=0.9998
        self.y*=0.995
        if p[3]>=self.h and abs(self.y)<1.5 and abs(self.x)<1.0:
             self.y=0
        if p[3]>=self.h and abs(self.x)<1 and abs(self.y)<1.0:
            self.x=0
            stopflag=True
        #衝突係数=0.96
        self.y+=self.a
        if p[0]<=0:
            self.x=abs(self.x)*0.96
        if p[2]>=self.w:
            self.x=-abs(self.x)*0.96
        if p[1]<=0:
            self.y=abs(self.y)*0.96
        if p[3]>=self.h:
            self.y=-abs(self.y)*0.96

#■Tk初期処理
def initTk():
    tk=Tk()
    tk.title("Ball")
    tk.resizable(0,0)
    return tk

#■待ち処理等
def wait():
    tk.update_idletasks()
    tk.update()
    time.sleep(0.01)

tk=initTk()
canvas=Canvas(tk,width=500,height=400,highlightthickness=0)
canvas.pack()
tk.update()
ball=Ball(canvas,'red')
ball.stopFlag=False
while True:
    ball.move()
    if ball.stopFlag:
        break;
    wait()
    

