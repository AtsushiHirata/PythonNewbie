from tkinter import *
root=Tk()
root.title("自作messagebox")
root.option_add("*gont",("Times",10))

msg_Win=None; msg_OK=False
def msgBoxOK()    :
    global msg_Win, msg_OK
    msg_OK=True ; msg_Win.destroy()
def msgBoxCancel():
    global msg_Win, msg_OK
    msg_OK=False; msg_Win.destroy()
def msgBox(Title, msg):
    global msg_Win
    if msg_Win is None or not msg_Win.winfo_exists():
        msg_Win=Toplevel()
        msg_Win.title(Title)
        msg_Win.geometry("300x100")
        Message(msg_Win,aspect=500, text=msg).pack()
        Button(msg_Win,text="Cancel", command=msgBoxCancel).pack(side='right')
        Button(msg_Win,text="OK"    , command=msgBoxOK    ).pack(side='right')
        msg_Win.protocol("WM_DELETE_WINDOW",msgBoxCancel)
def mnu_msgBox():
    strOK="False"
    if msg_OK: strOK="True"
    msgBox("msgBox","メッセージボックスのテスト\n　前回OK=ボタン"+strOK)
m=Menu(root)
root.configure(menu=m)
m.add_command(labe="表示", under=0, command=mnu_msgBox)
Label(root,text="「表示」メニューを選択してください").pack()
root.mainloop()
