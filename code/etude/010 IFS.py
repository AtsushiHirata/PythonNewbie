﻿from tkinter import *
import random

#■反復関数系
#  表示用パラメータ D≡[X0, Y0, SX, SY]
Title="羊歯の葉"
D=[200,350, 300,-300]
Color="#005500"
A=[
    [[ 0.836,  0.044   , 0.0  ],
     [-0.044,  0.836   , 0.169]],
    [[-0.141,  0.302   , 0.0  ],
     [ 0.302,  0.141   , 0.127]],
    [[ 0.141, -0.302   , 0.0  ],
     [ 0.302,  0.141   , 0.169]],
    [[ 0.0  ,  0.0     , 0.0  ],
     [ 0.0  ,  0.175337, 0.0  ]]]
P=[0.85, 0.92, 0.99, 1.0]

#Title="シェルピンスキのギャスケット"
#D=[50,200, 2.5,-2.5]
#Color="blue"
#A=[[[0.5,0,0],[0,0.5,0]],[[0.5,0,60],[0,0.5,0]],[[0.5,0,30],[0,0.5,30]]]
#P=[1/3, 2/3,1]

#■反復関数の大きさと繰返し回数（点の数に一致）
N=len(P)
MAX=20000
#■表示用計算
def IFS():
    #■表示用計算
    XN=0
    YN=0
    for i in range(MAX):
        X  = XN
        Y  = YN
        #乱数による配列の選択
        R  = random.random()
        ID = 5
        for k in range(0,N):
            if R<P[k]:
                ID=k
                break
        #行列演算
        XN = A[ID][0][0]*X + A[ID][0][1]*Y + A[ID][0][2]
        YN = A[ID][1][0]*X + A[ID][1][1]*Y + A[ID][1][2]

        #表示位置に点を移動
        XX = D[0] + D[2] * XN
        YY = D[1] + D[3] * YN
        canvas.coords(FID[i],XX,YY,XX,YY)
#■メイン処理
#キャンバスの定義
tk=Tk()
tk.title(Title)
canvas=Canvas(tk, width=400,height=400)
canvas.pack()

#表示用の点の準備
FID=[0 for j in range(MAX)]
for j in range(MAX):
    FID[j]=canvas.create_rectangle(0,0,0,0,fill=Color, outline='')

#表示用計算
IFS()

tk.update()
tk.mainloop()    




    
