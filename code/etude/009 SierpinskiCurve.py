import turtle
#■C曲線をTurtleグラフィックスで描く
#相対座標位置への移動
def draw(t,X,Y):
    P=t.pos()
    NX=P[0]+X
    NY=P[1]+Y
    t.goto(NX,NY)
def setOrder(order):
        H = 6
        S = 2000
        S = S / H
        for i in range(2, order+1):
            H=H*2+2
        return S/H
#ヒルベルト曲線を描く
def SierpinskiCurve(t,order):
    H=setOrder(order)
    ID=order
    URD(t, ID, H)
    draw(t, H,  H)
    LUR(t, ID, H)
    draw(t,-H,  H)
    DLU(t, ID, H)
    draw(t,-H, -H)
    RDL(t, ID, H)
    draw(t, H, -H)

#右→下→左
def RDL(t,ID,H):
    if ID>0:
        ID=ID-1
        RDL(t, ID, H)
        draw(t, H, -H)
        URD(t, ID, H)
        draw(t, 0, -2*H)
        DLU(t, ID, H)
        draw(t,-H, -H)
        RDL(t, ID, H)
        ID=ID+1
#下→左→上        
def DLU(t,ID,H):
    if ID>0:
        ID=ID-1
        DLU(t, ID, H)
        draw(t,-H, -H)
        RDL(t, ID, H)
        draw(t,-2*H, 0)
        LUR(t, ID, H)
        draw(t,-H,  H)
        DLU(t, ID, H)
        ID=ID+1
#左→上→右
def LUR(t,ID,H):
    if ID>0:
        ID=ID-1
        LUR(t, ID, H)
        draw(t,-H, H)
        DLU(t, ID, H)
        draw(t,0, 2*H)
        URD(t, ID, H)
        draw(t, H, H)
        LUR(t, ID, H)
        ID=ID+1
#上→右→下
def URD(t,ID,H):
    if ID>0:
        ID=ID-1
        URD(t, ID, H)
        draw(t,H, H)
        LUR(t, ID, H)
        draw(t,2*H, 0)
        RDL(t, ID, H)
        draw(t,H, -H)
        URD(t, ID, H)
        ID=ID+1
#■メイン処理
t=turtle.Pen()
t.screen.title("Sierpinski Curve used Turtle graphics")
#タートル移動を速くする
t.speed(0)
#描く順序を見たいとき以下の第1引数を小さくする
t.screen.tracer(100000,0)
#タートルを見えなくする
t.hideturtle()
#描画開始位置に移動
t.up()
t.goto(-300,-100)
t.down()
#線幅とカラーを設定
t.width(2)
t.color('blue')
#シェルピンスキ曲線描画開始
SierpinskiCurve(t,5)
t.screen.tracer(1,0)
t.screen.mainloop()
            
        
