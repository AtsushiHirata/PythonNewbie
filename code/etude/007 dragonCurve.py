import turtle
#■C曲線をTurtleグラフィックスで描く
#相対座標位置への移動
def C_draw(t,X,Y):
    P=t.pos()
    NX=P[0]+X
    NY=P[1]+Y
    t.goto(NX,NY)
#ドラゴン曲線を描く
def dragonCurve(t,X,Y,S, ML):
    if X*X+Y*Y<ML*ML:
        C_draw(t,X,Y)
    else:
        dragonCurve(t,(X-S*Y)/2, (Y+S*X)/2, 1, ML)
        dragonCurve(t,(X+S*Y)/2, (Y-S*X)/2,-1, ML)
      
#■メイン処理
t=turtle.Pen()
t.screen.title("Dragon Curve used Turtle graphics")
#タートル移動を速くする
t.speed(0)
#描く順序を見たいとき以下の第1引数を小さくする
t.screen.tracer(100000,0)
#タートルを見えなくする
t.hideturtle()
#描画開始位置に移動
t.up()
t.goto(-200,100)
t.down()
#線幅とカラーを設定
t.width(1)
t.color('blue')
#ドラゴン曲線描画開始
dragonCurve(t,200,0,1, 2)
t.screen.tracer(1,0)
t.screen.mainloop()
            
        
