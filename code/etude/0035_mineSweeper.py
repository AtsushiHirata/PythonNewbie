# coding: utf-8
from tkinter import *
from tkinter import messagebox
import sys
import random
import copy
def array(N1,N2):
    return [[0 for i in range(N2)] for j in range(N1)]
AX =[0, 0, False]
Size=[20,20,20]
numX=20
numY=20
Flag=array(22,22)
Hide=array(22,22)
DT=array(22,22)
root= Tk()
root.title("Minesweeper")
canvas=Canvas(root,width=440,height=460)
canvas.pack()
def drawFlag(X, Y, SZ): #旗を表示する。
    SX=SZ/3; SY=SZ/4; XX = SZ * X + SX; YY = SZ * Y + SY
    canvas.create_rectangle(XX, YY, XX+SX, YY+SY, fill="red", outline="black",tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX,YY,XX,YY+SY*2, fill="black", width=2, tag="hide%d,%d" %(X,Y) )

def drawHide(X, Y, SZ): #隠された状態の表示
    XX = X * SZ; YY = Y * SZ; S9 = SZ - 1; S8 = SZ - 2
    canvas.create_rectangle(XX, YY, XX+SZ, YY+SZ, fill="#CCCCCC", outline="",tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX     , YY     , XX + SZ, YY     , fill="#FFFFFF", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX     , YY     , XX     , YY + SZ, fill="#FFFFFF", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX     , YY + SZ, XX + SZ, YY + SZ, fill="#000000", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX + SZ, YY     , XX + SZ, YY + SZ, fill="#000000", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX     , YY + S9, XX + S9, YY + S9, fill="#777777", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX + S9, YY     , XX + S9, YY + S9, fill="#777777", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX +  1, YY + S8, XX + S8, YY + S8, fill="#777777", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX + S8, YY +  1, XX + S8, YY + S8, fill="#777777", tag="cell%d,%d" %(X,Y) )

    if(Flag[X][Y]): drawFlag(X,Y, SZ) #旗表示

def drawFlat(Col, X, Y, SZ): #フラット塗り潰し
    XX = X * SZ; YY = Y * SZ
    canvas.create_rectangle(XX, YY, XX+SZ, YY+SZ, fill=Col, outline="#777777",tag="cell%d,%d" %(X,Y))
def drawNum(V, X, Y, SZ): #爆弾個数の表示
    XX = X * SZ; YY = Y * SZ; SS =SZ/2
    drawFlat("#66FF66", X, Y, SZ);
    canvas.create_text(XX + SS, YY + SS, text="%d" %V, fill="black", font=('Times', 15), tag="cell%d,%d" %(X,Y) )
def drawSpace( X, Y, SZ):#空白の表示
    XX = X * SZ; YY = Y * SZ;S9=SZ-1; S8=SZ-2
    canvas.create_rectangle(XX, YY, XX+SZ, YY+SZ, fill="#FFFF77", outline="black",tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX +  1, YY +  1, XX + S9, YY +  1, fill="#777777", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX +  2, YY +  2, XX + S8, YY +  2, fill="#777777", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX +  1, YY +  1, XX +  1, YY + S9, fill="#444444", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX +  2, YY +  2, XX +  2, YY + S8, fill="#444444", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX +  1, YY + S9, XX + S9, YY + S9, fill="#FFFFFF", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX +  2, YY + S8, XX + S8, YY + S8, fill="#FFFFFF", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX + S9, YY +  1, XX + S9, YY + S9, fill="#FFFFFF", tag="cell%d,%d" %(X,Y) )
    canvas.create_line     (XX + S8, YY +  2, XX + S8, YY + S8, fill="#FFFFFF", tag="cell%d,%d" %(X,Y) )

def drawBomb(X, Y, SZ, AX, AY):# 爆弾の表示
    if (AX == X) and (AY == Y): drawFlat("#FF0000", X, Y, SZ);
    else: drawFlat("#FF8844", X, Y, SZ);
    XX = X * SZ; YY = Y * SZ; S1 = SZ / 4; S2 = S1 * 3; S3=SZ/2
    
    canvas.create_oval(XX+S1, YY+S1, XX+S2  , YY+S2, fill="#000000", outline="",tag="cell%d,%d" %(X,Y) )
    canvas.create_line(XX+S3, YY+S3, XX+S3+5, YY+3 , fill="#000000", tag="cell%d,%d" %(X,Y) )
 
Hiden0=-50
Bomb=-100
def Initialize(numX, numY, numB):#初期化
    AX[0] = 0; AX[2] = True;
    for X  in range(numX + 2): #初期値は隠された0とする
        for Y in range(numY + 2): DT[X][Y] = Hiden0
    for i in range(numB): setBomb(numX, numY)   #爆弾の生成
    for X in range(1,numX+1): #  周囲の爆弾の個数カウント
        for Y in range(1, numY+1):
            if (DT[X][Y] != Bomb): countSetNum(X, Y) 
    for X in range(numX + 2) :#初期値は旗なしとする
        for Y in range(1, numY + 2): Flag[X][Y] = 0;

def setBomb(numX, numY):#爆弾の生成
    X = int(random.random()*numX) + 1; Y = int(random.random()*numY) + 1
    while (DT[X][Y] == Bomb):
        X = int(random.random()*numX) + 1; Y = int(random.random()*numY) + 1
    DT[X][Y] = Bomb
def countB(count, X, Y):#爆弾の個数カウント
    if DT[X][Y] == Bomb: return count + 1
    else: return count;
def countSetNum(X, Y):#爆弾の数をカウントしてセットする。
    count = 0
    for i in range(X - 1, X + 2):
        for j in range(Y - 1, Y+2):
            if i != X or j != Y: count = countB(count, i, j);
    if count > 0 :DT[X][Y] = -count
def endDisplay(numX,numY):# 終了時の表示
    for X in range(1,numX+1):
        for Y in range(1,numY+1):
            if DT[X][Y] == Bomb: drawBomb(X, Y, Size[2], AX[0], AX[1])
            elif DT[X][Y] == Hiden0 or DT[X][Y] == 0: drawSpace(X, Y,Size[2])
            else : drawNum(abs(DT[X][Y]), X, Y,Size[2]);

def display(numX, numY,SZ):#ゲーム時の表示
    for X in range(1,numX+1):
        for Y in range(1,numY+1):
            canvas.delete("cell%d,%d" %(X,Y) )

            if DT[X][Y] < 0: drawHide(X, Y, SZ)
            elif DT[X][Y] > 0: drawNum(DT[X][Y], X, Y,SZ)
            else: drawSpace(X, Y,SZ)
def openCheck(sw, X, Y): #隠された0を開かれた0にする(開かれた0にしたらTrue)
    if DT[X][Y] == Hiden0: DT[X][Y] = 0; return True
    elif DT[X][Y] != Bomb and DT[X][Y]<0: DT[X][Y] = -DT[X][Y]
    return sw
def UDLR(sw, X, Y): #上下左右を検査
    if (DT[X][Y] == 0) :
        return openCheck(openCheck(openCheck(openCheck
                                             (sw, X, Y + 1), X, Y - 1), X - 1, Y), X + 1, Y)
    else: return sw
def sweep(): #掃き出し
    sw = True
    while sw:#オープンする残りがある間は繰り返す
        sw = False
        for X in range(1,numX+1):#Xプラス,Yのプラス方向に掃き出し
            for Y in range(1,numY+1):sw = UDLR(sw, X, Y)
        Y=numY
        for i in range(1,numY+1):#行/列逆転してXプラス，Yのマイナス方向に掃き出し
            for X in range(1,numX+1):
                sw = UDLR(sw, X, Y)
            Y-=1
        X=numX
        for i in range(1,numX+1):#Xマイナス，Yのプラス方向に掃き出し
            for Y in range(1,numY+1):sw = UDLR(sw, X, Y)
            X-=1
        for Y in range(1,numY+1):#行/列逆転してXマイナス，Yのプラス方向に掃き出し
            X=numX
            for X in range(1,numX+1):
                sw = UDLR(sw, X, Y)
                X-=1
        for X in range(1,numX+1):#オープンする残りがあるかをチェック
            for Y in range(1,numY+1):
                if (DT[X][Y] == 0):
                    sw=openCheck(openCheck(openCheck(openCheck
                                                     (sw, X-1, Y - 1), X -1, Y + 1), X + 1, Y-1), X + 1, Y+1)
def continueCheck(): #終了のチェック
    for X in range(1,numX+1):
        for Y in range(1,numY+1):
            if (DT[X][Y] < 0) and (DT[X][Y] != Bomb): return True;
    return False;

def cellSelect(X, Y): #セルが選択されたときの処理
    if X < 1 or X > numX or Y < 1 or Y >numY: return
    if DT[X][Y] == 0: return
    if DT[X][Y] == Bomb:# 爆弾位置が選択されたとき
        AX[0]=copy.copy(X)
        AX[1]=copy.copy(Y)
        AX[2]=False 
    else: # 爆弾位置でないセルが選択されたとき
        if DT[X][Y] == Hiden0:
            DT[X][Y] = 0; sweep()
        elif DT[X][Y] <0: DT[X][Y] = -DT[X][Y]
        AX[2] = continueCheck();
#■左ボタンイベント(開く場所の指定)
def leftMouseDown(event):
    X=event.x; Y=event.y
    if AX[2]: cellSelect(int(X / Size[2]), int( Y /Size[2]))
    else: Initialize(Size[0], Size[1], 50); #実行中でなければ開始
    display(Size[0], Size[1], Size[2])
    root.update()
    if(AX[0]!=0):
        endDisplay(Size[0], Size[1])
        messagebox.showinfo("Mine Sweeper","残念！そこは爆弾です")
    elif AX[2]:display(Size[0], Size[1],Size[2])
    else :
        endDisplay(Size[0], Size[1])
        messagebox.showinfo("Mine Sweeper","終了です")
      
#■右ボタンイベント（旗表示フラグのON/OFF）
def rightMouseDown(event):#ダウン
    if not AX[2]: return
    X=event.x; Y=event.y
    i=int(X / Size[2]); j=int( Y /Size[2])
    if i>=1 and i<=Size[0] and j>=1 and j<=Size[1]:
        Flag[i][j]=not Flag[i][j]
        display(Size[0], Size[1],Size[2])

#■ウィンドウが閉じられるときの処理
def exitProc():
    if messagebox.askokcancel("終了","終了してもよろしいですか?"):
        root.destroy();  sys.exit()

Initialize(Size[0],Size[1],50)
display(Size[0],Size[1], Size[2])

canvas.bind("<Button-1>",leftMouseDown)
canvas.bind("<Button-3>",rightMouseDown)
canvas.create_text(220, 440, text="左ボタンでセルの指定/終了後の再開、右ボタンで旗表示の切り替え", fill="blue", font=('HGSｺﾞｼｯｸE', 10))

root.update()
root.protocol("WM_DELETE_WINDOW",exitProc) #ウィンドウが閉じられたときexitProcを呼び出す。

root.mainloop()
