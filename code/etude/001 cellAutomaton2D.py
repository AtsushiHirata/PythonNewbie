from tkinter import *
import time

#■配列の宣言
def array2(N1,N2):
    return [[0 for j in range(N2)]for k in range(N1)]
def array3(N1,N2,N3):
    return [array2(N2,N3)for k in range(N1)]

#■セルの初期設定
def initdt(N):
    A=array3(2,31,31)
    A[0][15][15]=N-1
    return A

#■セルオートマトンの実行
def execCell(A, i1, i2, Mod):
    for i in range(1, 30):
        for j in range(1,30):
            T=(A[i1][i][j-1]+A[i1][i][j+1]+A[i1][i-1][j]+A[i1][i+1][j])
            A[i2][i][j]=T % Mod

#■セルの位置決めとＩＤの設定
def drawCell(canvas,A, k):
    x2=0
    CID=array2(31,31)
    for i in range(1,30):
        x1=x2
        x2=x1+10
        y2=0
        for j in range(1,30):
            y1=y2
            y2=y1+10
            CID[i][j]=canvas.create_rectangle(x1,y1,x2,y2,fill='black')
    return CID

#■セルの色設定
def modifyCell(canvas,A, k,CID):
    CL=['black','#FFFF00','#ff0000','#00ff00','#0000FF']
    for i in range(1,30):
        for j in range(1,30):
            canvas.itemconfig(CID[i][j], fill=CL[A[k][i][j]])
            canvas.itemconfig(CID[i][j], outline=CL[A[k][i][j]])

#■Tk初期設定
def initTk():
    tk=Tk()
    tk.title("2D Cell Automaton")
    tk.resizable(0,0)
    return tk

#■待ち処理等
def wait():
    tk.update_idletasks()
    tk.update()
    time.sleep(0.2)

#■実行メイン
A=initdt(5)
tk=initTk()
canvas=Canvas(tk,width=290,height=290,highlightthickness=0)
canvas.pack()
tk.update()
CID=drawCell(canvas,A,0)
k=0
while 1:
    # 一回ごとの処理
    modifyCell(canvas,A,k,CID)
    execCell(A,k,1-k,5)
    wait()
    k=1-k
