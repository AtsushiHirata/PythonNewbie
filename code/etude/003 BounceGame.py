from tkinter import *
import random
import time

#■ボールのクラス定義
class Ball:
    def __init__(self,canvas, paddle,color):
        self.canvas=canvas
        self.paddle=paddle
        self.id=canvas.create_oval(10,10,25,25,fill=color)
        self.canvas.move(self.id,245,100)
        self.start(canvas)
        self.point=0
        self.poin_ID=canvas.create_text(10,10,text=str(self.point))
    #■開始処理        
    def start(self,canvas):
        starts=[-3,-2,-1,1,2,3]
        random.shuffle(starts)
        self.x=starts[0]
        self.y=-3
        self.h=self.canvas.winfo_height()
        self.w=self.canvas.winfo_width()
        self.Failure=False

    #■ヒットの判定
    #
    #      P[0]  P[2]
    #    P[1]＿||
    #        　●        ／
    #    P[3]￣　＼ 　 ／      
    #            　＼／　　　　  ＿pdP[1]
    #            ■■■■■■■■
    #            |              |￣pdP[3]
    #          pdP[0]         pdP[2]
    #
    def hit_paddle(self, p):
        pdP=self.canvas.coords(self.paddle.id)
        return p[2]>=pdP[0] and p[0]<=pdP[2] and p[3]>=pdP[1] and p[3]<=pdP[3]

    #■ボールを動かす　　　
    def move(self):
        self.canvas.move(self.id,self.x,self.y)
        p=self.canvas.coords(self.id)
        #天上に当たったら反転
        if p[1]<=0:
            self.y=abs(self.y)
        #床に届いたら失敗
        if p[3]>=self.h:
            self.Failure=True
        #パドルで受け止めたら1点加算
        if self.hit_paddle(p):
            self.point+=1
            canvas.itemconfig(self.poin_ID, text=str(self.point))
            self.y=-3
        #左右壁に当たったら反転
        if p[0]<=0:
            self.x=abs(self.x)
        if p[2]>=self.w:
            self.x=-abs(self.x)
#■パドルのクラス定義      
class Paddle:
    def __init__(self,canvas, color):
        self.canvas=canvas
        self.id=canvas.create_rectangle(0,0,100,10,fill=color)
        self.canvas.move(self.id,200,300)
        self.x=2
        self.w=self.canvas.winfo_width()
        self.canvas.bind_all('<KeyPress-Left>', self.turn_left)
        self.canvas.bind_all('<KeyPress-Right>', self.turn_right)
    #■パドルを動かす　　　        
    def move(self):
        self.canvas.move(self.id, self.x,0)
        p=self.canvas.coords(self.id)
        if p[0]<=0:
            self.x=2
        elif p[2]>=self.w:
            self.x=-2
    #■カーソルキー入力イベント
    def turn_left(self, evt):
        self.x=-2
    def turn_right(self, evt):
        self.x=2

#■Tk初期処理
def initTk():
    tk=Tk()
    tk.title("Ball")
    tk.resizable(0,0)
    #tk.wm_attributes("_topmost",1)
    return tk

#■待ち処理等
def wait():
    tk.update_idletasks()
    tk.update()
    time.sleep(0.01)

#■メイン処理
tk=initTk()
canvas=Canvas(tk,width=500,height=400,highlightthickness=0)
canvas.pack()
tk.update()
paddle=Paddle(canvas,'green')
ball=Ball(canvas,paddle,'red')
while True:
    while not ball.Failure:
        ball.move()
        paddle.move()
        wait()
    ball.start(canvas)
    ball.canvas.move(ball.id,0,-ball.h)

    
