from tkinter import *


def callback():
    print("click! {0}".format(var.get()))
    
root = Tk()
root.title('Python Tk Examples @ pythonspot.com')

var = StringVar()
var.set("初期データ")
textbox = Entry(root, textvariable=var)
textbox.focus_set()
textbox.pack(pady=10, padx=10)
b = Button(root, text="OK", command=callback)
b.pack()





root.mainloop()
