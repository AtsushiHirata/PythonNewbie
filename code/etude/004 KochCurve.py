import turtle
#■コッホ曲線をTurtleグラフィックスで描く
def KochCurve(t,N,L):
    if N<=0:
        t.forward(L)
    else:
        NN=N-1
        LL=L/3
        KochCurve(t,NN,LL)
        t.left(60)
        KochCurve(t,NN,LL)
        t.right(120)
        KochCurve(t,NN,LL)
        t.left(60)
        KochCurve(t,NN,LL)

#■メイン処理
t=turtle.Pen()
t.screen.title("Koch Curve used Turtle graphics")
#タートル移動を速くする
t.speed(0)
#描く順序を見たいとき以下の第1引数を小さくする
t.screen.tracer(100000,0)
#タートルを見えなくする
t.hideturtle()
#描画開始位置に移動
t.up()
t.goto(-300,100)
t.down()
#線幅とカラーを設定
t.width(2)
t.color('blue')
#コッホ曲線描画開始
KochCurve(t,5,420)
t.screen.tracer(1,0)
t.screen.mainloop()

            
        
