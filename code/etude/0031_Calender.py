from tkinter import *
from tkinter import messagebox
import time
#■曜日の計算(ツェラー(Zeller)の公式)
def DayOfWeek(Y,M,D):
    MM=M
    YY=Y
    if M<3:   # 1月, 2月は前年の13月，14月とみなして計算
        YY-=1
        MM+=12
    return (YY+int(YY/4)-int(YY/100)+int(YY/400)+int((13*MM+8)/5)+D) % 7

#■うるう年の判定
#  4で割れる年が閏年だが，100で割り切れて400で割れない年は平年
def leapYear(y):
    return not((y % 400) and not(y % 100) or (y % 4))
#  本来は以下のように記述すべきだが、論理式の計算処理上は上記でよい
#    return not((y % 400 !=0) and not(y % 100 !=0) or (y % 4 !=0))

#■ひと月の日数
#  2月は閏年のとき1加算、7月以前は奇数月で30+1、8月以降は奇数月で31-1
def Mdays(Y, M):
    if M==2 : return 28 + leapYear(Y)
    if M<8  : return 30 + (M % 2)
    else    : return 31 - (M % 2)

#■日曜日を赤、土曜部を青、その他を黒にする
def colorSet(i):
    if i==0  : return 'red'
    elif i==6: return 'blue'
    else     : return 'black'
    
#■カレンダー表示
def dspColender():
    Y=int(txtY.get("1.0","1.4")) # なぜ，IDを文字列で指定する仕様になっているのだろう？
    M=int(txtM.get("1.0","1.2")) # しかも行は1から，桁位置は0が開始
    root.title("カレンダー %4d年 %2d月" % (Y, M))
    ist=DayOfWeek(Y,M,1)   # Y年M月1日の曜日
    Days=Mdays(Y,M)        # その月の日数
    canvas.delete('date')  # 前表示の日付削除(タグ='date')
    k=1                    # k : 日付
    #カレンダーの1行目表示(Y年M月1日の曜日までは表示なし)
    for i in range(ist,7):
        canvas.create_text(10+i*40, 40,text="%2d" % k,
                           font=('Helvetica',15,'bold'),
                           tag='date',fill=colorSet(i))
        k+=1               # 日付カウント
    #カレンダーの2行目以降を表示
    for j in range(1,6):
        for i in range(7):
            if k>Days: return  # その月の日数より日付が大きくなったら完了
            canvas.create_text(10+i*40, 40+35*j,text="%2d" % k,
                               font=('Helvetica',15,'bold'),
                               tag='date', fill=colorSet(i))
            k+=1           # 日付カウント

#■ウィンドウが閉じられるときの処理
def exitProc():
    if messagebox.askokcancel("終了","終了してもよろしいですか?"):
        root.destroy()
        sys.exit()
#■メイン処理
root =Tk()
root.geometry("300x300")
root.title("カレンダー")
#root.option_add('*font',('FixedSys',14, 'bold'))
canvas=Canvas(root,width=300,height=300)
canvas.place(x=10,y=50)
t=time.localtime() # 現在の日付取り出し
#テキスト・コントロール
txtY=Text(root, width=6,height=1)
txtM=Text(root, width=3,height=1)
# 現在の年と月を初期値とする 
txtY.insert(INSERT,str(t[0])) 
txtM.insert(INSERT,str(t[1]))
#ラベル・コントロール
lblY = Label(root, text='年', fg='black')
lblM = Label(root, text='月', fg='black')
#ボタン・コントロール
btn1 = Button(root, text='表示', height=1, command=dspColender)
btn2 = Button(root, text='終了', height=1, command=exitProc)
#コントロールの配置
txtY.place(x =  10, y = 8)
lblY.place(x =  50, y = 5)       
txtM.place(x =  70, y = 8)
lblM.place(x = 100, y = 5)       
btn1.place(x = 150, y =  0)
btn2.place(x = 250, y =  0)
#区切り表示
canvas.create_line(2,20,270,20,fill='blue')
#曜日の表示
headstr=[ "Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
for i in range(7):
    lbl=Label(root,text=headstr[i],fg=colorSet(i))
    xx=10+i*40
    lbl.place(x=xx,y=40)
#メイン・ループ 
root.protocol("WM_DELETE_WINDOW",exitProc) #ウィンドウが閉じられたときexitProcを呼び出す。
dspColender()
root.mainloop()

        
        


