# coding: utf-8
#
#■テトリス
#
#    キーボードのZで左移動、Xで右移動、
#    マウスの左ボタンで時計と反対方向へ回転
#    右ボタンクリックで時計と同じ向きに回転
#
from tkinter import *
from tkinter import messagebox
import winsound
import sys
import time
import random
import copy
def array(N1,N2):
    return [[0 for i in range(N2)] for j in range(N1)]

# (テトリミノの形状)
# +----------+------------------------------------+
# |          |      rot （□が基準点 ix,iy)       |
# |   bTP    +--------+--------+--------+---------+
# |          |   0    |   1    |    2   |   3     |
# +----------+--------+--------+--------+---------+
# |          |  ■    |     ■ |  ■■  |         |
# |    1     |  □    | ■□■ |    □　| ■□■  |
# |          |  ■■  |        |    ■  | ■      |
# +----------+--------+--------+--------+---------+
# |          |    ■  |        |  ■■  | ■      |
# |    2     |    □  | ■□■ |  □  　| ■□■  |
# |          |  ■■  |     ■ |  ■    |         |
# +----------+--------+--------+--------+---------+
# |          |        |   ■   |  ■    |   ■    |
# |    3     | ■□■ |   □■ |■□■　| ■□    |
# |          |   ■   |   ■   |        |   ■    |
# +----------+--------+--------+--------+---------+
# |          |        |     ■ |  ■■      
# |    4     | ■□   |   □■ |　　□■
# |          |   ■■ |   ■   | は結果的にrot=0と同じ
# +----------+--------+--------+
# |          |   ■■ | ■     |    □■    
# |    5     | ■□   | ■□   |　■■
# |          |        |   ■   | は結果的にrot=0と同じ
# +----------+--------+--------+
# |          |        |   ■   |        
# |          |        |   ■   |
# |    6     |■□■■|   □   | 
# |          |        |   ■   | 
# +----------+--------+--------+
# |          |  □■  |           
# |    7     |　■■  |    
# |          |        |    
# +----------+--------+
#テトリミノ形状テーブル(上記を表にしたもの)        # bTP  Rot
Tetrimino=(((-1,  0,  0,  0,  1,  0,  1,  1),      #  1    0 
            ( 0, -1,  0,  0,  0,  1, -1,  1),      #       1 
            ( 1,  0,  0,  0, -1,  0, -1, -1),      #       2 
            ( 1, -1,  0, -1,  0,  0,  0,  1)),     #       3 
           ((-1,  0,  0,  0,  1,  0,  1, -1),      #  2    0 
            ( 0, -1,  0,  0,  0,  1,  1,  1),      #       1 
            ( 1,  0,  0,  0, -1,  0, -1,  1),      #       2 
            (-1, -1,  0, -1,  0,  0,  0,  1)),     #       3 
           (( 0, -1,  0,  0,  0,  1,  1,  0),      #  3    0 
            (-1,  0,  0,  0,  1,  0,  0,  1),      #       1 
            (-1,  0,  0,  0,  0, -1,  0,  1),      #       2 
            (-1,  0,  0,  0,  0, -1,  1,  0)),     #       3 
           (( 0, -1,  0,  0,  1,  0,  1,  1),      #  4    0 
            ( 0,  1,  0,  0, -1,  1,  1,  0)),     #       1 
           (( 0, -1,  0,  0, -1,  0, -1,  1),      #  5    0
            (-1, -1,  0, -1,  0,  0,  1,  0)),     #       1
           (( 0, -1,  0,  0,  0,  1,  0,  2),      #  6    0
            ( 1,  0,  0,  0, -1,  0, -2,  0)),     #       1
           (( 0,  0,  0,  1,  1,  0,  1,  1),      #  7    0
            ( 0,  0,  0,  1,  1,  0,  1,  1)))     # 安全のための余裕
#■テトリミノの色
ClDt=('white','blue','green','brown','cyan','yellow','purple','red')
#■初期設定
Pos=[0,0,0,0,0] 
DT= array(20,10);
root= Tk()
root.title("Tetris")
canvas=Canvas(root,width=240,height=420)
canvas.pack()
#■タイル表示
def drawTile(i, j, ID):
    X = j * 20 + 20
    Y = i * 20
    canvas.create_rectangle(X, Y, X + 20, Y + 20, fill=ClDt[ID], outline="",tag="tile")
    if ID != 0:
        canvas.create_line(X   , Y   , X+20, Y    ,fill='white', tag="tile")
        canvas.create_line(X   , Y   , X   , Y +20,fill='white', tag="tile")
        canvas.create_line(X+ 1, Y+19, X+19, Y +19,fill='gray' , tag="tile")
        canvas.create_line(X+19, Y   , X+19, Y +19,fill='gray' , tag="tile")
#■4つのタイル表示
def drawTt(iy, ix, bTP, Tt):
    drawTile(iy +Tt[0], ix+Tt[1], bTP); drawTile(iy+Tt[2],  ix+Tt[3], bTP)
    drawTile(iy +Tt[4], ix+Tt[5], bTP); drawTile(iy+Tt[6], ix+Tt[7], bTP)
#■テトリミノの表示
def drawTetrimino(iy, ix, bTP, rot):
    M=len(Tetrimino); R=rot
    if bTP<=0 or bTP>M: return
    Tt=Tetrimino[bTP-1]; N=len(Tt)
    if R >= N: R=N-1
    drawTt(iy,ix, bTP, Tt[R])
def checkTetrimino(iy, ix, bTP, rot):
    M=len(Tetrimino); R=rot
    if bTP<=0 or bTP>M: return False
    Tt=Tetrimino[bTP-1]; N=len(Tt)
    if R >= N: R=N-1
    kk=0
    for i in range(4):
        jj=kk+1
        X=ix+Tt[R][jj]; Y=iy+Tt[R][kk]
        if Y>19: return False
        if X<0 or X>9: return False
        if DT[Y][X] !=0:return False
        kk+=2
    return True

#■初期表示
def initDraw():
    canvas.delete('wall'); canvas.delete('tile')
    canvas.create_rectangle(10 ,  10,  20, 400, fill='#555555', outline='',tag='wall')
    canvas.create_rectangle(220,  10, 230, 400, fill='#555555', outline='',tag='wall')
    canvas.create_rectangle(10 , 400, 230, 410, fill='#555555', outline='',tag='wall')
#■積み残しとテトリミノ表示
def draw():
    canvas.delete('tile')
    for i in range(20):
        for j in range(10):
            if DT[i][j] !=0 :drawTile(i,j,DT[i][j])
    drawTetrimino(Pos[3], Pos[2], Pos[0], Pos[1])
#■積み残しの設定
def setDT(iy, ix, bTP, Tt):
    DT[iy + Tt[0]][ix + Tt[1]] = copy.copy(bTP)
    DT[iy + Tt[2]][ix + Tt[3]] = copy.copy(bTP)
    DT[iy + Tt[4]][ix + Tt[5]] = copy.copy(bTP)
    DT[iy + Tt[6]][ix + Tt[7]] = copy.copy(bTP)
#■テトリミノが停止した際、積み残しにテトリミノ形状を移す
def stopProc(iy, ix, bTP, rot):
    M=len(Tetrimino); R=rot
    if bTP<=0 or bTP>M: return
    Tt=Tetrimino[bTP-1]; N=len(Tt)
    if R >= N: R=N-1
    setDT(iy,ix, bTP, Tt[R])
#■下方向に移動可能かどうかをチェック
def movable():
    return checkTetrimino(Pos[3]+1, Pos[2], Pos[0], Pos[1])
#■左方向に移動可能かどうかをチェック
def movableLeft():
    return checkTetrimino(Pos[3], Pos[2]-1, Pos[0], Pos[1])
#def movableLeft():

#■右方向に移動可能かどうかをチェック
def movableRight():
    return checkTetrimino(Pos[3], Pos[2]+1, Pos[0], Pos[1])

#■回転可能かどうかをチェック
def rotatable(left):
    bTP=Pos[0]; rot=Pos[1]; ix=Pos[2]; iy=Pos[3]
    if bTP == 7: return        #　　■■　のとき回転なし（bTP=70)
    if bTP == 6:               #    ■■
        if (rot == 0):         #   横方向■□■■のときの回転可能のチェック
            if   iy <= 2 or iy >= 8 :return
            elif DT[iy-2][ix] != 0 or DT[iy-1][ix] != 0 or DT[iy+1][ix] != 0 : return False
        elif rot == 1:         #   縦方向■□■■のときの回転可能チェック
            if   ix <= 2 or ix >= 8: return
            elif DT[iy][ix-2] != 0 or DT[iy][ix-1] != 0 or DT[iy][ix+1] != 0: return False
    else:  #   その他の回転可能チェック（周りにあれば回転不可とする）
        for i in range(iy-1, iy+2):
            for j in range(ix - 1, ix + 2):
                if i < 0 or i > 19 or j < 0 or j > 9: return
                if DT[i][j] != 0: return
    if left:
        rot+=1
        if   bTP <  4 and rot > 3             : rot = 0
        elif bTP >= 4 and bTP <= 6 and rot > 1: rot = 0
    else:
        rot-=1
        if   bTP < 4  and rot < 0             : rot = 3;
        elif bTP >= 4 and bTP <= 6 and rot < 0: rot = 1;
    Pos[1]=rot
    draw()
#■下方向への移動
def moveTetris():
    if movable():
        Pos[3]+=1; draw(); return False
    winsound.Beep(700,200)
    stopProc(Pos[3], Pos[2], Pos[0], Pos[1])
    root.update()
    time.sleep(0.5)
    return True
#■行方向が埋まったら削除
def deleteLine(): 
    allflag = True
    while allflag:
        allflag = False; i=20
        for ii in range(20):
            i -= 1
            iflag = True;
            for j in range(10):
                if DT[i][j] == 0: iflag = False; break
            if iflag:
                allflag = True
                k=i
                for kk in range(i):
                    for j in range(10): DT[k][j] = DT[k-1][j]
                    k-=1
                for j in range(10): DT[0][j] =0
                Pos[4]+=10
                root.title("Terris Point = %d" % Pos[4])
#■終了判定
def judgeEnd():
    for ii in range(3):
        for jj in range(10):
            if DT[ii][jj] != 0: return True
    return False
#■キーボードイベント(左右への移動)
def keyPressed(event):
    s=event.char
    #print(s)
    if   ((s == "Z" or s == "z") and movableLeft ()) : Pos[2]-=1
    elif ((s == "X" or s == "x") and movableRight()) : Pos[2]+=1;
#■マウスイベント(回転)

def leftMouseDown(event):
    rotatable(True)
def rightMouseDown(event):
    rotatable(False)
#■ゲーム再開
def rePlay():
    Pos=[0,0,0,0,0]
    for i in range(20):
        for j in range(10):
            DT[i][j]=0
    draw()
  
#■メイン処理   
winsound.Beep(1000,200)
canvas.focus_set()
canvas.bind("<Key>",keyPressed)
canvas.bind("<Button-1>",leftMouseDown)
canvas.bind("<Button-3>",rightMouseDown)
initDraw()
while True:#全体ループ
    Pos[0]=int(random.random()*7+1)
    Pos[1]=0
    Pos[2]=int(random.random()*5+2)
    Pos[3]=2
    while True:#ゲーム中ループ
        if moveTetris():
            break
        deleteLine()
        root.update_idletasks()
        root.update()
        time.sleep(0.5)
    if judgeEnd():#ゲーム終了判定
        winsound.Beep(1000,200)
        if not messagebox.askokcancel("ゲーム終了","再度プレイしますか?"):break
        rePlay()
        
