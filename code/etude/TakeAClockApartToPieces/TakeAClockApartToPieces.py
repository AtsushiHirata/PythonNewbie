from tkinter import *
from tkinter import messagebox
import math
import time

import GuriDataForSplash
import FigureDrawer

# 定数定義
GURI_APP_WIDTH  = 800   #アプリケーションの幅、高さでは、図形を表示する領域の高さ幅とは異なる。
GURI_APP_HEIGHT = 600
GURIVIEW_WIDTH  = 800   #図形を表示する領域の幅、高さき
GURIVIEW_HEIGHT = 600

#■開始処理
root =Tk()
root.title('Guri Prototype')
width  = GURIVIEW_WIDTH
height = GURIVIEW_HEIGHT
canvas=Canvas(root, width=width,height=height,bg="#CCCCCC")
canvas.pack(expand=False)

gdfs = GuriDataForSplash.GuriDataForSplash()
fd = FigureDrawer.FigureDrawer()


#■時計の針の表示
def drawHand():
    t=time.localtime()
    root.title('時計 '+'%02d:%02d:%02d' % (t[3], t[4], t[5]))

    # Guriの図形を描かせてみる
#    fd.drawToWindow(gdfs,t[5],45,1,canvas)
    fd.drawToWindow(gdfs,time.mktime(time.localtime())%360,45,1,canvas)


#■ウィンドウサイズが変化したときの処理
# def changeSize(event):
#     global width
#     width=canvas.winfo_width()
#     h= canvas.winfo_height()
#     if width> h: width=h
#     drawBack()
#     drawHand()
#■1秒単位の処理
def timeProc():
    drawHand()
    # root.after(1000,timeProc)  #1秒後に再度処理を行う
    root.after(500, timeProc)  # 0.3秒後に再度処理を行う


#■ウィンドウが閉じられるときの処理
def exitProc():
    if messagebox.askokcancel("終了","終了してもよろしいですか?"):
        root.destroy()
        sys.exit()

#■メイン処理
# root.bind('<Configure>',changeSize) #ウィンドウサイズが変更されたときchangeSizeを呼び出す。
root.protocol("WM_DELETE_WINDOW",exitProc) #ウィンドウが閉じられたときexitProcを呼び出す。
# drawBack() # 時計の文字盤表示
timeProc() # 1秒ごとの処理（針を表示する）
root.mainloop()
