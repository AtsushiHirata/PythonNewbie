#!/usr/bin/python
# -*- coding: utf-8 -*-

class GuriData:
    """立体グリグリデータを保持する"""

    def __init__(self):                  # コンストラクタ
        self._comment = "超簡易3D editor 立体グリグリ データオブジェクト"
        self._lines  = [ ] # 空のリスト
        self._maxpos = [ 0, 0, 0]
        self._minpos = [ 0, 0, 0]
        self._midpos = [ 0, 0, 0]
        self._maxsize = [ 0, 0, 0]

    def getComment(self):
        return self._comment

    def setComment(self,new_comment):
        self._comment = new_comment
        return True

    def getNumberOfLines(self):
        return len(self._lines)

    def setLine(self,new_line):
        """現在の線分データリスト_linesの末尾に、与えられた線分データを追加する。"""
        self._lines.append(new_line)
        # searchMaxMin()
        return True

    def appendLine(self,new_line):
        """現在の線分データリスト_linesの末尾に、与えられた線分データを追加する。"""
        self._lines.append(new_line)
        # searchMaxMin()
        return True

    def getLine(self,index_of_line):
        """index_of_lineで指定した添字の線分データをリストから取得する。添字は0始まり"""
        if (len(self._lines) < index_of_line) or (index_of_line < 0):
            # Error 不正な添字が指定された
            return []
        else:
            return self._lines[index_of_line]


    def searchMaxposMinposMidposMaxsize(self):
        self._MAX_INT = 99999
        _mid_pos   = [ 0, 0, 0]
        _max_size  = 0
        _maxX      = 0
        _maxY      = 0
        _maxZ      = 0
        _minX      = self._MAX_INT if 0 < len(self._lines)  else 0
        _minY      = self._MAX_INT if 0 < len(self._lines)  else 0
        _minZ      = self._MAX_INT if 0 < len(self._lines)  else 0
        for _a_line in self._lines:
            # 最大値検索
            _maxX = _a_line[0] if _maxX < _a_line[0] else _maxX
            _maxY = _a_line[1] if _maxY < _a_line[1] else _maxY
            _maxZ = _a_line[2] if _maxZ < _a_line[2] else _maxZ
            _maxX = _a_line[3] if _maxX < _a_line[3] else _maxX
            _maxY = _a_line[4] if _maxY < _a_line[4] else _maxY
            _maxZ = _a_line[5] if _maxZ < _a_line[5] else _maxZ
            # 最小値検索
            _minX = _a_line[0] if _a_line[0] < _minX else _minX
            _minY = _a_line[1] if _a_line[1] < _minX else _minY
            _minZ = _a_line[2] if _a_line[2] < _minX else _minZ
            _minX = _a_line[3] if _a_line[3] < _minX else _minX
            _minY = _a_line[4] if _a_line[4] < _minY else _minY
            _minZ = _a_line[5] if _a_line[5] < _minZ else _minZ
        # 中央座標値
        _midX = _maxX - _minX
        _midY = _maxY - _minY
        _midZ = _maxZ - _minZ
        # 各軸方向ごとの最大寸法
        self._maxsize = [ _maxX - _minX, _maxY - _minY, _maxZ - _minZ]
        self._maxpos = [ _maxX, _maxY, _maxZ]
        self._minpos = [ _minX, _minY, _minZ]
        self._midpos = [ _midX, _midY, _midZ]
        _list = [ self._maxpos,  self._minpos,  self._midpos,  self._maxsize ]
        return _list

    def getMaxSize(self):
        """データ内の最大寸法（最大座標値 - 最小座標値)を返す"""
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._maxsize

    def getMaxPos(self):
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._maxpos

    def getMinPos(self):
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._minpos

    def getMidPos(self):
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._midpos

    def loadData(self,filename):
        """
        :param filename:  これから読み込むグリデータのファイル名
        :return:          真理値。読み込み成功ならTrue
        """
        _data = open(filename).read()
        _LinesOfData = _data.split("\n")
        _count = 0
        _number_of_lines = 0
        _temp_comment = ""
        self._lines = []
        for _aline in _LinesOfData:
            if _count == 0:
                _splited = _aline.split()
                self._maxsize = [ _splited[0], _splited[1], _splited[2] ]
                _number_of_lines = int(_splited[3])
            elif _count <= _number_of_lines:
                _splited = _aline.split()
                self._lines.append([int(_splited[0]),int(_splited[1]),int(_splited[2]),int(_splited[3]),int(_splited[4]),int(_splited[5])])
            else:
                _temp_comment = _temp_comment + _aline
            _count+=1
        self._comment = _temp_comment
        return True
