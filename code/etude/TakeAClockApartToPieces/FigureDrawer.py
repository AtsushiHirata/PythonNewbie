#!/usr/bin/python
# -*- coding: utf-8 -*-

import GuriData
from tkinter import *
import math

import GuriConstants

class FigureDrawer:
    """立体グリグリデータをウインドウ上に描くクラス"""

    def drawToWindow(self,gd,lon,lat,r,canvas):
        canvas.delete("DT")
        for i in range(gd.getNumberOfLines()):
            aline = gd.getLine(i)
            rotated = self.rotateALine(aline, lon, lat)
            # print("r:" + str(rotated))
            transported = self.transToWindow(rotated,gd.getMidPos(),r)
            # print("t:" + str(transported))
            # #  直描き
            # canvas.create_line(int(aline[0]),int(aline[1]),
            #                    int(aline[3]),int(aline[4]), fill="blue", tag="DT", width=2)
            # # Rotateしてから描画
            # canvas.create_line(int(rotated[0]),int(rotated[1]),
            #                    int(rotated[3]),int(rotated[4]), fill="green", tag="DT", width=2)
            # 画面に適した座標にしてから描画
            canvas.create_line(int(transported[0]),int(transported[1]),
                               int(transported[2]),int(transported[3]), fill="red", tag="DT", width=2)
        return True

    def rotateALine(self,aline,longitude_deg,latitude_deg):
        lon = lon_rad = math.radians(longitude_deg) # 視点の経度（左右）
        lat = lat_rad = math.radians(latitude_deg)  # 視点の緯度（上下）
        xs = float(aline[0]) # means Start position X
        ys = float(aline[1])
        zs = float(aline[2])
        xe = float(aline[3]) # means End position X
        ye = float(aline[4])
        ze = float(aline[5])
        temp = [ xs * math.cos(lon) + zs * math.sin(lon),
                 ys,
                 -xs * math.sin(lon) + zs * math.cos(lon),

                 xe * math.cos(lon) + ze * math.sin(lon),
                 ye,
                 -xe * math.sin(lon) + ze * math.cos(lon)]
        return [ temp[0],
                 temp[1] * math.cos(lat) + temp[2] * math.sin(lat),
                 -temp[1] * math.sin(lat) + temp[2]* math.cos(lat),

                 temp[3],
                 temp[4] * math.cos(lat) + temp[5] * math.sin(lat),
                 -temp[4] * math.sin(lat) + temp[5] * math.cos(lat)]

    def transToWindow(self,aline,mid,r):
        """
        :param aline:   変換まえの線分データ
        :param mid:     図形の座標軸ごとの中心座標
        :param r:       表示倍率
        :return:        アプリケーションのウインドウに適した線分の座標
        """
        xs = float(aline[0])  # means Start position X
        ys = float(aline[1])
        xe = float(aline[3])  # means End position X
        ye = float(aline[4])
        txs = r * ( xs - float(mid[0]) ) + GuriConstants.GURI_WINDOW_WIDTH / 2
        txe = r * ( xe - float(mid[0]) ) + GuriConstants.GURI_WINDOW_WIDTH / 2
        tys = GuriConstants.GURI_WINDOW_HEIGHT / 2 - ( r * ( ys - float(mid[1])))
        tye = GuriConstants.GURI_WINDOW_HEIGHT / 2 - ( r * ( ye - float(mid[1])))
        return [txs, tys,  txe, tye]