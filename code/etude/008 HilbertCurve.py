import turtle
#■C曲線をTurtleグラフィックスで描く
#相対座標位置への移動
def draw(t,X,Y):
    P=t.pos()
    NX=P[0]+X*0.1
    NY=P[1]+Y*0.1
    t.goto(NX,NY)
def setOrder(order):
        H = 1
        S = 2000
        S = S / H
        for i in range(2, order+1):
            H=H*2+1
        return S/H
#ヒルベルト曲線を描く
def HilbertCurve(t,order):
    H=setOrder(order)
    ID=order
    RUL(t,ID,H)
#右→上→左
def RUL(t,ID,H):
    if ID>0:
        ID=ID-1
        URD(t,ID,H)
        draw(t,H,0)
        RUL(t,ID,H)
        draw(t,0,H)
        RUL(t,ID,H)
        draw(t,-H,0)
        DLU(t,ID,H)
        ID=ID+1
#下→左→上
def DLU(t,ID,H):
    if ID>0:
        ID=ID-1
        LDR(t,ID,H)
        draw(t,0,-H)
        DLU(t,ID,H)
        draw(t,-H,0)
        DLU(t,ID,H)
        draw(t,0,H)
        RUL(t,ID,H)
        ID=ID+1
#左→下→右
def LDR(t,ID,H):
    if ID>0:
        ID=ID-1
        DLU(t,ID,H)
        draw(t,-H,0)
        LDR(t,ID,H)
        draw(t,0,-H)
        LDR(t,ID,H)
        draw(t,H,0)
        URD(t,ID,H)
        ID=ID+1
#上→右→下
def URD(t,ID,H):
    if ID>0:
        ID=ID-1
        RUL(t,ID,H)
        draw(t,0,H)
        URD(t,ID,H)
        draw(t,H,0)
        URD(t,ID,H)
        draw(t,0,-H)
        LDR(t,ID,H)
        ID=ID+1
#■メイン処理
t=turtle.Pen()
t.screen.title("Hilbert Curve used Turtle graphics")
#タートル移動を速くする
t.speed(0)
#描く順序を見たいとき以下の第1引数を小さくする
t.screen.tracer(100000,0)
#タートルを見えなくする
t.hideturtle()
#描画開始位置に移動
t.up()
t.goto(-300,40)
t.down()
#線幅とカラーを設定
t.width(2)
t.color('blue')
#ヒルベルト曲線描画開始
HilbertCurve(t,5)
t.screen.tracer(1,0)
t.screen.mainloop()

            
        
