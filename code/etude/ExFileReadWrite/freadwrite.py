# -*- coding: utf-8 -*-
import codecs
fin  = codecs.open('read.txt', 'r', 'shift_jis')
fout_euc = codecs.open('euc_out.txt', 'w', 'euc_jp')
fout_utf = codecs.open('utf-8.txt', 'w', 'utf-8')
for row in fin:
    fout_euc.write(row)
    fout_utf.write(row)
fin.close()
fout_euc.close()
fout_utf.close()
