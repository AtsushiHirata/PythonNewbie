from tkinter import *             # Python 2.7のとき import Tkinter
from tkinter import messagebox    #                  import tkMessageBox

root=Tk()
canvas=Canvas(root,width=500,height=500)
canvas.pack()
canvas.focus_set()
listdata=[]
def keyPressed(event):
    ch=event.char
    if ch!='':
        listdata.append(ch)
    print(listdata)
    print("Key Pressed char=", ascii(ch)," key sombol=", event.keysym)
def keyReturn(event):
    print("Return Pressed key sombol=", event.keysym)
def shiftUp(event):
    print("Shift Up")
def altUp(event):
    print("Alt Up")
#■終わりの確認
def deleteWindow():
    if messagebox.askokcancel("Quit","終了します。よろしいですか?"): # Python 2.7のとき tkMessageBox
        root.destroy()
#■バインドの指定
root.protocol("WM_DELETE_WINDOW", deleteWindow)
# キーボード・イベント
canvas.bind("<Key>",keyPressed)
canvas.bind("<Shift-Up>",shiftUp)
canvas.bind("<Alt-Up>",altUp)
canvas.bind("<Return>",keyReturn)
#■ その他のキーボード・イベント
#   <Cancel>, <BackSpace>, <Tab>, <Shift_L>(any Shift key), <Control_LShift_L>(any Control key),
#   <Alt_L>(any Alt key), <Pause>, <Caps_Lock>, <Escape>, <Prior>(Page Up>, <Next>(Page Down),
#   <End><Home>, <Left>, <Up>, <Right>, <Down>, <Print>, <Insert>, <Delete>, <F1>, <F2>,…,<F12>,
#   <Bum_Lock>, <Scroll_Lock>, <space>（空白), <less>(空白より小さい)
#   <Shift-Up>(シフトキーを押したまま<Up>), <Alt-Up>(Altキーを押したまま<Up>), 
#   <Control-Up>(シフトキーを押しまま<Up>), その他"a"などの文字を直接記述 
#■メインループ
root.mainloop()
    

    
   
