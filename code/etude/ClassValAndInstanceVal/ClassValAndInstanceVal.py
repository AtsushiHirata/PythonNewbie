class Hoge(object):
    y = 2017
    print('y is',y) # this is OK
    # print('self.y is', self.y) # error

    def set_xz(self, x):
        self.x = x
        z = 9

    def print_xyz(self):
        print('self.x is', self.x)
        print('self.y is', self.y)
        # print('z      is',      z) # error
        # print('     y is',      y) # error
        #    print('     y is',      y)
        #    NameError: name 'y' is not defined

print('Hoge.y is ', Hoge.y)

a = Hoge()
print('a.y is', a.y)
a.set_xz(123)

print('a.x is', a.x)

# print('a.z is', a.z) # error
a.print_xyz()

b = Hoge()
b.set_xz(321)
b.print_xyz()
