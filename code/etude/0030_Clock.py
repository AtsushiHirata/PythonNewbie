from tkinter import *
from tkinter import messagebox
import math
import time
#■開始処理
root =Tk()
root.title('時計')
root.minsize(100,100)
root.maxsize(400,400)
width=240
canvas=Canvas(root, width=width,height=width,bg="#CCCCCC")
canvas.pack(expand=True, fill=BOTH)
#■1秒刻みのsin/cosを計算しておく
sinTab=[]
cosTab=[]
P60=math.pi/30
for i in range(60):
    rad=P60*i
    sinTab.append(math.sin(rad))
    cosTab.append(math.cos(rad))
#■時計の周りの円形(実際の座標はdrawBackで決定)
bcircle=canvas.create_oval(3,3,width-3,width-3, fill="#AAAAAA",outline="#000000")
circle=canvas.create_oval(6,6,width-6,width-6, fill="#FFFFFF",outline="#000000")
#■時計上の文字の用意(実際の文字座標はdrawBackで決定)
charH=[]
for i in range(12):
    ii=i
    if i==0:
        ii=12
    charH.append(canvas.create_text(10,10,text="%d" % ii,font=('Times',24)))
    # charH.append(canvas.create_text(10,10,text="一",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="二",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="三",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="四",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="五",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="六",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="七",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="八",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="九",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="十",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="Ⅺ",font=('Osaka',24)))
    # charH.append(canvas.create_text(10,10,text="Ⅻ",font=('Osaka',24)))
#■時計上の目盛り線の用意(実際の座標はdrawBackで決定)
graduation=[]
for i in range(60):
    tw=1
    if (i % 5)==0:
        tw=3
    graduation.append(canvas.create_line(1,1,3,3,width=tw))
#■時計の針の用意(実際の座標はdrawHandで決定)
houH=canvas.create_line(10,10,20,20,fill='black',width=4)
minH=canvas.create_line(10,10,20,20,fill='blue',width=3)
secH=canvas.create_line(10,10,20,20,fill='red',width=1)
#■時計上の中央軸の用意(実際の座標はdrawBackで決定)
center=canvas.create_oval(5,5,8,8, fill="#555555",outline="#000000")
#■時計の文字盤の表示
def drawBack():
    global width
    r=width/2
    #■周りと中央の配置
    canvas.coords(bcircle,3,3,width-3,width-3)
    canvas.coords(circle,6,6,width-6,width-6)
    canvas.coords(center,r-3,r-3,r+3,r+3)
    #■目盛りの配置
    rr1=r-6.0
    rr2=r-12.0
    for i in range(60):
        x1 = r + rr1 * sinTab[i]
        y1 = r + rr1 * cosTab[i]
        x2 = r + rr2 * sinTab[i]
        y2 = r + rr2 * cosTab[i]
        canvas.coords(graduation[i],x1,y1,x2,y2)
    #■文字の配置
    rr3=r-30.0
    for i in range(12):
        n=i*5
        x=r+rr3*sinTab[n]
        y=r-rr3*cosTab[n]
        canvas.coords(charH[i],x,y)
#■時計の針の表示
def drawHand():
    t=time.localtime()
    root.title('時計 '+'%02d:%02d:%02d' % (t[3], t[4], t[5]))
    r=width/2
    #各針の長さ
    rs=r*14/16
    rm=r*12/16
    rh=r*9/16
    #秒針の配置
    x1=r-20*sinTab[t[5]]
    y1=r+20*cosTab[t[5]]
    x2=r+rs*sinTab[t[5]]
    y2=r-rs*cosTab[t[5]]
    canvas.coords(secH,x1,y1,x2,y2)
    #分針の配置
    x1=r-10*sinTab[t[4]]
    y1=r+10*cosTab[t[4]]
    x2=r+rm*sinTab[t[4]]
    y2=r-rm*cosTab[t[4]]
    canvas.coords(minH,x1,y1,x2,y2)
    #時間針の配置
    k=t[3]
    if k>=12: k-=12       #12時以降は12を差し引く
    k=k*5+int(t[4]/12)    #分による時間針のズレを加える
    x2=r+rh*sinTab[k]
    y2=r-rh*cosTab[k]
    canvas.coords(houH,r,r,x2,y2)
#■ウィンドウサイズが変化したときの処理
def changeSize(event):
    global width
    width=canvas.winfo_width()
    h= canvas.winfo_height()
    if width> h: width=h
    drawBack()
    drawHand()
#■1秒単位の処理
def timeProc():
    drawHand()
    root.after(1000,timeProc)  #1秒後に再度処理を行う
#■ウィンドウが閉じられるときの処理
def exitProc():
    if messagebox.askokcancel("終了","終了してもよろしいですか?"):
        root.destroy()
        sys.exit()

#■メイン処理
root.bind('<Configure>',changeSize) #ウィンドウサイズが変更されたときchangeSizeを呼び出す。
root.protocol("WM_DELETE_WINDOW",exitProc) #ウィンドウが閉じられたときexitProcを呼び出す。
drawBack() # 時計の文字盤表示
timeProc() # 1秒ごとの処理（針を表示する）
root.mainloop()
