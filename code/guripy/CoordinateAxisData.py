#!/usr/bin/python
# -*- coding: utf-8 -*-

import GuriConstants


class CoordinateAxisData:
    """座標軸を表示するためのデータを保持するクラス。
       グリグリデータの形式をとる
    """

    line_length = GuriConstants.COORDINATE_AXIS_LINE_LENGTH

    def __init__(self):                  # コンストラクタ
        self._comment = "座標軸表示用データ"
        self._setDefaultLines()
        self._maxpos = [self.line_length, self.line_length, self.line_length]
        self._minpos = [0, 0, 0]
        lenDiv2 = self.line_length / 2
        self._midpos = [lenDiv2, lenDiv2, lenDiv2]
        self._maxsize = self.line_length

    def getComment(self):
        """コメントを返す。このクラスの場合は、アプリケーション名"""
        return self._comment

    def setComment(self, new_comment):
        """コメントをセットするメソッドは、何も変化を起こさせない。固定データのクラスだから。"""
        return True

    def _setDefaultLines(self):
        """線分データのリストを作成"""
        self._lines = [
            [0, 0, 0, self.line_length, 0, 0],
            [0, 0, 0, 0, self.line_length, 0],
            [0, 0, 0, 0, 0, self.line_length]
        ]

    def getLine(self, n):
        """n番目の線分データを返す。データの先頭は0番目とする。線分データとして、始点座標、終点座標の数値リストを返す。"""
        if 0 <= n and n < len(self._lines):
            return self._lines[n]
        else:
            return []

    def setLine(self, nl):
        """
        引数に与えた線分への参照から、座標データをデータの配列にセット　と見せかけて無視
        テスト用、あるいはスプラッシュ用のデータなので、変更は不可
        """
        return True  # 何を受け取ってもTrueを返す

    def appendLine(self, nl):
        """
        setLineと同じ。何もしない。
        """
        return self.setLine(nl)

    def getNumberOfLines(self):
        return len(self._lines)

    def getMaxPos(self):
        """最大座標値を返す。"""
        return self._maxpos

    def getMinPos(self):
        """最小座標値を返す。"""
        return self._minpos

    def getMidPos(self):
        """中央座標値を返す。"""
        return self._midpos

    def getMaxSize(self):
        """最大寸法を返す"""
        return self._maxsize

    def searchMaxposMinposMidposMaxsize(self):
        _list = [self._maxpos,  self._minpos,  self._midpos,  self._maxsize]
        return _list

    def loadData(self, filename):
        raise NotImplementedError(
            "座標軸クラスなので、ファイル{0}の読み込みに対応しません。".format(filename))
        return True

    def saveData(self, filename):
        raise NotImplementedError(
            "座標軸クラスなので、ファイル{0}の書き込みに対応しません。".format(filename))
        return True
