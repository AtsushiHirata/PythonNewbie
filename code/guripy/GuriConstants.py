#!/usr/bin/python
# -*- coding: utf-8 -*-

GURI_WINDOW_WIDTH = 800
GURI_WINDOW_HEIGHT = 600
# 図形を描画する領域の幅と高さ
GURI_VIEW_WIDTH = 800
GURI_VIEW_HEIGHT = 600

DATA_PATH = './data/'

GURI_VIEW_BGCOLOR = "#CCCCCC"
VIEW_MODE_BGCOLOR = "#008000"
DRAW_MODE_BGCOLOR = "#FFFFFF"

X_AXIS_COLOR = "blue"
Y_AXIS_COLOR = "yellow"
Z_AXIS_COLOR = "green"

FIG_LINE_WIDTH = 2
AXIS_LINE_WIDTH = 0.5
FIG_LINE_COLOR_VIEWING = "white"
FIG_LINE_COLOR_DRAWING = "black"
# 回転表示中、次の図に更新するまで待機する時間の長さ　ミリ秒
TICK_TIME = 200
# 回転表示中、一度で回る確度の大きさ
TICK_UNIT_ANGLE = 5
# 拡大や縮小の比率増分
UNIT_RATIO = 0.1
MAX_RATIO = 5.0
MIN_RATIO = 0.0

APP_NAME = '立体グリグリ・ニシキヘビ版'

COORDINATE_AXIS_LINE_LENGTH = 1000
# 作図モード時のポインタと、未決定の線の色
COLOR_POINTER_FROM = "black"
COLOR_POINTER_TO = "red"
COLOR_TEMP_LINE = "red"
# 描画ポインタの単位移動距離
DEFAULT_UNIT_LENGTH = 20
DIAMETER_POINTER = 10

MAX_POS = 1000
