#!/usr/bin/python
# -*- coding: utf-8 -*-


class GuriData:
    """立体グリグリデータを保持する"""

    def setDefault(self):
        self._comment = "超簡易3D editor 立体グリグリ データオブジェクト"
        self._lines = []
        self._maxpos = [0, 0, 0]
        self._minpos = [0, 0, 0]
        self._midpos = [0, 0, 0]
        self._maxsize = [0, 0, 0]

    def __init__(self):
        self.setDefault()
        # self._comment = "超簡易3D editor 立体グリグリ データオブジェクト"
        # self._lines = []
        # self._maxpos = [0, 0, 0]
        # self._minpos = [0, 0, 0]
        # self._midpos = [0, 0, 0]
        # self._maxsize = [0, 0, 0]

    def getComment(self):
        return self._comment

    def setComment(self, new_comment):
        self._comment = new_comment
        return True

    def getNumberOfLines(self):
        return len(self._lines)

    def setLine(self, new_line):
        """現在の線分データリスト_linesの末尾に、与えられた線分データを追加する。"""
        self._lines.append(new_line)
        self.searchMaxposMinposMidposMaxsize()
        return True

    def appendLine(self, new_line):
        """現在の線分データリスト_linesの末尾に、与えられた線分データを追加する。"""
        self._lines.append(new_line)
        self.searchMaxposMinposMidposMaxsize()
        return True

    def getLine(self, index_of_line):
        """index_of_lineで指定した添字の線分データをリストから取得する。添字は0始まり"""
        if (len(self._lines) < index_of_line) or (index_of_line < 0):
            # Error 不正な添字が指定された
            return []
        else:
            return self._lines[index_of_line]

    def deleteTail(self):
        """末尾の線分データを削除する。"""
        if self.getNumberOfLines() != 0:
            self._lines.pop()
            return True
        else:  # no lines
            return False

    def deleteLine(self, index):
        # indexで指定した線分が存在すれば、その線分データを削除する。
        if 0 <= index < len(self._lines):
            del self._lines[index]
            return True
        else:
            return False

    def isExists(self, someline):
        """引数に与えた線分データが、_linesの中に存在すれば真を返す。"""
        _reversed = [someline[3], someline[4], someline[5],
                     someline[0], someline[1], someline[2]]
        for _a_line in self._lines:
            if _a_line == someline or _a_line == _reversed:
                return True
            else:
                pass
        return False

    def getIndex(self, someline):
        """somelineで指定した要素の添字を返す。存在しなければ−1を返す"""
        if self.isExists(someline):
            _reversed = [someline[3], someline[4], someline[5],
                         someline[0], someline[1], someline[2]]
            if self._lines.count(someline) != 0:
                return self._lines.index(someline)
            else:
                return self._lines.index(_reversed)
        else:
            return -1

    def searchMaxposMinposMidposMaxsize(self):
        self._MAX_INT = 99999
        _mid_pos = [0, 0, 0]
        _max_size = 0
        _maxX, _maxY, _maxZ = 0, 0, 0
        _minX = _minY = _minZ = self._MAX_INT if 0 < len(self._lines) else 0
        for _a_line in self._lines:
            # 最大値検索
            _maxX = max(_a_line[0], _a_line[3], _maxX)
            _maxY = max(_a_line[1], _a_line[4], _maxY)
            _maxZ = max(_a_line[2], _a_line[5], _maxZ)
            # 最小値検索
            _minX = min(_a_line[0], _a_line[3], _minX)
            _minY = min(_a_line[1], _a_line[4], _minY)
            _minZ = min(_a_line[2], _a_line[5], _minZ)
        # 中央座標値
        _midX = int((_maxX + _minX)/2)
        _midY = int((_maxY + _minY)/2)
        _midZ = int((_maxZ + _minZ)/2)
        # 各軸方向ごとの最大寸法
        self._maxsize = [_maxX - _minX, _maxY - _minY, _maxZ - _minZ]
        self._maxpos = [_maxX, _maxY, _maxZ]
        self._minpos = [_minX, _minY, _minZ]
        self._midpos = [_midX, _midY, _midZ]
        _list = [self._maxpos, self._minpos, self._midpos, self._maxsize]
        return _list

    def getMaxSize(self):
        """データ内の最大寸法（最大座標値 - 最小座標値)を返す"""
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._maxsize

    def getMaxPos(self):
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._maxpos

    def getMinPos(self):
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._minpos

    def getMidPos(self):
        _list = self.searchMaxposMinposMidposMaxsize()
        return self._midpos

    def loadData(self, filename):
        """
        :param filename:  これから読み込むグリデータのファイル名
        :return:          真理値。読み込み成功ならTrue
        """
        f = open(filename)
        _data = f.read()
        f.close()
        _LinesOfData = _data.split("\n")
        _count = 0
        _number_of_lines = 0
        _temp_comment = ""
        self._lines = []
        for _aline in _LinesOfData:
            if _count == 0:
                _vals = _aline.split()
                self._maxsize = [[float(_vals[i]) for i in range(3)]]
                _number_of_lines = int(_vals[3])
            elif _count <= _number_of_lines:
                _vals = _aline.split()
                self._lines.append([float(_vals[i]) for i in range(6)])
            else:
                _temp_comment = _temp_comment + _aline
            _count += 1
        self._comment = _temp_comment
        return True

    def saveData(self, filename):
        """
        :param filename: これから保存するグリデータのファイル名
        :return:         真理値。保存成功ならTrue
        """
        # 出力用のデータを作成
        # １行目　最大寸法x、y、z、線分の本数
        _ms = self.getMaxSize()
        _datastring = str(_ms[0]) + "\t" + str(_ms[1]) + "\t" + str(_ms[2]) \
            + "\t" + str(self.getNumberOfLines()) + "\n"
        # ここから図形に含まれる線分座標の羅列
        for i in range(self.getNumberOfLines()):
            _ln = self.getLine(i)
            _datastring += str(_ln[0]) + "\t" \
                + str(_ln[1]) + "\t" \
                + str(_ln[2]) + "\t" \
                + str(_ln[3]) + "\t" \
                + str(_ln[4]) + "\t" \
                + str(_ln[5]) + "\n"
        # コメント、著作権情報
        _datastring += self.getComment()
        f = open(filename, 'w')
        f.write(_datastring)
        f.close()
        return True

    def clearData(self):
        """線分データを全て消去、コメントもリセット"""
        self.setDefault()
        return True
