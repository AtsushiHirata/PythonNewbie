#!/usr/bin/python
# -*- coding: utf-8 -*-

import copy
import GuriConstants


class AppData:
    """立体グリグリ実行中に、オブジェクトをまたいで必要な情報を保持する。"""

    longitude = 0
    latitude = 45
    directionTurn = 'RIGHT'    """ 'STOP', 'RIGHT', 'LEFT', 'UP', 'DOWN' """
    directionLon = 1
    directionLat = 0
    ratio = 1
    mode = 'VIEWING'    """ 'VIEWING', 'DRAWING' """
    backcolor = GuriConstants.VIEW_MODE_BGCOLOR
    linecolor = GuriConstants.FIG_LINE_COLOR_VIEWING

    datapath = GuriConstants.DATA_PATH  # ファイルロード、セーブにあたって開くべきフォルダのパス
    filename = ""  # データファイルのファイル名（フルパス）
    # 作図ポインタの位置
    pointerfrom = [0, 0, 0]
    pointerto = [0, 0, 0]
    unitlength = GuriConstants.DEFAULT_UNIT_LENGTH
    # 作図中の状態を保持　始点未決定 "FROM",始点決定済み・終点未決定 "TO"
    drawingcondition = "FROM"

    def setDirectionTurn(self, dir):
        if dir in {'STOP', 'RIGHT', 'LEFT', 'UP', 'DOWN'}:
            self.directionTurn = dir
        else:
            self.directionTurn = 'STOP'
        if dir == 'STOP':
            self.directionLon, self.directionLat = 0, 0
        elif dir == 'RIGHT':
            self.directionLon, self.directionLat = 1, 0
        elif dir == 'LEFT':
            self.directionLon, self.directionLat = -1, 0
        elif dir == 'UP':
            self.directionLon, self.directionLat = 0, 1
        elif dir == 'DOWN':
            self.directionLon, self.directionLat = 0, -1
        return self.directionTurn

    def upRatio(self):
        if (self.ratio + GuriConstants.UNIT_RATIO) < GuriConstants.MAX_RATIO:
            self.ratio = self.ratio + GuriConstants.UNIT_RATIO

    def downRatio(self):
        if GuriConstants.MIN_RATIO < (self.ratio - GuriConstants.UNIT_RATIO):
            self.ratio = self.ratio - GuriConstants.UNIT_RATIO

    def setView(self, name):
        if name in {'FRONT', 'TOP', 'SIDE', 'ISOMETRIC'}:
            self.setDirectionTurn('STOP')
            if name == 'FRONT':
                self.longitude, self.latitude = 0, 0
            elif name == 'TOP':
                self.longitude, self.latitude = 0, 90
            elif name == 'SIDE':
                self.longitude, self.latitude = 90, 0
            elif name == 'ISOMETRIC':
                self.longitude, self.latitude = 45, 30

    def tickAngle(self):
        """単位角度分表示角度を変化させる"""
        self.longitude += GuriConstants.TICK_UNIT_ANGLE * self.directionLon
        self.latitude += GuriConstants.TICK_UNIT_ANGLE * self.directionLat
        self.longitude = self.normalizeAngle(self.longitude)
        self.latitude = self.normalizeAngle(self.latitude)
        return True

    def normalizeAngle(self, angle):
        """角度の正規化。0°から360°の間に収める"""
        result = angle
        if 360 < angle:
            result -= 360
        elif angle < 0:
            result += 360
        return result

    def rotateTurnDirection(self):
        """回転方向を順次変更（ローテート）する指示があった場合に、回転方向情報を適切に変更する"""
        prev_dir = self.directionTurn
        if prev_dir == 'STOP':
            self.setDirectionTurn('RIGHT')
        elif prev_dir == 'RIGHT':
            self.setDirectionTurn('LEFT')
        elif prev_dir == 'LEFT':
            self.setDirectionTurn('UP')
        elif prev_dir == 'UP':
            self.setDirectionTurn('DOWN')
        elif prev_dir == 'DOWN':
            self.setDirectionTurn('RIGHT')
        return True

    def setMode(self, mode):
        if mode in {'VIEWING', 'DRAWING'}:
            if mode == 'VIEWING':
                self.backcolor = GuriConstants.VIEW_MODE_BGCOLOR
                self.linecolor = GuriConstants.FIG_LINE_COLOR_VIEWING
            elif mode == 'DRAWING':
                self.backcolor = GuriConstants.DRAW_MODE_BGCOLOR
                self.linecolor = GuriConstants.FIG_LINE_COLOR_DRAWING
            self.mode = mode
        return self.mode

    def setPointerFrom(self, pos):
        if (0 <= pos[0] <= GuriConstants.MAX_POS
                and 0 <= pos[1] <= GuriConstants.MAX_POS
                and 0 <= pos[2] <= GuriConstants.MAX_POS):
            self.pointerfrom = copy.deepcopy(pos)
        return self.pointerfrom

    def setPointerTo(self, pos):
        if (0 <= pos[0] <= GuriConstants.MAX_POS
                and 0 <= pos[1] <= GuriConstants.MAX_POS
                and 0 <= pos[2] <= GuriConstants.MAX_POS):
            self.pointerto = copy.deepcopy(pos)
        return self.pointerto

    def unitmovePointerFrom(self, dir):
        cur = copy.deepcopy(self.pointerfrom)
        if (dir == 'x'):
            cur[0] += GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'X'):
            cur[0] -= GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'y'):
            cur[1] += GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'Y'):
            cur[1] -= GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'z'):
            cur[2] += GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'Z'):
            cur[2] -= GuriConstants.DEFAULT_UNIT_LENGTH
        else:
            print("unitmovePointerFrom: Unknown direction. {0}".format(dir))
        self.setPointerFrom(cur)
        return self.pointerfrom

    def unitmovePointerTo(self, dir):
        cur = copy.deepcopy(self.pointerto)
        if (dir == 'x'):
            cur[0] += GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'X'):
            cur[0] -= GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'y'):
            cur[1] += GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'Y'):
            cur[1] -= GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'z'):
            cur[2] += GuriConstants.DEFAULT_UNIT_LENGTH
        elif (dir == 'Z'):
            cur[2] -= GuriConstants.DEFAULT_UNIT_LENGTH
        else:
            print("unitmovePointerTo: Unknown direction. {0}".format(dir))
        self.setPointerTo(cur)
        return self.pointerto

    def setDrawingCondition(self, cond):
        if cond in {'FROM', 'TO'}:
            self.drawingcondition = cond
        return self.drawingcondition
