#!/usr/bin/python
# -*- coding: utf-8 -*-

# from tkinter import *
from tkinter import Tk
from tkinter import Canvas
from tkinter import Button
from tkinter import StringVar
from tkinter import Entry
from tkinter import messagebox
import tkinter
import tkinter.filedialog
import tkinter as tk
import math
import time
import copy
import sys

import GuriDataForSplash
import FigureDrawer
import GuriConstants
import AppData
import GuriData
import CoordinateAxisData


def drawFigure():
    """保持している図形データを描画する"""
    fd.drawToGuriView(gd, ad.longitude, ad.latitude, ad.ratio, canvas, ad)
    ad.tickAngle()


def timerProc():
    """一定時間ごとの処理。これは図形を回転表示するためのタイマーとして使っている"""
    drawFigure()
    root.after(GuriConstants.TICK_TIME, timerProc)


def exitProc():
    """ウィンドウが閉じられるときの処理"""
    if messagebox.askokcancel("終了", "終了してもよろしいですか?"):
        root.destroy()
        sys.exit()


def unitmove(dir):
    if ad.mode == 'DRAWING':
        if ad.drawingcondition == 'FROM':
            ad.unitmovePointerFrom(dir)
            ad.unitmovePointerTo(dir)
        else:
            ad.unitmovePointerTo(dir)


def deleteLine():
    if ad.mode == 'DRAWING':
        if ad.drawingcondition == 'FROM':
            return gd.deleteTail()
        else:  # ad.drawingcondition == 'TO'
            # 作図線が重複するデータがあれば、それを削除
            line = [ad.pointerfrom[0], ad.pointerfrom[1], ad.pointerfrom[2],
                    ad.pointerto[0], ad.pointerto[1], ad.pointerto[2]]
            print("deleteing line({0})".format(line))
            if gd.isExists(line):
                print("{0} is exists".format(line))
                index = gd.getIndex(line)
                print("line{0} index is {1}".format(line, index))
                return gd.deleteLine(index)


def keyPressed(event):
    """キー操作に応じた処理への窓口"""
    ch = event.char
    if ch != '':
        listdata.append(ch)
    print(listdata)
    print("Key Pressed char=", ascii(ch), " key sombol=", event.keysym)
    if ch in {'a', 'b'}:
        print('a, b')
    elif ch == 'c':
        print('c')
    elif ch in {'x', 'X', 'y', 'Y', 'z', 'Z'}:
        print(ch)
        unitmove(ch)
    elif ch == 'o' or ch == 'O':
        print(ch)
        if ad.mode == 'DRAWING':
            if ad.drawingcondition == 'FROM':
                ad.setPointerFrom([0, 0, 0])
                ad.setPointerTo([0, 0, 0])
            else:
                ad.setPointerTo([0, 0, 0])
    elif ch == 'd':
        print(ch)
        if deleteLine():
            # 成功したら作図中のポインタを'FROM'設定に
            print("line deleted.")
            ad.setDrawingCondition('FROM')
            if ad.pointerfrom != ad.pointerto:
                ad.pointerfrom = copy.deepcopy(ad.pointerto)
            else:
                tail = gd.getLine(gd.getNumberOfLines()-1)
                ad.pointerfrom = [tail[3], tail[4], tail[5]]
                ad.pointerto = [tail[3], tail[4], tail[5]]
        else:
            print("line deleting failed.")
    elif ch == ' ':
        funcChangeTurn()


def funcChangeTurn():
    """回転方向変更操作があった時の処理"""
    ad.rotateTurnDirection()


def keyReturn(event):
    print("Return Pressed key sombol=", event.keysym)
    if ad.drawingcondition == 'FROM':
        ad.setDrawingCondition('TO')
        ad.setPointerTo(ad.pointerfrom)
    elif ad.drawingcondition == 'TO':
        _from = ad.pointerfrom
        _to = ad.pointerto
        gd.appendLine([_from[0], _from[1], _from[2], _to[0], _to[1], _to[2]])
        ad.setDrawingCondition('FROM')
        ad.setPointerFrom(ad.pointerto)


def deleteKeyPressed(event):
    print("Delete key pressed.")


def backspaceKeyPressed(event):
    print("BackSpace key pressed.")


def shift_L(event):
    print("Shift key pressed.")


def shiftUp(event):
    print("Shift Up")


def altUp(event):
    print("Alt Up")


def keyEscape(event):
    print("Escape key")
    funcESC()


def funcESC():
    ad.setDirectionTurn('STOP')


def keyF1(event):
    print("F1 key 拡大")
    ad.upRatio()


def keyF2(event):
    print("F2 key 縮小")
    ad.downRatio()


def keyF3(event):
    print("F3 key 正面図")
    funcFrontView()


def funcFrontView():
    ad.setView('FRONT')


def keyF4(event):
    print("F4 key 平面図")
    funcTopView()


def funcTopView():
    ad.setView('TOP')


def keyF5(event):
    print("F5 key 右側面図")
    funcSideView()


def funcSideView():
    ad.setView('SIDE')


def keyF6(event):
    print("F6 key 等角図")
    funcIsometricView()


def funcIsometricView():
    ad.setView('ISOMETRIC')


def loadGuriData():
    args = {"initialdir": "./data/",
            "filetypes": [("立体グリグリファイル", "*.gri")],
            "title": "データファイルの読み込み"
            }
    filename = str(tkinter.filedialog.askopenfilename(**args))
    gd.loadData(filename)
    var.set(gd.getComment())


def saveGuriData():
    args = {"initialdir": "./data/",
            "filetypes": [("立体グリグリファイル", "*.gri")],
            "title": "データファイルの保存"
            }
    filename = str(tkinter.filedialog.asksaveasfilename(**args))
    gd.saveData(filename)


def setViewingMode():
    ad.setMode('VIEWING')
    canvas['bg'] = GuriConstants.VIEW_MODE_BGCOLOR


def setDrawingMode():
    ad.setMode('DRAWING')
    canvas['bg'] = GuriConstants.DRAW_MODE_BGCOLOR

    """
    作図モードに入ったら、
        座標軸を表示
        作図ポインタを表示。始点が決まるまでは白丸、始点が決まったら、
        白丸はそのままの位置で動かなくなり、終点を指定するための赤丸を表示する。
    """


def toggleMode():
    """
    起動時デフォルトの閲覧(VIEWING)モードと、作図(DRAWING)モードの切り替えを行う
    """
    if ad.mode == 'VIEWING':
        setDrawingMode()
    elif ad.mode == 'DRAWING':
        setViewingMode()


def clearData():
    """線分データを全て消去する"""
    if ad.mode == 'VIEWING':
        pass
    elif ad.mode == 'DRAWING':
        gd.clearData()
        var.set(gd.getComment())


def callback():
    print("click! {0}".format(var.get()))
    gd.setComment(var.get())


# 開始処理
root = Tk()
root.title(GuriConstants.APP_NAME)
canvas = Canvas(root,
                width=GuriConstants.GURI_VIEW_WIDTH,
                height=GuriConstants.GURI_VIEW_HEIGHT,
                bg=GuriConstants.VIEW_MODE_BGCOLOR)
canvas.grid(row=1, column=0, columnspan=14)
canvas.focus_set()

listdata = []

ad = AppData.AppData()
cad = CoordinateAxisData.CoordinateAxisData()

gdfs = GuriDataForSplash.GuriDataForSplash()
gd = GuriData.GuriData()
for i in range(gdfs.getNumberOfLines()):
    gd.appendLine(gdfs.getLine(i))
fd = FigureDrawer.FigureDrawer()

ad.setMode('VIEWING')

root.protocol("WM_DELETE_WINDOW", exitProc)
# キーボード・イベント
canvas.bind("<Key>",        keyPressed)
canvas.bind("<Shift-Up>",   shiftUp)
canvas.bind("<Alt-Up>",     altUp)
canvas.bind("<Return>",     keyReturn)
canvas.bind("<Escape>",     keyEscape)
canvas.bind("<F1>",         keyF1)
canvas.bind("<F2>",         keyF2)
canvas.bind("<F3>",         keyF3)
canvas.bind("<F4>",         keyF4)
canvas.bind("<F5>",         keyF5)
canvas.bind("<F6>",         keyF6)
canvas.bind("<Delete>",     deleteKeyPressed)
canvas.bind("<BackSpace>",   backspaceKeyPressed)
"""その他のキーボード・イベント
#   <Cancel>, <BackSpace>, <Tab>, <Shift_L>(any Shift key),
#   <Control_LShift_L>(any Control key),
#   <Alt_L>(any Alt key), <Pause>, <Caps_Lock>, <Escape>, <Prior>(Page Up>,
#   <Next>(Page Down),
#   <End><Home>, <Left>, <Up>, <Right>, <Down>, <Print>, <Insert>,
#   <Delete>, <F1>, <F2>,…,<F12>,
#   <Bum_Lock>, <Scroll_Lock>, <space>（空白), <less>(空白より小さい)
#   <Shift-Up>(シフトキーを押したまま<Up>), <Alt-Up>(Altキーを押したまま<Up>),
#   <Control-Up>(シフトキーを押しまま<Up>), その他"a"などの文字を直接記述
"""
btnExit = Button(root, text='終了', height=1,
                 command=exitProc).grid(row=0, column=0)
btnLoad = Button(root, text='Load', height=1,
                 command=loadGuriData).grid(row=0, column=1)
btnSave = Button(root, text='Save', height=1,
                 command=saveGuriData).grid(row=0, column=2)
btnDraw = Button(root, text='作図', height=1,
                 command=toggleMode).grid(row=0, column=3)
btnNewFig = Button(root, text='白紙', height=1,
                   command=clearData).grid(row=0, column=4)
btnMagnify = Button(root, text='拡大', height=1,
                    command=ad.upRatio).grid(row=0, column=5)
btnReduce = Button(root, text='縮小', height=1,
                   command=ad.downRatio).grid(row=0, column=6)
btnTurn = Button(root, text='回転', height=1,
                 command=funcChangeTurn).grid(row=0, column=7)
btnESC = Button(root, text='停止', height=1,
                command=funcESC).grid(row=0, column=8)
btnFront = Button(root, text='正面図', height=1,
                  command=funcFrontView).grid(row=0, column=9)
btnTop = Button(root, text='平面図', height=1,
                command=funcTopView).grid(row=0, column=10)
btnSide = Button(root, text='右側面', height=1,
                 command=funcSideView).grid(row=0, column=12)
btnIsometric = Button(root, text='等角図', height=1,
                      command=funcIsometricView).grid(row=0, column=13)

var = StringVar()
var.set(gd.getComment())
textbox = Entry(root, textvariable=var, font=("", 12),
                justify="center", width=100) \
    .grid(row=2, column=0, columnspan=14, padx=1, pady=1, sticky=tk.W+tk.E)
b = Button(root, text="コメントの変更をデータに反映する", command=callback) \
    .grid(row=3, column=10, columnspan=5)

timerProc()
root.mainloop()
