#!/usr/bin/python
# -*- coding: utf-8 -*-


class GuriDataForSplash:
    """立体グリグリアプリケーションが、起動直後に持つべきデータ"""

    def __init__(self):                  # コンストラクタ
        self._comment = "超簡易3D editor 立体グリグリ Splash用データ"
        self._setDefaultLines()
        self._maxpos = [80, 80, 40]
        self._minpos = [00,  0,  0]
        self._midpos = [40, 40, 20]
        self._maxsize = 800

    def getComment(self):
        """コメントを返す。このクラスの場合は、アプリケーション名"""
        return self._comment

    def _setDefaultLines(self):
        """線分データのリストを作成"""
        self._lines = [
            [00,  0,  0, 80,  0,  0],
            [80,  0,  0, 80, 40,  0],
            [80, 40,  0, 40, 40,  0],
            [40, 40,  0,  0, 80,  0],
            [00, 80,  0,  0,  0,  0],
            [00,  0,  0,  0,  0, 40],
            [00,  0, 40,  0, 80, 40],
            [00, 80, 40,  0, 80,  0],
            [00, 80, 40, 40, 40, 40],
            [40, 40, 40, 40, 40,  0],
            [40, 40, 40, 80, 40, 40],
            [80, 40, 40, 80,  0, 40],
            [80,  0, 40,  0,  0, 40],
            [80,  0, 40, 80,  0,  0],
            [80, 40,  0, 80, 40, 40]
        ]

    def getLine(self, n):
        """n番目の線分データを返す。データの先頭は0番目とする。
           線分データとして、始点座標、終点座標の数値リストを返す。"""
        if 0 <= n and n < len(self._lines):
            return self._lines[n]
        else:
            return [0, 0, 0, 0, 0, 0]
            # ここはエラーをあげるべきなのか

    def setLine(self, nl):
        """
        引数に与えた線分への参照から、座標データをデータの配列にセット　と見せかけて無視
        テスト用、あるいはスプラッシュ用のデータなので、変更は不可
        """
        return True  # 何を受け取ってもTrueを返す

    def getNumberOfLines(self):
        return len(self._lines)

    def getMaxPos(self):
        """最大座標値を返す。"""
        return self._maxpos

    def getMinPos(self):
        """最小座標値を返す。"""
        return self._minpos

    def getMidPos(self):
        """中央座標値を返す。"""
        return self._midpos

    def getMaxSize(self):
        """最大寸法を返す"""
        return self._maxsize
