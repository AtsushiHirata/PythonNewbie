#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import os
from datetime import datetime

import GuriData
import GuriConstants


class TestGuriData(unittest.TestCase):

    def setUp(self):
        self.g = GuriData.GuriData()

    def test_getComment(self):
        self.assertEqual(self.g.getComment(), "超簡易3D editor 立体グリグリ データオブジェクト")

    def test_setComment(self):
        self.assertEqual(self.g.setComment("HogeFuga どっどーん！"),True)
        self.assertEqual(self.g.getComment(),"HogeFuga どっどーん！")

    def test_getNumberOfLines(self):
        self.assertEqual(self.g.getNumberOfLines(),0)   # オブジェクトが生成された直後は線分なし。
        self.assertEqual(self.g.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加
        self.assertEqual(self.g.getNumberOfLines(),1)                 # 線分本数は1のはず
        self.assertEqual(self.g.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　２本目
        self.assertEqual(self.g.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　３本目
        self.assertEqual(self.g.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　４本目
        self.assertEqual(self.g.getNumberOfLines(),4)                 # 線分本数は4のはず

    def test_setLine(self):
        self.assertEqual(self.g.getNumberOfLines(), 0)  # オブジェクトが生成された直後は線分なし。
        self.assertEqual(self.g.setLine([1,2,3, 4,5,6]),True)
        self.assertEqual(self.g.getNumberOfLines(), 1)
        self.assertEqual(self.g.getLine(self.g.getNumberOfLines()-1),[1,2,3, 4,5,6])

    def test_appendLine(self):
        self.assertEqual(self.g.appendLine([1,2,3, 4,5,6]),True)
        self.assertEqual(self.g.getNumberOfLines(),1)
        self.assertEqual(self.g.getLine(0),[1,2,3, 4,5,6])
        self.assertEqual(self.g.getLine(self.g.getNumberOfLines()-1),[1,2,3, 4,5,6])

    def test_getLine(self):
        self.assertEqual(self.g.getNumberOfLines(),0)
        self.assertEqual(self.g.appendLine([ 1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.g.appendLine([ 2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.g.appendLine([ 3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.g.getNumberOfLines(),3)
        self.assertEqual(self.g.getLine(1), [ 2, 2, 2, 2, 2, 2])
        self.assertEqual(self.g.getLine(2), [ 3, 3, 3, 3, 3, 3])
        self.assertEqual(self.g.getLine(100),[]) # 無効な添字が指定されたら、空のリストを返す

    def test_getMaxPos(self):
        self.assertEqual(self.g.getMaxPos(), [ 0, 0, 0]) # インスタンス生成直後は最大座標は全て0のはず
        # この命名も疑問がある。なにがしかの意味において、最大の座標値を返すと誤解を呼ぶ。
        # 実際には、最大のX座標値、Y座標値、Z座標値を返すメソッドである。
        self.assertEqual(self.g.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.g.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.g.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.g.getMaxPos(),[ 3, 3, 3])

    def test_searchMaxposMinposMidposMaxsize(self):
        # インスタンスの生成直後は、全て0のはず
        self.assertEqual(self.g.searchMaxposMinposMidposMaxsize(),[ [0, 0, 0],  [0, 0, 0],  [0, 0, 0],  [0, 0, 0] ])
        # ３本の線分データを与えた場合
        self.assertEqual(self.g.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.g.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.g.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.g.searchMaxposMinposMidposMaxsize(), [[3, 3, 3], [1, 1, 1], [2, 2, 2], [2, 2, 2]])

    def test_getMinPos(self):
        self.assertEqual(self.g.getMinPos(),[ 0, 0, 0])
        # ３本の線分データを与えた場合
        self.assertEqual(self.g.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.g.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.g.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.g.getMinPos(), [1, 1, 1])

    def test_getMidPos(self):
        self.assertEqual(self.g.getMidPos(),[ 0, 0, 0])
        # ３本の線分データを与えた場合
        self.assertEqual(self.g.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.g.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.g.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.g.getMidPos(), [2, 2, 2])

    def test_getMaxSize(self):
        self.assertEqual(self.g.getMaxSize(),[0,0,0]) # インスタンス生成直後は0
        self.assertEqual(self.g.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.g.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.g.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.g.getMaxSize(),[2,2,2])

    def test_loadData(self):
        filename = GuriConstants.DATA_PATH + "滑り台(UTF-8).gri"
        self.assertEqual(self.g.loadData(filename),True)
        self.assertEqual(self.g.getNumberOfLines(),75)
        self.assertEqual(self.g.getLine(9),[ 430, 550, 200, 430, 550, 0])
        self.assertEqual(self.g.getMaxSize(),[1000, 880, 200])

        filename = GuriConstants.DATA_PATH + "utf8_ウマ.gri"
        self.assertEqual(self.g.loadData(filename),True)

    def test_saveData(self):
        filename = GuriConstants.DATA_PATH \
                 + datetime.now().strftime("%Y_%m_%d_%H_%M_%S") \
                 + "_" + "testdata.gri"
        _g = GuriData.GuriData()
        self.assertEqual(_g.getNumberOfLines(),0)
        _g.appendLine([1,1,1, 2,2,2])
        self.assertEqual(_g.getNumberOfLines(),1)
        self.assertEqual(_g.saveData(filename),True)
        _g.loadData(filename)
        self.assertEqual(_g.getNumberOfLines(),1)
        os.remove(filename)

    def test_deleteTail(self):
        _g = GuriData.GuriData()
        self.assertEqual(_g.getNumberOfLines(),0)
        _g.appendLine([1,1,1, 2,2,2])
        self.assertEqual(_g.getNumberOfLines(),1)
        self.assertEqual(_g.deleteTail(),True)
        self.assertEqual(_g.getNumberOfLines(),0)
        self.assertEqual(_g.deleteTail(),False)
        self.assertEqual(self.g.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　1本目
        self.assertEqual(self.g.setLine([ 5, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　2本目
        self.assertEqual(self.g.setLine([ 4, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　3本目
        self.assertEqual(self.g.getNumberOfLines(),3)                 # 線分本数は3のはず
        self.assertEqual(self.g.deleteTail(),True)
        self.assertEqual(self.g.getNumberOfLines(),2)                 # 線分本数は2のはず
        self.assertEqual(self.g.getLine(1),[ 5, 5, 4, 3, 2, 1])

    def test_isExists(self):
        # 引数に指定した線分データが存在するかを真理値で返す。存在すればTrue、存在しなければFalse。
        _g = GuriData.GuriData()
        self.assertEqual(self.g.setLine([6, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([5, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([4, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([3, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([2, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([1, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.getNumberOfLines(),6)
        self.assertEqual(self.g.isExists([3, 5, 4, 3, 2, 1]),True)
        # 始点と終点を裏返した線分でも、同一の線分と判定しなければならない。
        self.assertEqual(self.g.isExists([3, 2, 1, 3, 5, 4]),True)
        # 含まれる要素が同じでも、順番が違えば別の線分と判定しなければならない。
        self.assertEqual(self.g.isExists([2, 3, 1, 3, 5, 4]),False)
        self.assertEqual(self.g.isExists([7, 5, 4, 3, 2, 1]),False)
        self.assertEqual(self.g.deleteLine(3),True)
        self.assertEqual(self.g.isExists([ 3, 5, 4, 3, 2, 1]),False)

    def test_deleteLine(self):
        _g = GuriData.GuriData()
        self.assertEqual(self.g.setLine([ 6, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([ 5, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([ 4, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.deleteLine(1),True)
        self.assertEqual(self.g.getNumberOfLines(),2)
        self.assertEqual(self.g.isExists([ 5, 5, 4, 3, 2, 1]),False)
        self.assertEqual(self.g.deleteLine(10),False)

    def test_getIndex(self):
        _g = GuriData.GuriData()
        self.assertEqual(self.g.setLine([6, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([5, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([4, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([3, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([2, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.setLine([1, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.getNumberOfLines(),6)
        self.assertEqual(self.g.isExists([3, 5, 4, 3, 2, 1]),True)
        self.assertEqual(self.g.getIndex([3, 5, 4, 3, 2, 1]),3)
        # 存在しない線分を指定したら、-1を返す。
        self.assertEqual(self.g.getIndex([3, 3, 4, 3, 2, 1]),-1)
        # 始点終点逆転していても、添字を返す。
        self.assertEqual(self.g.getIndex([3, 2, 1, 3, 5, 4]),3)

    def test_clearData(self):
        """保持していたデータを全て消去する"""
        _g = GuriData.GuriData()
        self.assertEqual(_g.getNumberOfLines(),0)
        self.assertEqual(_g.getComment(),"超簡易3D editor 立体グリグリ データオブジェクト")
        self.assertEqual(_g.setLine([6, 5, 4, 3, 2, 1]),True)
        self.assertEqual(_g.setLine([5, 5, 4, 3, 2, 1]),True)
        self.assertEqual(_g.setLine([4, 5, 4, 3, 2, 1]),True)
        self.assertEqual(_g.setLine([3, 5, 4, 3, 2, 1]),True)
        self.assertEqual(_g.setLine([2, 5, 4, 3, 2, 1]),True)
        self.assertEqual(_g.setLine([1, 5, 4, 3, 2, 1]),True)
        self.assertEqual(_g.getNumberOfLines(),6)
        self.assertEqual(_g.setComment("適当なコメント文"),True)
        self.assertEqual(_g.getComment(),"適当なコメント文")
        self.assertEqual(_g.clearData(),True)
        self.assertEqual(_g.getComment(),"超簡易3D editor 立体グリグリ データオブジェクト")
        self.assertEqual(_g.getNumberOfLines(),0)
        


if __name__ == "__main__":
    unittest.main()
