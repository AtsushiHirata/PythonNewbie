#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
from datetime import datetime
import os

import GuriData
import GuriConstants
import CoordinateAxisData


class TestCoordinateAxisData(unittest.TestCase):

    def setUp(self):
        self.g = GuriData.GuriData()
        self.c = CoordinateAxisData.CoordinateAxisData()

    def test_getComment(self):
        self.assertEqual(self.c.getComment(), "座標軸表示用データ")

    def test_setComment(self):
        self.assertEqual(self.c.setComment("HogeFuga どっどーん！"),True)
        self.assertEqual(self.c.getComment(),"座標軸表示用データ")

    def test_getNumberOfLines(self):
        self.assertEqual(self.c.getNumberOfLines(),3)   # オブジェクトが生成された直後、すでに座標軸データを持っている。
        self.assertEqual(self.c.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加。しかし、追加はされない。
        self.assertEqual(self.c.getNumberOfLines(),3)                 # 線分本数は変化せず、３のまま。
        self.assertEqual(self.c.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　
        self.assertEqual(self.c.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　
        self.assertEqual(self.c.setLine([ 6, 5, 4, 3, 2, 1]),True)    # 一つ線分を追加　
        self.assertEqual(self.c.getNumberOfLines(),3)                 # 線分本数は３のまま

    def test_setLine(self):
        self.assertEqual(self.c.getNumberOfLines(), 3)  # オブジェクトが生成された直後、すでに座標軸分のデータあり。
        self.assertEqual(self.c.setLine([1,2,3, 4,5,6]),True)
        self.assertEqual(self.c.getNumberOfLines(), 3)  # 変化なし。
        self.assertEqual(self.c.getLine(self.c.getNumberOfLines()-1),
                         [0,0,0, 0,0,GuriConstants.COORDINATE_AXIS_LINE_LENGTH])

    def test_appendLine(self):
        self.assertEqual(self.c.appendLine([1,2,3, 4,5,6]),True)
        self.assertEqual(self.c.getNumberOfLines(),3)
        self.assertEqual(self.c.getLine(0),[0,0,0, GuriConstants.COORDINATE_AXIS_LINE_LENGTH,0,0])
        self.assertEqual(self.c.getLine(self.c.getNumberOfLines()-1),
                         [0,0,0, 0,0,GuriConstants.COORDINATE_AXIS_LINE_LENGTH])

    def test_getLine(self):
        self.assertEqual(self.c.getNumberOfLines(),3)
        self.assertEqual(self.c.appendLine([ 1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.c.appendLine([ 2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.c.appendLine([ 3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.c.getNumberOfLines(),3)
        self.assertEqual(self.c.getLine(1), [ 0,0,0, 0, GuriConstants.COORDINATE_AXIS_LINE_LENGTH, 0])
        self.assertEqual(self.c.getLine(2), [ 0,0,0, 0, 0, GuriConstants.COORDINATE_AXIS_LINE_LENGTH])
        self.assertEqual(self.c.getLine(100),[]) # 無効な添字が指定されたら、空のリストを返す

    def test_getMaxPos(self):
        self.assertEqual(self.c.getMaxPos(),
                        [GuriConstants.COORDINATE_AXIS_LINE_LENGTH,
                         GuriConstants.COORDINATE_AXIS_LINE_LENGTH,
                         GuriConstants.COORDINATE_AXIS_LINE_LENGTH]) # インスタンス生成直後は最大座標は全て0のはず
        # この命名も疑問がある。なにがしかの意味において、最大の座標値を返すと誤解を呼ぶ。
        # 実際には、最大のX座標値、Y座標値、Z座標値を返すメソッドである。
        self.assertEqual(self.c.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.c.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.c.appendLine([3, 3, 3, 3, 3, 3]), True)
        CALL = GuriConstants.COORDINATE_AXIS_LINE_LENGTH
        self.assertEqual(self.c.getMaxPos(), [CALL, CALL, CALL])

    def test_searchMaxposMinposMidposMaxsize(self):
        CALL = GuriConstants.COORDINATE_AXIS_LINE_LENGTH
        self.assertEqual(self.c.searchMaxposMinposMidposMaxsize(),
                        [ [CALL, CALL, CALL],         # max pos
                        [0, 0, 0],                  # min pos
                        [CALL/2, CALL/2, CALL/2],   # mid pos
                        CALL ])                     # max size
        # ３本の線分データを与えた場合
        self.assertEqual(self.c.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.c.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.c.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.c.searchMaxposMinposMidposMaxsize(),
                        [ [CALL, CALL, CALL],         # max pos
                        [0, 0, 0],                  # min pos
                        [CALL/2, CALL/2, CALL/2],   # mid pos
                        CALL ])                     # max size

    def test_getMinPos(self):
        self.assertEqual(self.c.getMinPos(),[ 0, 0, 0])
        # ３本の線分データを与えた場合
        self.assertEqual(self.c.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.c.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.c.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.c.getMinPos(), [0,0,0])

    def test_getMidPos(self):
        CALL = GuriConstants.COORDINATE_AXIS_LINE_LENGTH
        self.assertEqual(self.c.getMidPos(),[CALL/2, CALL/2, CALL/2])
        # ３本の線分データを与えた場合
        self.assertEqual(self.c.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.c.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.c.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.c.getMidPos(), [CALL/2, CALL/2, CALL/2])

    def test_getMaxSize(self):
        CALL = GuriConstants.COORDINATE_AXIS_LINE_LENGTH
        self.assertEqual(self.c.getMaxSize(),CALL) # インスタンス生成直後は0
        self.assertEqual(self.c.appendLine([1, 1, 1, 1, 1, 1]), True)
        self.assertEqual(self.c.appendLine([2, 2, 2, 2, 2, 2]), True)
        self.assertEqual(self.c.appendLine([3, 3, 3, 3, 3, 3]), True)
        self.assertEqual(self.c.getMaxSize(),CALL)

    def test_loadData(self):
        try:
            filename = GuriConstants.DATA_PATH + "滑り台(UTF-8).gri"
            self.assertEqual(self.c.loadData(filename),True)
            filename = GuriConstants.DATA_PATH + "utf8_ウマ.gri"
            self.assertEqual(self.g.loadData(filename),True)
        except NotImplementedError as e:
        	print('\n' + str(e))

    def test_saveData(self):
        try:
            filename = GuriConstants.DATA_PATH + datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + "_" + "testdata.gri"
            print("saving " + filename)
            # 線分一本だけのデータ
            _c = CoordinateAxisData.CoordinateAxisData()
            self.assertEqual(_c.getNumberOfLines(),3)
            _c.appendLine([1,1,1, 2,2,2])
            self.assertEqual(_c.getNumberOfLines(),3)
            self.assertEqual(_c.saveData(filename),True)
        except NotImplementedError as e:
            print('\n' + str(e))


if __name__ == "__main__":
    unittest.main()
