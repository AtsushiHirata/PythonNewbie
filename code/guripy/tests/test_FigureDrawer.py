#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import time
from tkinter import *

import GuriConstants
import AppData
import FigureDrawer
import GuriData
import GuriDataForSplash


class TestFigureDrawer(unittest.TestCase):

    def setUp(self):
        self.fd = FigureDrawer.FigureDrawer()
        self.gdfs = GuriDataForSplash.GuriDataForSplash()
        self.gd = GuriData.GuriData()
        self.gd.loadData(GuriConstants.DATA_PATH + "滑り台(UTF-8).gri")
        self.ad = AppData.AppData()
        self.root = Tk()
        self.root.title("FigureDrawer.drawToWindow test")
        self.canvas=Canvas(self.root, width=GuriConstants.GURI_VIEW_WIDTH,height=GuriConstants.GURI_VIEW_HEIGHT,bg="#CCCCCC")
        self.canvas.pack(expand=False)
        self.root.update()
        self.timeViewing = 0.2
        # self.timeViewing = 1

    def test_drawToGuriView(self):
        self.ad.ratio = 0.01
        self.ad.setMode('VIEWING')
        self.assertEqual(self.fd.drawToGuriView(self.gd,45,30,0.2,self.canvas,self.ad),True)
        self.root.update()
        time.sleep(self.timeViewing)
        self.assertEqual(self.fd.drawToGuriView(self.gdfs,45,30,1.0,self.canvas,self.ad),True)
        self.root.update()
        time.sleep(self.timeViewing)
        self.assertEqual(self.fd.drawToGuriView(self.gdfs,45,30,3.0,self.canvas,self.ad),True)
        self.root.update()
        self.ad.setMode('DRAWING')
        self.assertEqual(self.fd.drawToGuriView(self.gd,45,30,0.2,self.canvas,self.ad),True)
        self.root.update()
        time.sleep(self.timeViewing)
        self.assertEqual(self.fd.drawToGuriView(self.gdfs,45,30,1.0,self.canvas,self.ad),True)
        self.root.update()
        time.sleep(self.timeViewing)
        self.assertEqual(self.fd.drawToGuriView(self.gdfs,45,30,3.0,self.canvas,self.ad),True)
        self.root.update()
        time.sleep(self.timeViewing)
        #
        self.ad.pointerfrom = [10, 10, 10]
        self.ad.pointerto = [100,100,100]
        self.assertEqual(self.fd.drawToGuriView(self.gdfs,45,30,3.0,self.canvas,self.ad),True)
        self.root.update()
        time.sleep(self.timeViewing)
        

    def test_rotateALine(self):
        result = self.fd.rotateALine([0,0,0, 1,0,0],90,0)
        self.assertTrue(self.isGoodEnough(0,int(result[0])))
        self.assertTrue(self.isGoodEnough(0,int(result[1])))
        self.assertTrue(self.isGoodEnough(0,int(result[2])))
        self.assertTrue(self.isGoodEnough(0,int(result[3])))
        self.assertTrue(self.isGoodEnough(0,int(result[4])))
        self.assertTrue(self.isGoodEnough(-1,int(result[5])))

    def isGoodEnough(sefl,targetVal,result):
        if abs(targetVal - result) < 0.0000001 :
            return True
        else:
            return False

    def tearDown(self):
        self.root.update_idletasks()
        self.root.update()


if __name__ == "__main__":
    unittest.main()
