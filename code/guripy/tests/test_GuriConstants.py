import unittest
import GuriConstants

class TestGuriConstants(unittest.TestCase):

    def setUp(self):
        self.gc = GuriConstants

    def test_Constants(self):
        self.assertEqual(GuriConstants.GURI_WINDOW_WIDTH,800)
        self.assertEqual(GuriConstants.GURI_WINDOW_HEIGHT,600)
        self.assertEqual(self.gc.GURI_VIEW_WIDTH,800)
        self.assertEqual(self.gc.GURI_VIEW_HEIGHT,600)
        self.assertEqual(self.gc.DATA_PATH,'./data/')
        self.assertEqual(self.gc.GURI_VIEW_BGCOLOR,"#CCCCCC")
        self.assertEqual(self.gc.TICK_TIME,200)
#        self.assertEqual(self.gc.TICK_UNIT_ANGLE,30)
        self.assertEqual(self.gc.TICK_UNIT_ANGLE,5)
        self.assertEqual(self.gc.UNIT_RATIO,0.1)
        self.assertEqual(self.gc.APP_NAME,'立体グリグリ・ニシキヘビ版')
