import unittest
import GuriDataForSplash

class TestGuriDataForSplash(unittest.TestCase):

    def setUp(self):
        self.g = GuriDataForSplash.GuriDataForSplash()

    def test_getComment(self):
        self.assertEqual(self.g.getComment(), "超簡易3D editor 立体グリグリ Splash用データ")

    def test_getLine(self):
        self.assertEqual(self.g.getLine(0), [ 0, 0, 0,80, 0, 0])

    def test_setLine(self):
        self.assertEqual(self.g.setLine([1,2,3, 4,5,6]),True)

    def test_getNumberOfLines(self):
        self.assertEqual(self.g.getNumberOfLines(),15)

    def test_getMaxPos(self):
        self.assertEqual(self.g.getMaxPos(),[80,80,40])

    def test_getMinPos(self):
        self.assertEqual(self.g.getMinPos(),[ 0, 0, 0])

    def test_getMidPos(self):
        self.assertEqual(self.g.getMidPos(),[40,40,20])

    def test_getMaxSize(self):
        self.assertEqual(self.g.getMaxSize(),800)

if __name__ == "__main__":
    unittest.main()