#!/usr/bin/python
# -*- coding: utf-8 -*-

import unittest
import sys

import AppData
import GuriConstants

class TestAppData(unittest.TestCase):

    def setUp(self):
        self.ad = AppData.AppData()

    def test_setDirectionTurn(self):
        self.assertEqual(self.ad.setDirectionTurn('STOP'),'STOP')
        self.assertEqual(self.ad.directionTurn,'STOP')
        self.assertEqual(self.ad.directionLon,0)
        self.assertEqual(self.ad.directionLat,0)

        self.assertEqual(self.ad.setDirectionTurn('RIGHT'),'RIGHT')
        self.assertEqual(self.ad.directionTurn,'RIGHT')
        self.assertEqual(self.ad.directionLon,1)
        self.assertEqual(self.ad.directionLat,0)

        self.assertEqual(self.ad.setDirectionTurn('LEFT'),'LEFT')
        self.assertEqual(self.ad.directionTurn,'LEFT')
        self.assertEqual(self.ad.directionLon,-1)
        self.assertEqual(self.ad.directionLat,0)

        self.assertEqual(self.ad.setDirectionTurn('UP'),'UP')
        self.assertEqual(self.ad.directionTurn,'UP')
        self.assertEqual(self.ad.directionLon,0)
        self.assertEqual(self.ad.directionLat,1)

        self.assertEqual(self.ad.setDirectionTurn('DOWN'),'DOWN')
        self.assertEqual(self.ad.directionTurn,'DOWN')
        self.assertEqual(self.ad.directionLon,0)
        self.assertEqual(self.ad.directionLat,-1)

        self.assertEqual(self.ad.setDirectionTurn('HOGE'),'STOP')

    def test_upRatio(self):
        for count in range(50):
            self.ad.upRatio()
            self.assertTrue(self.ad.ratio <= GuriConstants.MAX_RATIO)

    def test_downRatio(self):
        for count in range(50):
            self.ad.downRatio()
            self.assertTrue(GuriConstants.MIN_RATIO <= self.ad.ratio)

    def test_setView(self):
        self.ad.setView('FRONT')
        self.assertEqual(self.ad.directionTurn,'STOP')
        self.assertEqual(self.ad.longitude,0)
        self.assertEqual(self.ad.latitude,0)

        self.ad.setView('TOP')
        self.assertEqual(self.ad.directionTurn,'STOP')
        self.assertEqual(self.ad.longitude,0)
        self.assertEqual(self.ad.latitude,90)

        self.ad.setView('SIDE')
        self.assertEqual(self.ad.directionTurn,'STOP')
        self.assertEqual(self.ad.longitude,90)
        self.assertEqual(self.ad.latitude,0)

        self.ad.setView('ISOMETRIC')
        self.assertEqual(self.ad.directionTurn,'STOP')
        self.assertEqual(self.ad.longitude,45)
        self.assertEqual(self.ad.latitude,30)

    def test_normalizeAngle(self):
        self.assertEqual(self.ad.normalizeAngle(0),0)
        self.assertEqual(self.ad.normalizeAngle(100),100)
        self.assertEqual(self.ad.normalizeAngle(360),360)
        self.assertEqual(self.ad.normalizeAngle(361),1)
        self.assertEqual(self.ad.normalizeAngle(-1),359)
        self.assertEqual(self.ad.normalizeAngle(-365),-5)

    def test_tickAngle(self):
        self.ad.directionTurn = 'RIGHT'
        self.repeat360times()
        self.ad.directionTurn = 'LEFT'
        self.repeat360times()
        self.ad.directionTurn = 'UP'
        self.repeat360times()
        self.ad.directionTurn = 'DOWN'
        self.repeat360times()
        self.ad.directionTurn = 'STOP'
        self.repeat360times()
        self.ad.directionTurn = 'HOGE'
        self.repeat360times()

    def repeat360times(self):
        for i in range(360):
            prev_lon = self.ad.longitude
            prev_lat = self.ad.latitude
            self.ad.tickAngle()
            expected_lon = prev_lon + GuriConstants.TICK_UNIT_ANGLE * self.ad.directionLon
            if 360 < expected_lon:
                expected_lon -= 360
            elif expected_lon < 0:
                expected_lon += 360
            expected_lat = prev_lat + GuriConstants.TICK_UNIT_ANGLE * self.ad.directionLat
            if 360 < expected_lat:
                expected_lat -= 360
            elif expected_lat < 0:
                expected_lat += 360
            self.assertEqual(self.ad.longitude,expected_lon)
            self.assertEqual(self.ad.latitude, expected_lat)

    def test_rotateTurnDirection(self):
        prev_dir = self.ad.directionTurn
        self.ad.rotateTurnDirection()
        if prev_dir == 'STOP':
            self.assertEqual(self.ad.directionTurn,'RIGHT')
        elif prev_dir == 'RIGHT':
            self.assertEqual(self.ad.directionTurn,'LEFT')
        elif prev_dir == 'LEFT':
            self.assertEqual(self.ad.directionTurn,'UP')
        elif prev_dir == 'UP':
            self.assertEqual(self.ad.directionTurn,'DOWN')
        elif prev_dir == 'DOWN':
            self.assertEqual(self.ad.directionTurn,'RIGHT')

    def test_setMode(self):
        self.assertEqual(self.ad.setMode('VIEWING'),'VIEWING')
        self.assertEqual(self.ad.backcolor,GuriConstants.VIEW_MODE_BGCOLOR)
        self.assertEqual(self.ad.setMode('DRAWING'),'DRAWING')
        self.assertEqual(self.ad.backcolor,GuriConstants.DRAW_MODE_BGCOLOR)
        current_mode = self.ad.mode
        self.assertEqual(self.ad.setMode('HOGE'),current_mode)
        self.assertEqual(self.ad.backcolor,GuriConstants.DRAW_MODE_BGCOLOR)

    def test_setPointerFrom(self):
        self.assertEqual(self.ad.setPointerFrom([0, 0, 0]), [0, 0, 0])
        self.assertEqual(self.ad.setPointerFrom([-1, -1, -1]),[0, 0, 0])
        overmaxpos = GuriConstants.MAX_POS + 1
        self.assertEqual(self.ad.setPointerFrom([1, 1, 1]), [1, 1, 1])
        cppf = current_pos_pointer_from = self.ad.pointerfrom
        self.assertEqual(self.ad.setPointerFrom([overmaxpos, overmaxpos, overmaxpos]),
                         [cppf[0], cppf[1], cppf[2]])

    def test_setPointerTo(self):
        self.assertEqual(self.ad.setPointerTo([0, 0, 0]), [0, 0, 0])
        self.assertEqual(self.ad.setPointerTo([-1, -1, -1]),[0, 0, 0])
        overmaxpos = GuriConstants.MAX_POS + 1
        self.assertEqual(self.ad.setPointerTo([1, 1, 1]), [1, 1, 1])
        cppt = current_pos_pointer_to = self.ad.pointerto
        self.assertEqual(self.ad.setPointerTo([overmaxpos, overmaxpos, overmaxpos]),
                         [cppt[0], cppt[1], cppt[2]])

    def test_unitmovePointerFrom(self):
        currentPointerFromPos = self.ad.pointerfrom = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerFrom('x'),
                [currentPointerFromPos[0] + GuriConstants.DEFAULT_UNIT_LENGTH, \
                 currentPointerFromPos[1], currentPointerFromPos[2]])
        currentPointerFromPos = self.ad.pointerfrom = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerFrom('X'),
                [currentPointerFromPos[0] - GuriConstants.DEFAULT_UNIT_LENGTH, \
                 currentPointerFromPos[1], currentPointerFromPos[2]])
        currentPointerFromPos = self.ad.pointerfrom = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerFrom('y'),
                [currentPointerFromPos[0],
                 currentPointerFromPos[1] + GuriConstants.DEFAULT_UNIT_LENGTH,
                 currentPointerFromPos[2]])
        currentPointerFromPos = self.ad.pointerfrom = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerFrom('Y'),
                [currentPointerFromPos[0],
                 currentPointerFromPos[1] - GuriConstants.DEFAULT_UNIT_LENGTH,
                 currentPointerFromPos[2]])
        currentPointerFromPos = self.ad.pointerfrom = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerFrom('z'),
                [currentPointerFromPos[0],
                currentPointerFromPos[1],
                currentPointerFromPos[2] + GuriConstants.DEFAULT_UNIT_LENGTH])
        currentPointerFromPos = self.ad.pointerfrom = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerFrom('Z'),
                [currentPointerFromPos[0],
                currentPointerFromPos[1],
                currentPointerFromPos[2] - GuriConstants.DEFAULT_UNIT_LENGTH])

    def test_unitmovePointerTo(self):
        currentPointerToPos = self.ad.pointerto = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerTo('x'),
                [currentPointerToPos[0] + GuriConstants.DEFAULT_UNIT_LENGTH, \
                 currentPointerToPos[1], currentPointerToPos[2]])
        currentPointerToPos = self.ad.pointerto = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerTo('X'),
                [currentPointerToPos[0] - GuriConstants.DEFAULT_UNIT_LENGTH, \
                 currentPointerToPos[1], currentPointerToPos[2]])
        currentPointerToPos = self.ad.pointerto = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerTo('y'),
                [currentPointerToPos[0],
                 currentPointerToPos[1] + GuriConstants.DEFAULT_UNIT_LENGTH,
                 currentPointerToPos[2]])
        currentPointerToPos = self.ad.pointerto = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerTo('Y'),
                [currentPointerToPos[0],
                 currentPointerToPos[1] - GuriConstants.DEFAULT_UNIT_LENGTH,
                 currentPointerToPos[2]])
        currentPointerToPos = self.ad.pointerto = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerTo('z'),
                [currentPointerToPos[0],
                currentPointerToPos[1],
                currentPointerToPos[2] + GuriConstants.DEFAULT_UNIT_LENGTH])
        currentPointerToPos = self.ad.pointerto = [50, 50, 50]
        self.assertEqual(self.ad.unitmovePointerTo('Z'),
                [currentPointerToPos[0],
                currentPointerToPos[1],
                currentPointerToPos[2] - GuriConstants.DEFAULT_UNIT_LENGTH])

    def test_setDrawingCondition(self):
        currentcondition = self.ad.drawingcondition
        self.assertEqual(self.ad.setDrawingCondition('FROM'),'FROM')
        self.assertEqual(self.ad.setDrawingCondition('TO'),'TO')
        self.assertEqual(self.ad.setDrawingCondition('HOGE'),'TO')
