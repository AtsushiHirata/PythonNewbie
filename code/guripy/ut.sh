echo ""
echo "       　　　　　　／　　　　　 　 　 ＼ "
echo "　　　　　　　　　／　　　　　　　　　　　　ヽ "
echo "　　 　 　 　　 /　　}　　　　　　　　　　　ｒ; ∨}､ "
echo "　　　　　　　/　　/　　　　　　　　　　　 | V　〉〉"
echo "　　 　 　 　/ 　 〈　　　　　　　　　　 ∧ﾉ　 //〉 "
echo "　　　　　 く　　　 ＼　　 ＿＿＿＿〈∧〉　　/7　 　 　 　} "
echo "　　　　　　　 >､{／￣￣　／＼::::::::７　 　 _∠＿＿＿_ノ "
echo "　　　　　　 /　/　　　__／ ｨ尓7＼:∧　　|::::::::＜ "
echo "　 　 　 　 /　厶ィT「炒　　｀¨´,,,　 | .{/¨７__:::::::::＼ "
echo "　　　 ∧/　　　ノゝく　　　 　 　 　 |　　　／::::::::::::::＼ "
echo "　　　 | /　　／}　　　＼ ｀⌒　　／!　　　|::::::::＼＼::::| "
echo "　　／´￣￣｀く　　　　__＞　 イ　 ｌ 　 　 |⌒＼:::＼ﾚ' "
echo "　/　　　 /￣￣ 　　 く　　 　 /!　 ｌ　　　 ト､　　＼/ "
echo "./　　　 厶＿＿ｒく￣￣＼　厶ｨ､ l　　 　 |　ヽΟ　＼ "
echo "　 　　 /　　　　　　＼　i!　〉'　 }　ｌ　　　　|　 ∧ 　 　 ＼ "
echo "　　 ／　　　　　　　　 ＼/　 　　 L　　　　　　∧　　＼　＼ "
echo "￣￣￣¨7ｰ────ｧ'　　 ,　　　 ￣￣く 　　∧　　　＼　＼ "
echo "　　　　 /: : : : : : : : : : {　　 〈　　　　　　　 :　 ./ ∧　　　　＼　＼ "
echo "　 　 　 l: : : : : : : : : : :ﾊ 　　 ヽ　　　　　　ﾉ　/: : :∧Ο　　　 ＼　｀ー──ｒく￣￣ "
echo "　　　　 |: : : : : : : : : : : :〉　　　　　　　　 　　 }: : : : : ＼　　　　　＼　　　　　　＼ "
echo "         Python だけに　千石撫子"
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
echo " ut.sh begins"
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
python -m unittest tests.test_GuriDataForSplash
python -m unittest tests.test_FigureDrawer
python -m unittest tests.test_GuriData
python -m unittest tests.test_GuriConstants
python -m unittest tests.test_AppData
python -m unittest tests.test_CoordinateAxisData
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
echo " ut.sh finished"
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
echo "　　 ＿r～＝ー-、"
echo "　 ／　　　 ヽ　＼ "
echo "'／　　　　　ﾉ　　ヽ "
echo "(　　 ノ乙＿< へ　 | "
echo "ヽ/ﾌ　／――――＼ノ "
echo "＜Z_／＼＿＿＿＿＿)ｺ "
echo "'/ | Lﾚ=ﾐ　　r=<L|ヽ "
echo "/ ||亅ﾋOｿ　　ﾋOﾉ/｜| "
echo "L/|ヽ＼　　｀　 L｜| "
echo "｜ﾊヽ'<　 ‐　／/｜| "
echo "// ヽ＼>'ー-イ // L| "
echo "/⌒ヽ＼>　　∧ ＺL| "
echo "　　|ｏヽ＿＿| o|ヽ "
echo "　 ヽ￣￣ 》《￣　| "
echo ""
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
echo " pycodestyle tool check starts"
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
# pep8 ./*.py
pycodestyle ./*.py
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
echo " pycodestyle tool check ends"
echo "-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-"
