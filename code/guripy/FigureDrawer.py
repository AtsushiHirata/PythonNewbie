#!/usr/bin/python
# -*- coding: utf-8 -*-

import GuriData
from tkinter import *
import math
from math import sin
from math import cos
from math import radians

import GuriConstants
import CoordinateAxisData


class FigureDrawer:
    """立体グリグリデータをウインドウ上に描くクラス"""

    _cad = CoordinateAxisData.CoordinateAxisData()

    def createLine(self, canvas, pos, color, w):
        canvas.create_line(int(pos[0]), int(pos[1]),
                           int(pos[2]), int(pos[3]),
                           fill=color, tag="DT",
                           width=w)
        return True

    def createOval(self, canvas, x, y, radi, color, w):
        canvas.create_oval(int(x)-radi,
                           int(y)-radi,
                           int(x)+radi,
                           int(y)+radi,
                           fill=color,
                           tag="DT",
                           width=w)
        return True

    def drawToGuriView(self, gd, lon, lat, r, canvas, ad):
        """
        gd     : GuriDataオブジェクト
        lon    : longitude
        lat    : latitude
        r      : ratio
        canvas : canvasへの参照
        ad     : AppDataオブジェクト
        """
        canvas.delete("DT")
        _line_color = ad.linecolor
        canvas['bg'] = ad.backcolor
        midpos = gd.getMidPos()
        midposAsLine = [midpos[0], midpos[1], midpos[2], 0, 0, 0]
        rotatedMidPos = self.rotateALine(midposAsLine, lon, lat)
        for i in range(gd.getNumberOfLines()):
            aline = gd.getLine(i)
            rotated = self.rotateALine(aline, lon, lat)
            transported = self.transToGuriView(rotated, rotatedMidPos, r)
            self.createLine(canvas, transported,
                            _line_color, GuriConstants.FIG_LINE_WIDTH)
        if ad.mode == 'DRAWING':
            _axis_color = GuriConstants.X_AXIS_COLOR
            for i in range(self._cad.getNumberOfLines()):
                aline = self._cad.getLine(i)
                rotated = self.rotateALine(aline, lon, lat)
                """ midposの値は表示する図形データのものをそのまま使う。
                    そうしないと、座標軸原点位置と図形の原点位置がずれてしまう。 """
                transported = self.transToGuriView(rotated, rotatedMidPos, r)
                if i == 0:
                    _axis_color = GuriConstants.X_AXIS_COLOR
                elif i == 1:
                    _axis_color = GuriConstants.Y_AXIS_COLOR
                else:  # elif i == 2:
                    _axis_color = GuriConstants.Z_AXIS_COLOR
                self.createLine(canvas, transported,
                                _axis_color, GuriConstants.AXIS_LINE_WIDTH)
            aline = [ad.pointerfrom[0], ad.pointerfrom[1], ad.pointerfrom[2],
                     ad.pointerto[0], ad.pointerto[1], ad.pointerto[2]]
            rotated = self.rotateALine(aline, lon, lat)
            transported = self.transToGuriView(rotated, rotatedMidPos, r)
            radius = GuriConstants.DIAMETER_POINTER / 2
            self.createLine(canvas, transported,
                            GuriConstants.COLOR_TEMP_LINE,
                            GuriConstants.AXIS_LINE_WIDTH)
            if ad.drawingcondition == 'FROM':
                self.createOval(canvas, transported[2], transported[3], radius,
                                GuriConstants.COLOR_POINTER_TO,
                                GuriConstants.AXIS_LINE_WIDTH)
                self.createOval(canvas, transported[0], transported[1], radius,
                                GuriConstants.COLOR_POINTER_FROM,
                                GuriConstants.AXIS_LINE_WIDTH)
            else:  # 'TO'
                self.createOval(canvas, transported[0], transported[1], radius,
                                GuriConstants.COLOR_POINTER_FROM,
                                GuriConstants.AXIS_LINE_WIDTH)
                self.createOval(canvas, transported[2], transported[3], radius,
                                GuriConstants.COLOR_POINTER_TO,
                                GuriConstants.AXIS_LINE_WIDTH)
        return True

    def rotateALine(self, aline, longitude_deg, latitude_deg):
        # 視点の経度（左右）# 視点の緯度（上下）
        lon, lat = radians(longitude_deg), radians(latitude_deg)
        # means Start position
        xs, ys, zs = float(aline[0]), float(aline[1]), float(aline[2])
        # means End position
        xe, ye, ze = float(aline[3]), float(aline[4]), float(aline[5])
        temp = [xs * cos(lon) + zs * sin(lon),
                ys,
                -xs * sin(lon) + zs * cos(lon),
                xe * cos(lon) + ze * sin(lon),
                ye,
                -xe * sin(lon) + ze * cos(lon)]
        return [temp[0],
                temp[1] * cos(lat) + temp[2] * sin(lat),
                -temp[1] * sin(lat) + temp[2] * cos(lat),
                temp[3],
                temp[4] * cos(lat) + temp[5] * sin(lat),
                -temp[4] * sin(lat) + temp[5] * cos(lat)]

    def transX(self, x, r, mid):
        return r * (x - float(mid)) \
            + GuriConstants.GURI_VIEW_WIDTH / 2

    def transY(self, y, r, mid):
        return GuriConstants.GURI_VIEW_HEIGHT / 2 \
             - (r * (y - float(mid)))

    def transToGuriView(self, aline, mid, r):
        """
        :param aline:   変換まえの線分データ
        :param mid:     図形の座標軸ごとの中心座標
        :param r:       表示倍率
        :return:        アプリケーションのウインドウに適した線分の座標
        """
        return [self.transX(float(aline[0]), r, mid[0]),
                self.transY(float(aline[1]), r, mid[1]),
                self.transX(float(aline[3]), r, mid[0]),
                self.transY(float(aline[4]), r, mid[1])]
